﻿using BLL.Models;
using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SQLite;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace BLL.ShiftManager
{
    public class ShiftManager
    {
        public ShiftViewModel ActiveShift;
        public bool ActiveShiftExists { get { return ActiveShift != null && ActiveShift.IsActiveShift; } }
        
        //Set userID
        //If user shift is already open then set the IsShiftActive bool
        //Load Shift data for that user 
        public ShiftManager(string userID)
        {
            _getActiveShift(userID);
        }

        private void _getActiveShift(string userID)
        {
            SQLiteDataReader result;
            DataTable datatable = new DataTable();

            try
            {
                using (SQLiteConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    cnn.Open();

                    SQLiteCommand command = new SQLiteCommand("SELECT * FROM tblShifts where Date=@dateparam and UserID=@userID and IsActiveShift='True'", cnn);
                    command.Parameters.AddWithValue("dateparam", DateTime.Now.Date.ToString());
                    command.Parameters.AddWithValue("userID", userID);
                    result = command.ExecuteReader();
                    datatable.Load(result);

                    cnn.Close();
                }
                if (datatable.Rows.Count > 0)
                {
                    ActiveShift = new ShiftViewModel();
                    ActiveShift.ID = Convert.ToInt32(datatable.Rows[0]["ID"]);
                    ActiveShift.Date = datatable.Rows[0]["Date"].ToString();
                    ActiveShift.UserID = datatable.Rows[0]["UserID"].ToString();
                    ActiveShift.ShiftStartTime = datatable.Rows[0]["ShiftStartTime"].ToString();
                    ActiveShift.IsActiveShift = Convert.ToBoolean(datatable.Rows[0]["IsActiveShift"]);
                    ActiveShift.IsUploadedToServer = Convert.ToBoolean(datatable.Rows[0]["IsUploadedToServer"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool _addShift()
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    Result = cnn.Execute("Insert into tblshifts (Date,UserID,ShiftStartTime,IsActiveShift, IsUploadedToServer) VALUES (@Date,@UserID,@ShiftStartTime,@IsActiveShiftString,@IsUploadedToServerString)", ActiveShift);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Convert.ToInt32(Result) > 0;
        }

        private bool _updateShift()
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    Result = cnn.Execute("Update tblShifts set ShiftStartTime=@ShiftStartTime,ShiftEndTime=@ShiftEndTime, IsActiveShift=@IsActiveShiftString, CashInHand=@CashInHand, IsUploadedToServer=@IsUploadedToServerString Where ID=@ID", ActiveShift);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Convert.ToInt32(Result) > 0;
        }

    //    {
    //"name": null,
    //"heading": null,
    //"summary": null,
    //"message": "Shift Started Successfully.",
    //"exceptionMessage": "",
    //"status": 200,
    //"isSuccess": true,
    //"responseObject": {}
    //}
        public bool StartShift(string userID)
        {
            try
            {
                if (!ActiveShiftExists)
                {
                    

                    string url = ServiceURLManager.GetShiftServiceURL() + "/StartShift";
                    object body = new
                    {
                        userId = StaticClassUserData.UserID,
                        deviceCode = StaticClassSyncData.POS_Code,
                        deviceMacAddress = StaticClassSyncData.POS_PSAM_MAC
                    };
                    var json1 = JsonConvert.SerializeObject(body);
                    string inputJson = (new JavaScriptSerializer()).Serialize(body);
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    string apiUrl = url+ "?body=" + json1;
                    string json = client.UploadString(url, json1);

                    var res = JsonConvert.DeserializeObject<dynamic>(json);

                    ActiveShift = new ShiftViewModel();
                    ActiveShift.UserID = userID;
                    ActiveShift.Date = DateTime.Now.Date.ToString();
                    ActiveShift.ShiftStartTime = DateTime.Now.ToString("HH:mm:ss");
                    ActiveShift.IsActiveShift = true;
                    ActiveShift.IsUploadedToServer = Convert.ToBoolean(res["isSuccess"]);
                    
                    var result = _addShift();

                    if (result)
                        _getActiveShift(ActiveShift.UserID);
                    return result;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

//     {
//    "name": null,
//    "heading": null,
//    "summary": null,
//    "message": "Shift Ended Successfully.",
//    "exceptionMessage": "",
//    "status": 200,
//    "isSuccess": true,
//    "responseObject": {}
//     }
        public bool EndShift()
        {
            try
            {
                if (ActiveShiftExists)
                {
                    string url = ServiceURLManager.GetShiftServiceURL() + "/EndShift";
                    object body = new
                    {
                        userId = StaticClassUserData.UserID,
                        deviceCode = StaticClassSyncData.POS_Code,
                        deviceMacAddress = StaticClassSyncData.POS_PSAM_MAC,
                        shiftAmount = ActiveShift.CashInHand.ToString(),
                        remarks = "Shift Ended"
                    };
                    var json1 = JsonConvert.SerializeObject(body);
                    string inputJson = (new JavaScriptSerializer()).Serialize(body);
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    string json = client.UploadString(url, json1);

                    var res = JsonConvert.DeserializeObject<dynamic>(json);

                    ActiveShift.ShiftEndTime = DateTime.Now.ToString("HH:mm:ss");
                    ActiveShift.IsActiveShift = false;

                    ActiveShift.IsUploadedToServer = Convert.ToBoolean(res["isSuccess"]);

                    var result = _updateShift();
                    if (result)
                        ActiveShift = null;
                    return result;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



//{
//    "name": null,
//    "heading": null,
//    "summary": null,
//    "message": "",
//    "exceptionMessage": "",
//    "status": 200,
//    "isSuccess": true,
//    "responseObject": {
//        "userId": "eb42b7f0-cde7-402e-af6c-a648b147d0fa",
//        "userName": "N Five",
//        "deviceCode": "be59e473-8265-4ee9-8480-09645041a375",
//        "deviceMacAddress": "F81654E53ADF",
//        "shiftStartTime": "4/25/2022 3:11:58 PM",
//        "shiftEndTime": "4/25/2022 4:00:20 PM",
//        "generationTime": "4/25/2022 4:43:13 PM",
//        "shiftAmount": "500",
//        "totalRevenue": "",
//        "totalQrSales": "0",
//        "qrSalesAmount": "",
//        "totalCardSales": null,
//        "cardSalesAmount": null,
//        "totalTopUp": null,
//        "totalTopUpAmount": null,
//        "remarks": "Shift Ended"
//    }
//}
    public RootobjectReceipt GenerateLastReceipt()
        {
            try
            {
                string url = ServiceURLManager.GetShiftServiceURL() + "/GenerateRecipt";
                object body = new
                {
                    userId = StaticClassUserData.UserID,
                    deviceCode = StaticClassSyncData.POS_Code,
                    deviceMacAddress = StaticClassSyncData.POS_PSAM_MAC
                };
                var json1 = JsonConvert.SerializeObject(body);
                string inputJson = (new JavaScriptSerializer()).Serialize(body);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.UploadString(url, json1);

                var res = JsonConvert.DeserializeObject<RootobjectReceipt>(json);

                return res;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
