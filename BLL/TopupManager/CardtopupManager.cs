﻿using BLL.Models;
using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.CardRechargeModel;

namespace BLL.TopupManager
{
    public class CardtopupManager
    {
        public RootobjectCardRecharge Addtopup(CardRechargeViewModel cardRechargeViewModel)
        {
            RootobjectCardRecharge data = new RootobjectCardRecharge();
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "CardTopup/CardRecharge";
                // string apiUrl = "https://localhost:44352/Api/CardTopup/CardRecharge";
                object input = new
                {
                    CardNumber = cardRechargeViewModel.CardNumber,
                    SerialNumber = cardRechargeViewModel.SerialNumber,
                    TransactionRef = cardRechargeViewModel.TransactionRef,
                    RechargeAmount = cardRechargeViewModel.RechargeAmount,
                    BalanceAfter = cardRechargeViewModel.BalanceAfter,
                    POS_Code = StaticClassSyncData.POS_Code,
                    UserID = StaticClassUserData.UserID,
                    TransactionType = cardRechargeViewModel.TransactionType,
                    PaymentMethod = cardRechargeViewModel.PaymentMethod,
                    PaymentTxnRef = cardRechargeViewModel.PaymentTxnRef
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.UploadString(apiUrl, inputJson);
                data = JsonConvert.DeserializeObject<RootobjectCardRecharge>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
        public RootobjectCardHolderInformation GetCardHolderInformationBySerialNumber(string SerialNumber)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();

            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "CardTopup/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                //string apiUrl = "https://localhost:44352/Api/CardTopup/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                object input = new
                {
                    SerialNumber = SerialNumber
                };

                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }

        public RootobjectCardRecharge GetCardTypeByOrganizationID(string OrganizationID)
        {
            RootobjectCardRecharge data = new RootobjectCardRecharge();

            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "application/GetCardTypes?organizationID=" + "a4563f0f-5554-416e-b662-183c7b028587";// + OrganizationID;
                object input = new
                {
                    OrganizationID = OrganizationID
                };
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectCardRecharge>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }

    }
}
