﻿using BLL.Models;
using BLL.Models.ViewModel;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SQLite;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace BLL.QRScanManager
{
    public class QRScanManager
    {
        //Sample QR Data : "TID":"2022041401364808931","FA":40.0,"CD":"Apr 14, 2022 01:36:48 PM","ET":"Apr 14, 2022 02:06:48 PM","SC":"0020","OC":"ORG-002"
        //Scan Method with scan mode parameter
        public QRScanResult ScanQRCheckin(QRScanModel qrData)
        {
            try
            {
                //Check Station
                if (qrData.SC != StaticClassSyncData.Station_StationCode)
                {
                    return new QRScanResult() { IsSuccess = false, Message = "Invalid Station: Check-in Station is not the same as ticket." };
                }
                //Check Expiry
                if (DateTime.Now > qrData.StringToDateTime(qrData.ET) || DateTime.Now < qrData.StringToDateTime(qrData.CD))
                {
                    return new QRScanResult() { IsSuccess = false, Message = "Ticket is expired" };
                }
                //Check TID in local db 
                // 0 concatenated at the end of TID to show it as checkin mode 
                if(CheckTIDAntiReuse(qrData.TID + "1"))
                {
                    return new QRScanResult() { IsSuccess = false, Message = "Ticket already used for check-in." };
                }

                //Process checkin
                return _processTicketScan(qrData,true);
            }
            catch (Exception ex)
            {
                return new QRScanResult() { IsSuccess = false, Message = "Unable to process check-in: " + ex.Message };
            }
        }

        public QRScanResult ScanQRCheckout(QRScanModel qrData)
        {
            try
            {
                //Check TID in local db
                // 1 is concatenated at end of TID to show it as checout mode
                if (CheckTIDAntiReuse(qrData.TID + "0"))
                {
                    return new QRScanResult() { IsSuccess = false, Message = "Ticket is already used." };
                }

                //Process checkout
                return _processTicketScan(qrData, false);
            }
            catch (Exception ex)
            {
                return new QRScanResult() { IsSuccess = false, Message = "Unable to process check-out: " + ex.Message };
            }
        }

        
        private QRScanResult _processTicketScan(QRScanModel QRScanVM, bool IsCheckin)
        {
            try
            {
                var txnID = QRScanVM.TID;

                //Create a checkin transaction and add to local db.
                var qrTxnModel = new QRTxnModel();
                qrTxnModel.DeviceCode = StaticClassSyncData.POS_ID;
                qrTxnModel.FareAmount = QRScanVM.FA;
                qrTxnModel.IsUploaded = false;
                qrTxnModel.StationCode = QRScanVM.SC;
                qrTxnModel.TID = QRScanVM.TID;
                qrTxnModel.TxnDateTime = DateTime.Now.ToString();
                qrTxnModel.TxnType = IsCheckin ? "checkin" : "checkout";

                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    cnn.Execute("insert into tblQRTxn (TID,StationCode,DeviceCode,FareAmount,TxnType,IsUploaded,TxnDateTime) values (@TID,@StationCode,@DeviceCode,@FareAmount,@TxnType,@IsUploaded,@TxnDateTime)",qrTxnModel);
                }
                
                //Send txn to server
                _sendTXNtoServer(qrTxnModel);
                //Add TID in local db
                _addQRIDtoLocalDB(txnID + Convert.ToInt32(IsCheckin).ToString());
                return new QRScanResult() { IsSuccess = true, Message = "Ticket scanned successfully." };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void _addQRIDtoLocalDB(string qrID)
        {
            using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
            {
                cnn.Execute("insert into tblQRID (TID) values ('" + qrID +"')");
            }
        }

        private void _sendTXNtoServer(QRTxnModel qrTxnModel)
        {
            try
            {
                var url = ServiceURLSettingManager.ServiceURLManager.GetQRTxnCollectionServiceURL();
                object body = new
                {
                    QRNumber = qrTxnModel.TID,
                    Type = "qr",
                    qrTxnModel.StationCode,
                    qrTxnModel.DeviceCode,
                    DirectionCode = qrTxnModel.TxnType == "checkin" ? "IN" : "OUT",
                    RouteCode = StaticClassSyncData.Station_RouteID,
                    BusNumber = "00000",
                    DeviceType = "POS",
                    TransactionType = qrTxnModel.TxnType,
                    TransactionDateTime = qrTxnModel.TxnDateTime,
                    qrTxnModel.FareAmount,
                    QrId = qrTxnModel.TID
                };

                var json1 = JsonConvert.SerializeObject(body);
                string inputJson = (new JavaScriptSerializer()).Serialize(body);
                using (WebClient client = new WebClient())
                {
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    var res = client.UploadString(url, json1);
                    var response = JsonConvert.DeserializeObject<dynamic>(res);

                    if(response["IsSuccess"]==true)
                    {
                        using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                        {
                            cnn.Execute(string.Format("update tblQRTxn set IsUploaded=1 where TID='{0}' and TxnType='{1}'", qrTxnModel.TID,qrTxnModel.TxnType));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
            //Send txn to server
            //Get acknowledgment response from server set local bit to true

        }

        public static bool CheckTIDAntiReuse(string tID)
        {
            bool result = false;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    result = cnn.ExecuteScalar<bool>("SELECT EXISTS(SELECT 1 FROM tblQRID WHERE TID='" + tID + "')");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return result;
        }
    }
}
