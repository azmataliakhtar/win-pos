﻿using BLL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Constants
{
    public static class Constants
    {
        public static string ConnectionString(string id = "POSConnectionString")
        {
            return "Data Source="+ System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "POS_DB.db"; //ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
        public static void clearStaticObject()
        {

            StaticClassSyncData.Organization_Address = null;
            StaticClassSyncData.Organization_Fax = null;
            StaticClassSyncData.Organization_Mobile = null;
            StaticClassSyncData.Organization_Name = null;
            StaticClassSyncData.Organization_OrganizationTypeID = null;
            StaticClassSyncData.Organization_Remarks = null;
            StaticClassSyncData.Organization_Skype = null;
            StaticClassSyncData.Organization_Telephone = null;
            StaticClassSyncData.PersonInchargeID = null;
            StaticClassSyncData.POS_Code = null;
            StaticClassSyncData.POS_Description = null;
            StaticClassSyncData.POS_ID = null;
            StaticClassSyncData.POS_IP = null;
            StaticClassSyncData.POS_MaxSaleLimit = null;
            StaticClassSyncData.POS_MaxTopUp = null;
            StaticClassSyncData.POS_MinTopUp = null;
            StaticClassSyncData.POS_Name = null;
            StaticClassSyncData.POS_PSAM_MAC = null;
            StaticClassSyncData.POS_StationID = null;
            StaticClassSyncData.PreviousStationID = null;
            StaticClassSyncData.Station_Details = null;
            StaticClassSyncData.Station_Latitude = null;
            StaticClassSyncData.Station_Longitude = null;
            StaticClassSyncData.Station_Name = null;
            StaticClassSyncData.Station_OrganizationID = null;
            StaticClassSyncData.Station_RouteID = null;
            StaticClassSyncData.IsSuccess = null;
            StaticClassSyncData.ZipCode = null;

            // Sy

            StaticClassSyncData.Organization_Address = null;
            StaticClassSyncData.Organization_Fax = null;
            StaticClassSyncData.Organization_Mobile = null;
            StaticClassSyncData.Organization_Name = null;
            StaticClassSyncData.Organization_OrganizationTypeID = null;
            StaticClassSyncData.Organization_Remarks = null;
            StaticClassSyncData.Organization_Skype = null;
            StaticClassSyncData.Organization_Telephone = null;
            StaticClassSyncData.PersonInchargeID = null;
            StaticClassSyncData.POS_Code = null;
            StaticClassSyncData.POS_Description = null;
            StaticClassSyncData.POS_ID = null;
            StaticClassSyncData.POS_IP = null;
            StaticClassSyncData.POS_MaxSaleLimit = null;
            StaticClassSyncData.POS_MaxTopUp = null;
            StaticClassSyncData.POS_MinTopUp = null;
            StaticClassSyncData.POS_Name = null;
            StaticClassSyncData.POS_PSAM_MAC = null;
            StaticClassSyncData.POS_StationID = null;
            StaticClassSyncData.PreviousStationID = null;
            StaticClassSyncData.Station_Details = null;
            StaticClassSyncData.Station_Latitude = null;
            StaticClassSyncData.Station_Longitude = null;
            StaticClassSyncData.Station_Name = null;
            StaticClassSyncData.Station_OrganizationID = null;
            StaticClassSyncData.Station_RouteID = null;
            StaticClassSyncData.IsSuccess = null;
            StaticClassSyncData.ZipCode = null;

            // UserData
            StaticClassUserData.Heading = null;
            StaticClassUserData.status = null;
            StaticClassUserData.IsSuccess = null;
            StaticClassUserData.UserID = null;
            StaticClassUserData.OrganizationID = null;
            StaticClassUserData.DepartmentID = null;
            StaticClassUserData.DesignationID = null;
            StaticClassUserData.TeamID = null;
            StaticClassUserData.UserTypeID = null;
            StaticClassUserData.Name = null;
            StaticClassUserData.OrganizationName = null;
            StaticClassUserData.DepartmentName = null;
            StaticClassUserData.TeamName = null;
            StaticClassUserData.CNIC = null;
            StaticClassUserData.Email = null;
            StaticClassUserData.Password = null;
            StaticClassUserData.UserTypeName = null;
            StaticClassUserData.GenderID = null;
            StaticClassUserData.GenderName = null;
            StaticClassUserData.Mobile = null;
            StaticClassUserData.DateOfBirth = null;
            StaticClassUserData.IsAllowLogin = null;
            StaticClassUserData.LastLogin = null;
        }
        public static string MacAddress()
        {
            return ShowNetworkInterfaces(); //ConfigurationManager.AppSettings["MACAddress"];
        }


        public static string ShowNetworkInterfaces()
        {
            string AddressString = "";
            IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            
                   
            if (nics == null || nics.Length < 1)
            {
                
                return "Error Reading MAC";
            }

            
            foreach (NetworkInterface adapter in nics)
            {
                IPInterfaceProperties properties = adapter.GetIPProperties(); //  .GetIPInterfaceProperties();
               
                PhysicalAddress address = adapter.GetPhysicalAddress();
                byte[] bytes = address.GetAddressBytes();
                for (int i = 0; i < bytes.Length; i++)
                {

                    AddressString += bytes[i].ToString("X2");
                    // Insert a hyphen after each byte, unless we are at the end of the
                    // address.
                    if (i != bytes.Length - 1)
                    {
                        
                    }
                }
                return AddressString;
            }

            return "Error Reading MAC";
        }
    }
}