﻿namespace BLL.Models
{

    //{
    //    "name": null,
    //    "heading": null,
    //    "summary": null,
    //    "message": "",
    //    "exceptionMessage": "",
    //    "status": 200,
    //    "isSuccess": true,
    //    "responseObject": {
    //        "userId": "eb42b7f0-cde7-402e-af6c-a648b147d0fa",
    //        "userName": "N Five",
    //        "deviceCode": "be59e473-8265-4ee9-8480-09645041a375",
    //        "deviceMacAddress": "F81654E53ADF",
    //        "shiftStartTime": "4/25/2022 3:11:58 PM",
    //        "shiftEndTime": "4/25/2022 4:00:20 PM",
    //        "generationTime": "4/25/2022 4:43:13 PM",
    //        "shiftAmount": "500",
    //        "totalRevenue": "",
    //        "totalQrSales": "0",
    //        "qrSalesAmount": "",
    //        "totalCardSales": null,
    //        "cardSalesAmount": null,
    //        "totalTopUp": null,
    //        "totalTopUpAmount": null,
    //        "remarks": "Shift Ended"
    //    }
    //}

    public class RootobjectReceipt
    {
      
        public  string name { get; set; }
        public string heading { get; set; }
        public string summary { get; set; }
        public string message { get; set; }
        public string exceptionMessage  { get; set; }

        public  int status { get; set; }
        public bool isSuccess { get; set; }
        public ReceiptResponseObject responseObject { get; set; }
    }
    public class ReceiptResponseObject
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string deviceCode { get; set; }
        public string deviceMacAddress { get; set; }
        public string ShiftStartTime { get; set; }
        public string ShiftEndTime { get; set; }
        public string generationTime { get; set; }
        public string ShiftAmount { get; set; }
        public string totalRevenue { get; set; }
        public string totalQrSales { get; set; }
        public string qrSaleAmount { get; set; }
        public string totalCardSales { get; set; }
        public string cardSalesAmount { get; set; }
        public string totalTopUp { get; set; }
        public string totalTopUpAmount { get; set; }
        public string remarks { get; set; }

    }
}
