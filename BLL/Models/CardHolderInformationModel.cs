﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class CardHolderInformationModel
    {
        public class RootobjectCardHolderInformation
        {
            public  string Heading { get; set; }
            public  int status { get; set; }
            public  bool IsSuccess { get; set; }
            public  string ExceptionMessage { get; set; }
            public string Message { get; set; }
            public ResponseObject responseObject { get; set; }

        }
        public class ResponseObject
        { 

            public string SerialNumber { get; set; }

            public string CardNumber { get; set; }

            public string CardHolderName { get; set; }

            public string Email { get; set; }

            public string Phone { get; set; }

            public string Address { get; set; }
   
            public string CNIC { get; set; }
            public string Gender { get; set; }

            public string CardTypeID { get; set; }

            public string OrganizationID { get; set; }
            public DateTime InsertedDateTime { get; set; }
            public string InsertedBy { get; set; }
            public int CardStatus { get; set; }
            public DateTime InitializedDateTime { get; set; }
            public DateTime UpdateDateTime { get; set; }
            public int TopupBalance  { get; set; }
            public string CardType { get; set; }
            public DateTime expiryDate { get; set; }
            public int CurrentBalance { get; set; }
            public string Name { get; set; }
        }

    }
}
