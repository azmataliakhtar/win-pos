﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
   public class InitializeCard
    {
        public string SerialNumber { get; set; }
        public string CardNumber { get; set; }
        public DateTime InitializedDateTime { get; set; }
        public int CardStatus { get; set; }

    }
}
