﻿namespace BLL.Models.ViewModel
{
    public class ShiftViewModel
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public string UserID { get; set; }
        public bool IsActiveShift { get; set; }
        public string ShiftStartTime { get; set; }
        public string ShiftEndTime { get; set; }
        public int CashInHand { get; set; }
        public bool IsUploadedToServer { get; set; }
        public string IsActiveShiftString { get { return IsActiveShift.ToString(); } }

        public string IsUploadedToServerString { get { return IsUploadedToServer.ToString(); } }
    }
}
