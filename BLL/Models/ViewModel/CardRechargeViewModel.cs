﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
    public class CardRechargeViewModel
    {
        public Guid CardRechargeID { get; set; }
        public string TransactionRef { get; set; }
        public string CardNumber { get; set; }
        public string SerialNumber { get; set; }
        public int RechargeAmount { get; set; }
        public int BalanceAfter { get; set; }
        public string POS_Code { get; set; }
        public string UserID { get; set; }
        public DateTime TransactionDateTime { get; set; }
        //TransactionType Topup,Transfer,Refund
        public string TransactionType { get; set; }
        public bool Status { get; set; }
        // PaymentMethod = Cash,Easypaisa etc
        public string PaymentMethod { get; set; }
        public string PaymentTxnRef { get; set; }
    }
}
