﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
    class QRTxnModel
    {
        public string TID { get; set; }
        public string StationCode { get; set; }
        public string DeviceCode { get; set; }
        public string FareAmount { get; set; }
        public string TxnType { get; set; }
        public bool IsUploaded { get; set; }
        public string TxnDateTime { get; set; }
    }
}
