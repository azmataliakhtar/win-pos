﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
   public class IssueCard
    {
        public string SerialNumber { get; set; }
        public string OrganizationID { get; set; }
        public string CardNumber { get; set; }
        public string IssuedBy { get; set; }
        public string Issuedat { get; set; }
        public string CardPrice { get; set; }
        public DateTime IssuedDateTime { get; set; }
        public string CardType { get; set; }
        public string Name { get; set; }
        public string CNIC { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public int CardtopupAmount { get; set; }
        public bool IsPersonalized { get; set; }
        public DateTime ExpiryTime { get; set; }

    }
}
