﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
    public class URLSettingViewModel
    {
        public int ID { get; set; }
        public string IPAddress { get; set; }
        public int PortNumber { get; set; }
    }
}
