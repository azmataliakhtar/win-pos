﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
   public class ConfigurationViewModel
    {
        public int ID { get; set; }
        public  string POS_ID { get; set; }
        public  string POS_StationID { get; set; }
        public  string POS_Name { get; set; }
        public  string POS_Code { get; set; }
        public  string POS_MaxSaleLimit { get; set; }
        public  string POS_IP { get; set; }
        public  string POS_MinTopUp { get; set; }
        public  string POS_MaxTopUp { get; set; }
        public  string POS_Description { get; set; }
        public  string POS_PSAM_MAC { get; set; }
        public  string Station_OrganizationID { get; set; }
        public  string Station_PersonInchargeID { get; set; }
        public  string PreviousStationID { get; set; }
        public  string Station_RouteID { get; set; }
        public  string Station_Latitude { get; set; }
        public  string Station_Longitude { get; set; }
        public  string Station_Name { get; set; }
        public  string Station_Details { get; set; }
        public  string Station_StationCode { get; set; }
        public  string Station_SerialNumber { get; set; }
        public  string Station_MaxSaleLimit { get; set; }
        public  string Organization_OrganizationTypeID { get; set; }
        public  string Organization_Name { get; set; }
        public  string Organization_ZipCode { get; set; }
        public  string PersonInchargeID { get; set; }
        public  string Organization_Mobile { get; set; }
        public  string Organization_Telephone { get; set; }
        public  string Organization_Skype { get; set; }
        public  string Organization_Fax { get; set; }
        public  string Organization_Address { get; set; }
        public  string Organization_Remarks { get; set; }
        public string POS_FareAmount { get; set; }
        public string POS_TicketValidityMinutes { get; set; }
        public string SyncDateTime { get; set; }
        public string Mac_Address { get; set; }
    }
}
