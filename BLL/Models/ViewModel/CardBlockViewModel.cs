﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ViewModel
{
    public class CardBlockViewModel
    {
        public Guid CardRechargeID { get; set; }
        public string CardNumber { get; set; }
        public string SerialNumber { get; set; }
        public string POS_Code { get; set; }
        public string UserID { get; set; }
        public string Reason { get; set; }
    }
}
