﻿using System.Collections.Generic;
using System.Linq;

namespace BLL.Models.ViewModel
{
    public class StationsViewModel
    {
        public string OrganizationCode { get; set; }
        public List<StationViewModel> Stations { get; set; }
        public StationViewModel SelectedStation { get; set; }

        public StationsViewModel(string organizationCode,StationsDataModel dataModel, string stationCode)
        {
            OrganizationCode = organizationCode;
            Stations = new List<StationViewModel>();
            foreach (var item in dataModel.ResponseObject)
            {
                var station = new StationViewModel()
                {
                    ID = item.StationID,
                    Code = item.StationCode,
                    Name = item.Name,
                    RouteID = item.RouteID
                };
                Stations.Add(station);
            }

            SelectedStation = Stations.FirstOrDefault(x => x.Code == stationCode);
        }
    }

    public class StationViewModel
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public string Code { get; set; }
        public string RouteID { get; set; }
    }
}
