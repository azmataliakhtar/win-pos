﻿namespace BLL.Models.ViewModel
{
    public class QRScanResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
