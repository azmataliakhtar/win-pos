﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ChangePasswordDataModel
    {
        public class RootobjectChangePassword
        {

            public  string Heading { get; set; }
 
            public  int status { get; set; }
            public  bool IsSuccess { get; set; }
            public string Message { get; set; }
            public ResponseObjectChangePassword responseObject { get; set; }
        }
        public class ResponseObjectChangePassword
        {

        }
    }
}
