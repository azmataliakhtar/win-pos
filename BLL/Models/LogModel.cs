﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Model
{
    public class LogModel
    {
        public string UserID { get; set; }
        public string ActionPerform { get; set; }
        public string LoginDate { get; set; }
        public string IP_Address { get; set; }
        public string UserName { get; set; }
    }
}
