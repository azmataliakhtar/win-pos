﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BLL.Models
{
    public class StationsDataModel
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Heading")]
        public string Heading { get; set; }

        [JsonProperty("Summary")]
        public string Summary { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("ExceptionMessage")]
        public object ExceptionMessage { get; set; }

        [JsonProperty("Status")]
        public int Status { get; set; }

        [JsonProperty("IsSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("ResponseObject")]
        public List<StationDataModel> ResponseObject { get; set; }
    }

    public class StationDataModel
    {
        [JsonProperty("StationID")]
        public string StationID { get; set; }

        [JsonProperty("OrganizationID")]
        public string OrganizationID { get; set; }

        [JsonProperty("PersonInChargeID")]
        public string PersonInChargeID { get; set; }

        [JsonProperty("PreviousStationID")]
        public string PreviousStationID { get; set; }

        [JsonProperty("RouteID")]
        public string RouteID { get; set; }

        [JsonProperty("TotalEquipments")]
        public int TotalEquipments { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("PreviousStationName")]
        public object PreviousStationName { get; set; }

        [JsonProperty("RouteName")]
        public object RouteName { get; set; }

        [JsonProperty("Details")]
        public object Details { get; set; }

        [JsonProperty("StationCode")]
        public string StationCode { get; set; }

        [JsonProperty("SerialNumber")]
        public string SerialNumber { get; set; }

        [JsonProperty("MaxSaleLimit")]
        public double MaxSaleLimit { get; set; }

        [JsonProperty("CreationDate")]
        public DateTime CreationDate { get; set; }

        [JsonProperty("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }

        [JsonProperty("CreatedBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("LastUpdatedBy")]
        public string LastUpdatedBy { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }
    }
}
