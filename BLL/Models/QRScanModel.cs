﻿using System;

namespace BLL.Models
{
    //"TID":"2022041401364808931","FA":40.0,"CD":"Apr 14, 2022 01:36:48 PM","ET":"Apr 14, 2022 02:06:48 PM","SC":"0020","OC":"ORG-002"
    public class QRScanModel
    {
        public string TID { get; set; }
        public string FA { get; set; }
        public string CD { get; set; }
        public string ET { get; set; }
        public string SC { get; set; }
        public string OC { get; set; }

        public DateTime StringToDateTime(string dateTime)
        {
            try
            {
                DateTime DT = Convert.ToDateTime(dateTime);
                return DT;
            }
            catch (FormatException)
            {
                throw new InvalidCastException();
            }
        }
    }
}
