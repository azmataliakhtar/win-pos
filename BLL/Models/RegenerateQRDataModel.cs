﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
   public class RegenerateQRDataModel
    {
        public class RootobjectRegenrateQR
        {

            public int errorCode { get; set; }

            public bool status { get; set; }
            public string message { get; set; }
            public ResponseObject responseObject { get; set; }
        }
        public class ResponseObject
        {
            public string SingleJourneyTickedID { get; set; }
            public string poid { get; set; }
            public string qrNumber { get; set; }
            public string StationCode { get; set; }
            public string organizationCode { get; set; }
            public string ecnryptedTicketID { get; set; }
            public string fareAmount { get; set; }
            public string IsEnter { get; set; }
            public string scanByAppID { get; set; }
            public string remarks { get; set; }
            public string creationDate { get; set; }
            public string expiryTime { get; set; }
            public string scanTime { get; set; }
            public string updatedTime { get; set; }

            public string createdBy { get; set; }
            public string lastUpdatedBy { get; set; }
            public bool isActive { get; set; }
            public string fileAsBase64 { get; set; }
            public int ticketCount { get; set; }

        }
    }
}
