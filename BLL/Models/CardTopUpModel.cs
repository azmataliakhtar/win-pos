﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class CardTopUpModel
    {
        public class RootobjecCardToup
        {

            public string Heading { get; set; }

            public int status { get; set; }

            public bool IsSuccess { get; set; }
            public string ExceptionMessage { get; set; }
            public string Message { get; set; }
            public ResponseObject responseObject { get; set; }

        }
        public class ResponseObject
        {

            public string CardTypeID { get; set; }

            public string OrganizationID { get; set; }

            public string Name { get; set; }

            public string Code { get; set; }

            public bool IsPersonalized { get; set; }

            public string CardPrice { get; set; }

            public string MinimumTopup { get; set; }

            public string MaximumTopup { get; set; }

            public string MaximumBalance { get; set; }

            public string CardValidity { get; set; }

            public string Discount { get; set; }

            public string CreationDate { get; set; }

            public string UpdatedDate { get; set; }

            public string CreatedBy { get; set; }

            public string LastUpdatedBy { get; set; }



        }
    }
}
