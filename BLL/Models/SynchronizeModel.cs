﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class SynchronizeModel
    {
        public class RootobjectSync
        {
            [JsonProperty("Heading")]
            public static string Heading { get; set; }
            [JsonProperty("status")]
            public static int status { get; set; }
            [JsonProperty("IsSuccess")]
            public static bool IsSuccess { get; set; }
            public ResponseObject responseObject { get; set; }

        }
        public class ResponseObject
        {
            public PosDetails posDetails { get; set; }
        }
        public class PosDetails
        {
            [JsonProperty("POS_ID")]
            public string POS_ID { get; set; }
            [JsonProperty("POS_StationID")]
            public string POS_StationID { get; set; }
            [JsonProperty("POS_Name")]
            public string POS_Name { get; set; }
            [JsonProperty("POS_Code")]
            public string POS_Code { get; set; }
            [JsonProperty("POS_MaxSaleLimit")]
            public string POS_MaxSaleLimit { get; set; }
            [JsonProperty("POS_IP")]
            public string POS_IP { get; set; }
            [JsonProperty("POS_MinTopUp")]
            public string POS_MinTopUp { get; set; }
            [JsonProperty("POS_MaxTopUp")]
            public string POS_MaxTopUp { get; set; }
            [JsonProperty("POS_Description")]
            public string POS_Description { get; set; }
            [JsonProperty("POS_PSAM_MAC")]
            public string POS_PSAM_MAC { get; set; }
            [JsonProperty("Station_OrganizationID")]
            public string Station_OrganizationID { get; set; }
            [JsonProperty("Station_PersonInchargeID")]
            public string Station_PersonInchargeID { get; set; }
            [JsonProperty("PreviousStationID")]
            public string PreviousStationID { get; set; }
            [JsonProperty("Station_RouteID")]
            public string Station_RouteID { get; set; }
            [JsonProperty("Station_Latitude")]
            public string Station_Latitude { get; set; }
            [JsonProperty("Station_Longitude")]
            public string Station_Longitude { get; set; }
            [JsonProperty("Station_Name")]
            public string Station_Name { get; set; }
            [JsonProperty("Station_Details")]
            public string Station_Details { get; set; }
            [JsonProperty("Station_StationCode")]
            public string Station_StationCode { get; set; }
            [JsonProperty("Station_SerialNumber")]
            public string Station_SerialNumber { get; set; }
            [JsonProperty("Station_MaxSaleLimit")]
            public string Station_MaxSaleLimit { get; set; }
            [JsonProperty("Organization_OrganizationTypeID")]
            public string Organization_OrganizationTypeID { get; set; }
            [JsonProperty("Organization_Name")]
            public string Organization_Name { get; set; }
            [JsonProperty("ZipCode")]
            public string ZipCode { get; set; }
            [JsonProperty("PersonInchargeID")]
            public string PersonInchargeID { get; set; }
            [JsonProperty("Organization_Mobile")]
            public string Organization_Mobile { get; set; }
            [JsonProperty("Organization_Telephone")]
            public string Organization_Telephone { get; set; }
            [JsonProperty("Organization_Skype")]
            public string Organization_Skype { get; set; }
            [JsonProperty("Organization_Fax")]
            public string Organization_Fax { get; set; }
            [JsonProperty("Organization_Address")]
            public string Organization_Address { get; set; }
            [JsonProperty("Organization_Remarks")]
            public string Organization_Remarks { get; set; }
            [JsonProperty("pos_TicketValidityMinutes")]
            public string pos_TicketValidityMinutes { get; set; }
            [JsonProperty("POS_FareAmount")]
            public string POS_FareAmount { get; set; }


        }

    }
}
