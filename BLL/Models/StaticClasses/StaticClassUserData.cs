﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public static class StaticClassUserData
    {
        public static string Heading { get; set; }
        public static int? status { get; set; }
        public static string IsSuccess { get; set; }
        //user Data
        public static string UserID { get; set; }
        public static string OrganizationID { get; set; }
        public static string DepartmentID { get; set; }
        public static string DesignationID { get; set; }
        public static string TeamID { get; set; }
        public static string UserTypeID { get; set; }
        public static string Name { get; set; }
        public static string OrganizationName { get; set; }
        public static string DepartmentName { get; set; }
        public static string TeamName { get; set; }
        public static string CNIC { get; set; }
        public static string Email { get; set; }
        public static string Password { get; set; }
        public static string UserTypeName { get; set; }
        public static string GenderID { get; set; }
        public static string GenderName { get; set; }
        public static string Mobile { get; set; }
        public static string DateOfBirth { get; set; }
        public static bool? IsAllowLogin { get; set; }
        public static string LastLogin { get; set; }
    }
}
