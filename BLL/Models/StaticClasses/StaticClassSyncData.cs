﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class StaticClassSyncData
    {
        public static int ID { get; set; }
        public static string IsSuccess { get; set; }
        public static string POS_ID { get; set; }
        public static string POS_StationID { get; set; }
        public static string POS_Name { get; set; }
        public static string POS_Code { get; set; }
        public static string POS_MaxSaleLimit { get; set; }
        public static string POS_IP { get; set; }
        public static string POS_MinTopUp { get; set; }
        public static string POS_MaxTopUp { get; set; }
        public static string POS_Description { get; set; }
        public static string POS_PSAM_MAC { get; set; }
        public static string Station_OrganizationID { get; set; }
        public static string Station_PersonInchargeID { get; set; }
        public static string PreviousStationID { get; set; }
        public static string Station_RouteID { get; set; }
        public static string Station_Latitude { get; set; }
        public static string Station_Longitude { get; set; }
        public static string Station_Name { get; set; }
        public static string Station_Details { get; set; }
        public static string Station_StationCode { get; set; }
        public static string Station_SerialNumber { get; set; }
        public static string Station_MaxSaleLimit { get; set; }
        public static string Organization_OrganizationTypeID { get; set; }
        public static string Organization_Name { get; set; }
        public static string ZipCode { get; set; }
        public static string PersonInchargeID { get; set; }
        public static string Organization_Mobile { get; set; }
        public static string Organization_Telephone { get; set; }
        public static string Organization_Skype { get; set; }
        public static string Organization_Fax { get; set; }
        public static string Organization_Address { get; set; }
        public static string Organization_Remarks { get; set; }
        public static string pos_TicketValidityMinutes { get; set; }
        public static string POS_FareAmount { get; set; }
        public static string SyncDateTime { get; set; }
    }
}
