﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.StaticClasses
{
   public class IssueCardStatic
    {
        public static string CardTypeID { get; set; }
        public static string OrganizationID { get; set; }
        public static string Name { get; set; }
        public static string Code { get; set; }
        public static bool IsPersonalized { get; set; }
        public static string CardPrice { get; set; }
        public static string MinimumTopup { get; set; }
        public static string MaximumTopup { get; set; }
        public static string MaximumBalance { get; set; }
        public static string CardValidity { get; set; }
        public static string Discount { get; set; }
        public static string CreationDate { get; set; }
        public static string UpdatedDate { get; set; }
        public static string CreatedBy { get; set; }
        public static string LastUpdatedBy { get; set; }
        public static string IsActive { get; set; }
    }
}
