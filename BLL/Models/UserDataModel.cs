﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BLL.Model
{
    public class Rootobject
    {
        [JsonProperty("Heading")]
        public static string Heading { get; set; }
        [JsonProperty("status")]
        public static int status { get; set; }
        [JsonProperty("IsSuccess")]
        public static bool IsSuccess { get; set; }
        public ResponseObject responseObject { get; set; }
    }
    public class ResponseObject
    {
        public UserInfo userInfo { get; set; }
        public List<UserRole> userRoles { get; set; }
        public List<UserAction> userActions { get; set; }
    }
    public class UserInfo
    {
        [JsonProperty("UserID")]
        public string UserID { get; set; }
        [JsonProperty("OrganizationID")]
        public string OrganizationID { get; set; }
        [JsonProperty("DepartmentID")]
        public string DepartmentID { get; set; }
        [JsonProperty("DesignationID")]
        public string DesignationID { get; set; }
        [JsonProperty("TeamID")]
        public string TeamID { get; set; }
        [JsonProperty("UserTypeID")]
        public string UserTypeID { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("OrganizationName")]
        public string OrganizationName { get; set; }
        [JsonProperty("DepartmentName")]
        public string DepartmentName { get; set; }
        [JsonProperty("TeamName")]
        public string TeamName { get; set; }
        [JsonProperty("CNIC")]
        public string CNIC { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Password")]
        public string Password { get; set; }
        [JsonProperty("UserTypeName")]
        public string UserTypeName { get; set; }
        [JsonProperty("GenderID")]
        public string GenderID { get; set; }
        [JsonProperty("GenderName")]
        public string GenderName { get; set; }
        [JsonProperty("Mobile")]
        public string Mobile { get; set; }
        [JsonProperty("DateOfBirth")]
        public string DateOfBirth { get; set; }
        [JsonProperty("IsAllowLogin")]
        public bool IsAllowLogin { get; set; }


    }
    public class UserRole
    {
        public string RoleName { get; set; }
    }
    public class UserAction
    {
        [JsonProperty("ActionName")]
        public string ActionName { get; set; }
        [JsonProperty("ActionCode")]
        public string ActionCode { get; set; }
    }
}
