﻿using BLL.Models.ViewModel;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ServiceURLSettingManager
{
    public class ServiceURLManager
    {


        public static DataTable LoadURLSettings()
        {
            SQLiteDataReader result;
            DataTable datatable = new DataTable();

            try
            {
                using (SQLiteConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    cnn.Open();

                    SQLiteCommand command = new SQLiteCommand("SELECT * FROM tblServiceURLSettings", cnn);
                    result = command.ExecuteReader();
                    datatable.Load(result);

                    cnn.Close();
                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            return datatable;
        }
        public static int UpdateURLSettings(URLSettingViewModel model)
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    Result = cnn.Execute("Update tblServiceURLSettings set IPAddress=@IPAddress,PortNumber=@PortNumber Where ID=@ID ", model);

                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }


            return Convert.ToInt32(Result);
        }

        public static string GetShiftServiceURL()
        {
            DataTable dt = LoadURLSettings();
            DataRow[] dr = dt.Select("ServiceName = 'ShiftServiceURL'");
            var IP = dr[0]["IPAddress"].ToString();
            var PortNumber = dr[0]["PortNumber"].ToString();
            return "http://" + IP + ":" + PortNumber + "/api/ShiftEvent";
        }

        public static string GetQRTxnCollectionServiceURL()
        {
            DataTable dt = LoadURLSettings();
            DataRow[] dr = dt.Select("ServiceName = 'DataCollectionServiceURL'");
            var IP = dr[0]["IPAddress"].ToString();
            var PortNumber = dr[0]["PortNumber"].ToString();
            return "http://" + IP + ":" + PortNumber + "/api/DataCollection?RecordType=QrTransaction";
        }
    }
}
