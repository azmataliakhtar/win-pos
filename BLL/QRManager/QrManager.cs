﻿using BLL.Models;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BLL.QRManager
{
    public class QrManager
    {
        public RootobjectQR GetQR(string appID, string UserName, string Password, string CurrentUser)
        {
            RootobjectQR data = new RootobjectQR();
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'QRServiceURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                object body = new
                {
                    stationCode = StaticClassSyncData.Station_StationCode,
                    organizationCode = StaticClassUserData.OrganizationID,
                    ValidityMinutes = StaticClassSyncData.pos_TicketValidityMinutes,
                    FareAmount = StaticClassSyncData.POS_FareAmount//,
                    //POSID = StaticClassSyncData.POS_ID
                };
                var json1 = JsonConvert.SerializeObject(body);
                string inputJson = (new JavaScriptSerializer()).Serialize(body);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string apiUrl = BaseURL + "ticket/GetQR?body=" + json1 + "&appid=" + appID + "&username=" + UserName + "&password=" + Password + "&currentUserID=" + CurrentUser;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectQR>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            return data;
        }

        public StationsDataModel GetStationsList(string organizationID)
        {
            StationsDataModel data = new StationsDataModel();
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/api/";

                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string apiUrl = BaseURL + "/Application/OrganizationStation?organizationID=" + organizationID;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<StationsDataModel>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            return data;
        }
    }
}
