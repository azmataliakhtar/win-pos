﻿using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System.Data;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.InitializeCardModel;

namespace BLL.InitializeCardManager
{
    public class InitializeCardManager
    {
        public RootobjectInitializeCard InitializeCard(InitializeCard initializeCard)
        {
            RootobjectInitializeCard data = new RootobjectInitializeCard();
           
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "Card/InitializeCard";
                // string apiUrl = "https://localhost:44352/Api/Card/InitializeCard";
                object input = new
                {
                    CardNumber = initializeCard.CardNumber,
                    SerialNumber = initializeCard.SerialNumber
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.UploadString(apiUrl, inputJson);
                data = JsonConvert.DeserializeObject<RootobjectInitializeCard>(json);
            }
            catch (System.Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
        public RootobjectCardHolderInformation GetCardHolderInformationBySerialNumber(string SerialNumber)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();
            try
            {
               
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "Card/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                // string apiUrl = "https://localhost:44352/Api/Card/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                object input = new
                {
                    SerialNumber = SerialNumber
                };
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
            }
            catch (System.Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }

    }
}
