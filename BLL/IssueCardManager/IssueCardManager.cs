﻿using BLL.Models;
using BLL.Models.StaticClasses;
using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.IssueCardModel;

namespace BLL.IssueCardManager
{
    public class IssueCardManager
    {
        public RootobjectIssueCard GetCardTypeByOrganizationID(string OrganizationID)
        {
            RootobjectIssueCard data = new RootobjectIssueCard();
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "application/GetCardTypes?organizationID=" + OrganizationID;
                object input = new
                {
                    OrganizationID = OrganizationID
                };
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectIssueCard>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }


        public RootobjectCardHolderInformation GetCardHolderInformation(string CardNumber)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();

            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "Card/GetCardHolderInformation?CardNumber=" + CardNumber;
                // string apiUrl = "https://localhost:44352/Api/Card/GetCardHolderInformation?CardNumber="+CardNumber;
                object input = new
                {
                    CardNumber = CardNumber
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
                string IsSuccess = JObject.Parse(json)["IsSuccess"].ToString();
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }

        public RootobjectCardHolderInformation IssueCard(IssueCard issueCard)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();

            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "/Card/IssueCard";
                //string apiUrl = "https://localhost:44352/Api/Card/IssueCard";
                object input = new
                {
                    CardNumber = issueCard.CardNumber,
                    SerialNumber = issueCard.SerialNumber,
                    Issuedat = issueCard.Issuedat,
                    CardType = issueCard.CardType,
                    CardPrice = issueCard.CardPrice,
                    IssuedBy = StaticClassUserData.UserID,
                    Name = issueCard.Name,
                    CNIC = issueCard.CNIC,
                    Phone = issueCard.Phone,
                    Gender = issueCard.Gender,
                    Address = issueCard.Address,
                    TopupAmount = issueCard.CardtopupAmount,
                    Ispersonalized = issueCard.IsPersonalized,
                    ExpiryTime = issueCard.ExpiryTime
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.UploadString(apiUrl, inputJson);
                data = JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }


    }
}
