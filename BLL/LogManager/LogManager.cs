﻿using BLL.Model;
using BLL.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.LogManager
{
    public class LogManager
    {
        public void SaveLog(LogModel logModel)
        {
            using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
            {
                cnn.Execute("insert into tblLog (UserID,ActionPerform,LoginDate,IP_Address,UserName) values(@UserID,@ActionPerform,@LoginDate,@IP_Address,@UserName)", logModel);
            }

        }
        public List<LogModel> LoadLog(string UserID)
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.Constants.ConnectionString()))
                {
                    Result = cnn.Query<LogModel>("select LoginDate from tblLog where UserID='" + UserID + "' order by LoginDate Desc LIMIT 1", new DynamicParameters());

                }

            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            return Result.ToList();
        }
    }

}
