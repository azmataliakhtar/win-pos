﻿using BLL.Models;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BLL.HeartBeatManager
{
    public class HeartBeatManager
    {
        //http://103.173.186.85:9003/api/DataCollection?RecordType=HEARTBEAT
        //  {
                //"DEVICECODE": "0003",
                //"DEVICETYPE": "handheld", //"pos","handheld","tvm","validator"
                //"DEVICENAME": "handheld 003",
                //"DEVICELOCALTIME": "2022-01-31T14:35:51",
                //"FARETABLEVERSION": "2022010505170303",
                //"SOFTWAREVERSION": "1.0",
                //"BLOCKLISTVERSION": "2021091311311825192",
                //"OPERATIONMODE": "BOTH",
                //"ROUTECODE": "1",
                //"STATIONCODE": "0021",
                //"BUSNUMBER": "BUS-0021",
                //"IP": "192.168.232.2",
                //"MACPSAM":"E4:08:E7:EA:96:E9",
                //"ORGANIZATIONCODE":"21282595-6c22-4961-967f-242d32ae54ca"
        //  }
    public void SendHeartBeat(string softwareVersion)
        {
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'HeartbeatServiceURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/api/DataCollection?RecordType=HEARTBEAT";
                object body = new
                {
                    DEVICECODE = StaticClassSyncData.POS_Code,
                    DEVICETYPE = "pos",
                    DEVICENAME = StaticClassSyncData.POS_Name,
                    DEVICELOCALTIME = DateTime.Now.ToString(),
                    FARETABLEVERSION = "",
                    SOFTWAREVERSION = softwareVersion,
                    BLOCKLISTVERSION = "",
                    OPERATIONMODE = "",
                    ROUTECODE = StaticClassSyncData.Station_RouteID,
                    STATIONCODE = StaticClassSyncData.Station_StationCode,
                    BUSNUMBER = "",
                    IP = StaticClassSyncData.POS_IP,
                    MACPSAM = Constants.Constants.MacAddress(),
                    ORGANIZATIONCODE = StaticClassUserData.OrganizationID, 
                };
                var json1 = JsonConvert.SerializeObject(body);
                string inputJson = (new JavaScriptSerializer()).Serialize(body);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                
                string json = client.UploadString(BaseURL,json1);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

        }
    }
}
