﻿using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static BLL.Models.RegenerateQRDataModel;

namespace BLL.RegenerateQRManager
{
    public class RegenerateQRManager
    {
        public RootobjectRegenrateQR RegenerateQR(string appID, string UserName, string Password, string QrNumber)
        {
            RootobjectRegenrateQR data = new RootobjectRegenrateQR();
            try
            {
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'QRServiceURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string apiUrl = BaseURL + "ticket/RegenerateQR?appid=" + appID + "&username=" + UserName + "&password=" + Password + "&qRNumber=" + QrNumber;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectRegenrateQR>(json);
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }

            return data;
        }
    }
}
