﻿using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static BLL.Models.ChangePasswordDataModel;

namespace BLL.ChangePasswordManager
{
    public class ChangePasswordManager
    {
        public RootobjectChangePassword ChangePassword(string UserName, string Password,string NewPassword)
        {
            RootobjectChangePassword data = new RootobjectChangePassword();
            try
            {
                //string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/api/";
                string apiUrl = BaseURL + "Account/ChangePassword";
                object input = new
                {
                    email = UserName,
                    Password = Password,
                    NewPassword = NewPassword
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.UploadString(apiUrl, inputJson);
                data = JsonConvert.DeserializeObject<RootobjectChangePassword>(json);
              
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
    }
}
