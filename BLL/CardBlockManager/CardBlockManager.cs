﻿using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static BLL.Models.CardBlockModel;
using static BLL.Models.CardHolderInformationModel;

namespace BLL.CardBlockManager
{
    public class CardBlockManager
    {
        public RootobjectCardHolderInformation GetCardHolderInformationBySerialNumber(string SerialNumber)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();
            try
            {
                //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "CardBlock/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                // string apiUrl = "https://localhost:44352/Api/CardBlock/GetCardInfobySerial?SerialNumber=" + SerialNumber;
                object input = new
                {
                    SerialNumber = SerialNumber
                };
                // string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.DownloadString(apiUrl);
               data= JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
              
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }

        public RootobjectCardBlock BlockCard(CardBlockViewModel cardBlockViewModel)
        {
            RootobjectCardBlock data = new RootobjectCardBlock();
            try
            {
                //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "CardBlock/BlockCard";
                //string apiUrl = "https://localhost:44352/Api/CardBlock/BlockCard";
                object input = new
                {
                    CardNumber = cardBlockViewModel.CardNumber,
                    SerialNumber = cardBlockViewModel.SerialNumber,
                    POS_Code = cardBlockViewModel.POS_Code,
                    UserID = cardBlockViewModel.UserID,
                    Reason = cardBlockViewModel.Reason
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

                string json = client.UploadString(apiUrl, inputJson);
                //Rootobject data = (new JavaScriptSerializer()).Deserialize<Rootobject>(json);
               data  = JsonConvert.DeserializeObject<RootobjectCardBlock>(json);
            
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
        public RootobjectCardHolderInformation SearchCardByCardNumberCNIC(string CardNumber, string CNIC)
        {
            RootobjectCardHolderInformation data = new RootobjectCardHolderInformation();
            try
            {
                //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "CardBlock/SearchCardByCardNumberCNIC?CardNumber=" + CardNumber + "&CNIC=" + CNIC;
                // string apiUrl = "https://localhost:44352/Api/CardBlock/SearchCardByCardNumberCNIC?CardNumber=" + CardNumber + "&CNIC=" + CNIC;
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                // System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.DownloadString(apiUrl);
                 data = JsonConvert.DeserializeObject<RootobjectCardHolderInformation>(json);
                
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
    }
}
