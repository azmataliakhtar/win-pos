﻿using BLL.Model;
using BLL.Models;
using BLL.ServiceURLSettingManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Data;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace BLL.UserManager
{
    public class UserManager
    {
        public Rootobject Login(string UserName, string Password)
        {
            Rootobject data = new Rootobject();
            try
            {
                //string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "Account/LoginApp";
                object input = new
                {
                    email = UserName,
                    Password = Password
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.UploadString(apiUrl, inputJson);

                data = JsonConvert.DeserializeObject<Rootobject>(json);
                StaticClassUserData.IsSuccess = JObject.Parse(json)["IsSuccess"].ToString();
                if (StaticClassUserData.IsSuccess == "True")
                {
                    StaticClassUserData.Heading = JObject.Parse(json)["Heading"].ToString();
                    StaticClassUserData.status = (int)JObject.Parse(json)["Status"];
                    StaticClassUserData.UserID = data.responseObject.userInfo.UserID;
                    StaticClassUserData.OrganizationName = data.responseObject.userInfo.OrganizationName;
                    StaticClassUserData.DepartmentName = data.responseObject.userInfo.DepartmentName;
                    StaticClassUserData.DesignationID = data.responseObject.userInfo.DesignationID;
                    StaticClassUserData.TeamName = data.responseObject.userInfo.TeamName;
                    StaticClassUserData.UserTypeName = data.responseObject.userInfo.UserTypeName;
                    StaticClassUserData.Name = data.responseObject.userInfo.Name;
                    StaticClassUserData.OrganizationID = data.responseObject.userInfo.OrganizationID;
                }
            }
            catch (System.Exception ex)
            {

                ex.Message.ToString();
            }
            return data;
        }
    }
}
