﻿namespace BLL.ConfiguratonManager
{
    using BLL.Models;
	using BLL.Constants;
    using BLL.Models.ViewModel;
    using BLL.ServiceURLSettingManager;
    using Dapper;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SQLite;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web.Script.Serialization;
    using static BLL.Models.SynchronizeModel;

    /// <summary>
    /// Defines the <see cref="ConfigurationManager" />.
    /// </summary>
    public class ConfigurationManager
    {
        /// <summary>
        /// The Synchronize.
        /// </summary>
        /// <param name="MacAddress">The MacAddress<see cref="string"/>.</param>
        /// <returns>The <see cref="RootobjectSync"/>.</returns>
        public RootobjectSync Synchronize(string MacAddress)
        {
            RootobjectSync data = new RootobjectSync();
            try
            {
                //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
                DataTable dt = ServiceURLManager.LoadURLSettings();
                DataRow[] dr = dt.Select("ServiceName = 'BaseURL'");
                var IP = dr[0]["IPAddress"].ToString();
                var PortNumber = dr[0]["PortNumber"].ToString();
                string BaseURL = "http://" + IP + ":" + PortNumber + "/Api/";
                string apiUrl = BaseURL + "application/synchronize?macAddress=" + MacAddress;
                object input = new
                {
                    MacAddress = MacAddress
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string json = client.DownloadString(apiUrl);
                data = JsonConvert.DeserializeObject<RootobjectSync>(json);
                string IsSuccess = JObject.Parse(json)["IsSuccess"].ToString();
                if (IsSuccess == "True")
                {
                    StaticClassSyncData.IsSuccess = IsSuccess;
                    StaticClassSyncData.Organization_Address = data.responseObject.posDetails.Organization_Address;
                    StaticClassSyncData.Organization_Name = data.responseObject.posDetails.Organization_Name;
                    StaticClassSyncData.Organization_Mobile = data.responseObject.posDetails.Organization_Mobile;
                    StaticClassSyncData.Organization_Fax = data.responseObject.posDetails.Organization_Fax;
                    StaticClassSyncData.Organization_Skype = data.responseObject.posDetails.Organization_Skype;
                    StaticClassSyncData.Station_Name = data.responseObject.posDetails.Station_Name;
                    StaticClassSyncData.ZipCode = data.responseObject.posDetails.ZipCode;
                    StaticClassSyncData.POS_Code = data.responseObject.posDetails.POS_Code;
                    StaticClassSyncData.POS_IP = data.responseObject.posDetails.POS_IP;
                    StaticClassSyncData.Station_StationCode = data.responseObject.posDetails.Station_StationCode;
                    StaticClassSyncData.Station_OrganizationID = data.responseObject.posDetails.Station_OrganizationID;
                    StaticClassSyncData.POS_Description = data.responseObject.posDetails.POS_Description;
                    StaticClassSyncData.POS_PSAM_MAC = Constants.MacAddress();
                    StaticClassSyncData.pos_TicketValidityMinutes = data.responseObject.posDetails.pos_TicketValidityMinutes;
                    StaticClassSyncData.POS_FareAmount = data.responseObject.posDetails.POS_FareAmount;
                    StaticClassSyncData.POS_Name = data.responseObject.posDetails.POS_Name;
                    StaticClassSyncData.POS_MaxTopUp = data.responseObject.posDetails.POS_MaxTopUp;
                    StaticClassSyncData.POS_MinTopUp = data.responseObject.posDetails.POS_MinTopUp;

                    StaticClassSyncData.POS_ID = data.responseObject.posDetails.POS_ID;
                    StaticClassSyncData.POS_StationID = data.responseObject.posDetails.POS_StationID;
                    StaticClassSyncData.POS_MaxSaleLimit = data.responseObject.posDetails.POS_MaxSaleLimit;
                    StaticClassSyncData.Station_PersonInchargeID = data.responseObject.posDetails.Station_PersonInchargeID;
                    StaticClassSyncData.PreviousStationID = data.responseObject.posDetails.PreviousStationID;
                    StaticClassSyncData.Station_RouteID = data.responseObject.posDetails.Station_RouteID;
                    StaticClassSyncData.Station_Latitude = data.responseObject.posDetails.Station_Latitude;
                    StaticClassSyncData.Station_Longitude = data.responseObject.posDetails.Station_Longitude;
                    StaticClassSyncData.Station_Details = data.responseObject.posDetails.Station_Details;
                    StaticClassSyncData.Station_SerialNumber = data.responseObject.posDetails.Station_SerialNumber;
                    StaticClassSyncData.Station_MaxSaleLimit = data.responseObject.posDetails.Station_MaxSaleLimit;
                    StaticClassSyncData.Organization_OrganizationTypeID = data.responseObject.posDetails.Organization_OrganizationTypeID;
                    StaticClassSyncData.PersonInchargeID = data.responseObject.posDetails.PersonInchargeID;
                    StaticClassSyncData.Organization_Telephone = data.responseObject.posDetails.Organization_Telephone;
                    StaticClassSyncData.Organization_Remarks = data.responseObject.posDetails.Organization_Remarks;
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return data;
        }

        /// <summary>
        /// The SaveConfiguration.
        /// </summary>
        /// <param name="model">The model<see cref="ConfigurationViewModel"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        public int SaveConfiguration(ConfigurationViewModel model)
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.ConnectionString()))
                {
                    Result = cnn.Execute("insert into tblConfiguration (POS_ID,POS_StationID,POS_Name,Organization_Name,POS_Code,POS_MaxSaleLimit,POS_IP,POS_MinTopUp,POS_MaxTopUp,POS_Description,POS_FareAmount,POS_TicketValidityMinutes,Station_Latitude,Station_Longitude,Station_Name,Station_Details,Station_StationCode,Station_MaxSaleLimit,Organization_Name,Organization_ZipCode,Organization_Mobile,Organization_Telephone,Organization_Skype,Organization_Fax,Organization_Address,Organization_Remarks,Organization_OrganizationTypeID,PersonInchargeID,POS_PSAM_MAC,PreviousStationID,Station_OrganizationID,Station_PersonInchargeID,Station_RouteID,Station_SerialNumber,SyncDateTime) " +
                                                                           "values(@POS_ID,@POS_StationID,@POS_Name,@Organization_Name,@POS_Code,@POS_MaxSaleLimit,@POS_IP,@POS_MinTopUp,@POS_MaxTopUp,@POS_Description,@POS_FareAmount,@POS_TicketValidityMinutes,@Station_Latitude,@Station_Longitude,@Station_Name,@Station_Details,@Station_StationCode,@Station_MaxSaleLimit,@Organization_Name,@Organization_ZipCode,@Organization_Mobile,@Organization_Telephone,@Organization_Skype,@Organization_Fax,@Organization_Address,@Organization_Remarks,@Organization_OrganizationTypeID,@PersonInchargeID,@POS_PSAM_MAC,@PreviousStationID,@Station_OrganizationID,@Station_PersonInchargeID,@Station_RouteID,@Station_SerialNumber,@SyncDateTime)", model);
                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return Convert.ToInt32(Result);
        }

        /// <summary>
        /// The LoadConfiguration.
        /// </summary>
        /// <param name="POS_Code">The POS_Code<see cref="string"/>.</param>
        /// <returns>The <see cref="List{ConfigurationViewModel}"/>.</returns>
        public List<ConfigurationViewModel> LoadConfiguration(string POS_Code)
        {


            using (IDbConnection cnn = new SQLiteConnection(Constants.ConnectionString()))
            {
                var Result = cnn.Query<ConfigurationViewModel>("select *  from tblConfiguration where POS_Code='" + POS_Code + "'", new DynamicParameters());
                return Result.ToList();
            }
        }

        /// <summary>
        /// The UpdateConfiguration.
        /// </summary>
        /// <param name="model">The model<see cref="ConfigurationViewModel"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        public int UpdateConfiguration(ConfigurationViewModel model)
        {
            dynamic Result = null;
            try
            {
                using (IDbConnection cnn = new SQLiteConnection(Constants.ConnectionString()))
                {
                    Result = cnn.Execute("Update tblConfiguration set POS_ID=@POS_ID,POS_StationID=@POS_StationID,POS_Name=@POS_Name,Organization_Name=@Organization_Name,POS_Code=@POS_Code,POS_MaxSaleLimit=@POS_MaxSaleLimit,POS_IP=@POS_IP" +
                       ",POS_MinTopUp=@POS_MinTopUp,POS_MaxTopUp=@POS_MaxTopUp,POS_Description=@POS_Description,POS_FareAmount=@POS_FareAmount" +
                       ",POS_TicketValidityMinutes=@POS_TicketValidityMinutes,Station_Latitude=@Station_Latitude,Station_Longitude=@Station_Longitude,Station_Name=@Station_Name" +
                       ",Station_Details=@Station_Details,Station_StationCode=@Station_StationCode,Station_MaxSaleLimit=@Station_MaxSaleLimit" +
                       ",Organization_Name=@Organization_Name,Organization_ZipCode=@Organization_ZipCode,Organization_Mobile=@Organization_Mobile" +
                       ",Organization_Telephone=@Organization_Telephone,Organization_Skype=@Organization_Skype,Organization_Fax=@Organization_Fax" +
                       ",Organization_Address=@Organization_Address,Organization_Remarks=@Organization_Remarks,Organization_OrganizationTypeID=@Organization_OrganizationTypeID" +
                       ",PersonInchargeID=@PersonInchargeID,POS_PSAM_MAC=@POS_PSAM_MAC,PreviousStationID=@PreviousStationID" +
                       ",Station_OrganizationID=@Station_OrganizationID,Station_PersonInchargeID=@Station_PersonInchargeID," +
                       "Station_RouteID=@Station_RouteID,Station_SerialNumber=@Station_SerialNumber,SyncDateTime=@SyncDateTime Where ID=@ID ", model);

                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return Convert.ToInt32(Result);
        }
    }
}
