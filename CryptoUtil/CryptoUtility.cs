﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CryptoUtil
{
    public class CryptoUtility    
    {
        private RSACryptoServiceProvider _rsaDecryptor = null;

        public CryptoUtility()
        {
            var fileText = File.ReadAllText(@"private-key.xml");
            _rsaDecryptor = new RSACryptoServiceProvider();
            _rsaDecryptor.FromXmlString(fileText);
        }

        public string DecryptRSA(string data)
        {
            //var value = _rsaDecryptor.Decrypt(Convert.FromBase64String(data), true);
            //return Encoding.UTF8.GetString(value).Replace("\0", "");


            byte[] IV1 = Encoding.UTF8.GetBytes("METROISB_0987689");
            byte[] KEY1 = Encoding.UTF8.GetBytes("1236547896541236");
            var decrypt = DecryptStringFromBytes_Aes(Convert.FromBase64String(data), KEY1, IV1);
            return decrypt.Replace("\0", "");
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
        //public static string RSAEncrypt(string data, string PublicKeyXML)
        //{
        //    try
        //    {
        //        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        //        byte[] DataToEncrypt = ByteConverter.GetBytes(data);
        //        byte[] encryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {

        //            //Import the RSA Key information. This only needs
        //            //toinclude the public key information.
        //            RSA.FromXmlString(PublicKeyXML);

        //            //Encrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            encryptedData = RSA.Encrypt(DataToEncrypt, RSAEncryptionPadding.OaepSHA1);
        //        }
        //        return Convert.ToBase64String(encryptedData);


        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.Message);

        //        return null;
        //    }
        //}


        //public static string RSADecrypt(string data, string PrivateKeyXML)
        //{
        //    try
        //    {

        //        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        //        var d = Convert.FromBase64String(data);
        //        byte[] DataToDecrypt = d;
        //        byte[] decryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {
        //            //Import the RSA Key information. This needs
        //            //to include the private key information.
        //            RSA.FromXmlString(PrivateKeyXML);

        //            //Decrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            decryptedData = RSA.Decrypt(DataToDecrypt, RSAEncryptionPadding.OaepSHA1);
        //        }
        //        return ByteConverter.GetString(decryptedData);
        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.ToString());

        //        return null;
        //    }
        //}
    }
}
