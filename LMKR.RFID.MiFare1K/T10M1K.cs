﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LMKR.RFID.MiFare1K
{
    public static class T10M1K
    {
        public static readonly string selectBaund = "115200";
        private static int icdev = -1;
        private static short iport = 100;
        [DllImport("dcrf32.dll")]
        private static extern int rf_init_sam(int icdev, byte bound);
        [DllImport("dcrf32.dll")]


        public static extern short dc_init(Int16 port, int baud);  //初试化
        [DllImport("dcrf32.dll")]
        public static extern short dc_exit(int icdev);
        [DllImport("dcrf32.dll")]
        public static extern short dc_reset(int icdev, uint sec);
        [DllImport("dcrf32.dll")]
        public static extern short dc_config_card(int icdev, char cardtype);  //初试化
        [DllImport("dcrf32.dll")]
        public static extern short dc_card(int icdev, byte _Mode, ref uint SnrLen);

        [DllImport("dcrf32.dll")]
        public static extern short dc_request(int icdev, byte _Mode, [Out] byte[] TagType);

        //short USER_API dc_card_n(HANDLE icdev, unsigned char _Mode, unsigned int* SnrLen, unsigned char* _Snr);
        [DllImport("dcrf32.dll")]
        public static extern short dc_card_n(int icdev, byte _Mode, ref uint SnrLen, [Out] byte[] _Snr);
        //short USER_API dc_beep(HANDLE icdev, unsigned short _Msec);
        [DllImport("dcrf32.dll")]
        public static extern short dc_beep(int icdev, ushort _Msec);
        //short USER_API dc_authentication_passaddr(HANDLE icdev, unsigned char _Mode, unsigned char _Addr, unsigned char* passbuff);
        [DllImport("dcrf32.dll")]
        public static extern short dc_authentication_passaddr(int icdev, byte _Mode, byte _Addr, [In] byte[] passbuff);
        //short USER_API dc_write(HANDLE icdev, unsigned char _Adr, unsigned char* _Data);
        [DllImport("dcrf32.dll")]
        public static extern short dc_write(int icdev, byte adr, [In] byte[] sdata);  //向卡中写入数据
        //short USER_API dc_write_hex(HANDLE icdev, unsigned char _Adr, char *_Data);
        [DllImport("dcrf32.dll")]
        public static extern short dc_write_hex(int icdev, int adr, [In] byte[] sdata);  //向卡中写入数据(转换为16进制)
        //short USER_API dc_read(HANDLE icdev, unsigned char _Adr, unsigned char *_Data);
        [DllImport("dcrf32.dll")]
        public static extern short dc_read(int icdev, byte adr, [Out] byte[] sdata);  //从卡中读数据
        //short USER_API dc_read_hex(HANDLE icdev, unsigned char _Adr, char *_Data);
        [DllImport("dcrf32.dll")]
        public static extern short dc_read_hex(int icdev, int adr, [Out] byte[] sdata);  //从卡中读数据(转换为16进制)
        //short USER_API a_hex(unsigned char *a, unsigned char *hex, short len);
        [DllImport("dcrf32.dll")]
        public static extern short a_hex([In] byte[] a, [Out] byte[] hex, short len);  //普通字符转换成十六进制字符
        //short USER_API hex_a(unsigned char *hex, unsigned char *a, short length);
        [DllImport("dcrf32.dll")]
        public static extern short hex_a([In] byte[] hex, [Out] byte[] a, short length);  //普通字符转换成十六进制字符
        //short USER_API dc_initval(HANDLE icdev, unsigned char _Adr, unsigned int _Value);
        [DllImport("dcrf32.dll")]
        public static extern short dc_initval(int icdev, byte _Adr, uint _Value);  //普通字符转换成十六进制字符
        // short USER_API dc_readval(HANDLE icdev, unsigned char _Adr, unsigned int *_Value);
        [DllImport("dcrf32.dll")]
        public static extern short dc_readval(int icdev, byte _Adr, ref uint _Value);  //普通字符转换成十六进制字符
        // short USER_API dc_increment(HANDLE icdev, unsigned char _Adr, unsigned int _Value);
        [DllImport("dcrf32.dll")]
        public static extern short dc_increment(int icdev, byte _Adr, uint _Value);  //普通字符转换成十六进制字符
        // short USER_API dc_decrement(HANDLE icdev, unsigned char _Adr, unsigned int _Value);
        [DllImport("dcrf32.dll")]
        public static extern short dc_decrement(int icdev, byte _Adr, uint _Value);  //普通字符转换成十六进制字符
        //short USER_API dc_restore(HANDLE icdev, unsigned char _Adr);
        [DllImport("dcrf32.dll")]



        public static extern short dc_restore(int icdev, byte _Adr);  //普通字符转换成十六进制字符
        [DllImport("dcrf32.dll")]



        public static extern short dc_SamAReadSerialNumber(int icdev, [Out] byte[] number);  //普通字符转换成十六进制字符
        //short USER_API dc_transfer(HANDLE icdev, unsigned char _Adr);
        [DllImport("dcrf32.dll")]
        public static extern short dc_transfer(int icdev, byte _Adr);  //普通字符转换成十六进制字符
        //short USER_API dc_pro_resetInt(HANDLE icdev, unsigned char *rlen, unsigned char *receive_data);
        [DllImport("dcrf32.dll")]
        public static extern short dc_pro_resetInt(int icdev, ref byte rlen, [Out] byte[] receive_data);  //普通字符转换成十六进制字符

        [DllImport("dcrf32.dll")]
        public static extern short dc_pro_reset(int icdev, ref byte rlen, [Out] byte[] receive_data);
        //short USER_API dc_pro_commandlinkInt(HANDLE icdev, unsigned int slen, unsigned char *sendbuffer, unsigned int *rlen, unsigned char *databuffer, unsigned char timeout);
        [DllImport("dcrf32.dll")]
        public static extern short dc_pro_commandlinkInt(int icdev, uint slen, [In] byte[] sendbuffer, ref uint rlen, [Out] byte[] databuffer, byte timeout);  //普通字符转换成十六进制字符
        //short USER_API dc_card_b(HANDLE icdev, unsigned char *rbuf);
        [DllImport("dcrf32.dll")]
        public static extern short dc_pro_commandlink(int icdev, byte slen, [In] byte[] sendbuffer, ref byte rlen, [Out] byte[] databuffer, byte timeout, int flag);
        [DllImport("dcrf32.dll")]
        public static extern short dc_card_b(int icdev, [Out] byte[] rbuf);  //普通字符转换成十六进制字符
        // short USER_API dc_MFPL0_writeperso(HANDLE icdev, unsigned int BNr, unsigned char *dataperso);
        [DllImport("dcrf32.dll")]
        public static extern short dc_MFPL0_writeperso(int icdev, [In] uint BNr, [Out] byte[] dataperso);  //普通字符转换成十六进制字符
        //short USER_API dc_auth_ulc(HANDLE icdev, unsigned char *key);
        [DllImport("dcrf32.dll")]
        public static extern short dc_auth_ulc(int icdev, [In] byte[] key);  //普通字符转换成十六进制字符
        //short USER_API dc_verifypin_4442(HANDLE icdev, unsigned char *passwd);
        [DllImport("dcrf32.dll")]
        public static extern short dc_verifypin_4442(int icdev, [In] byte[] passwd);  //普通字符转换成十六进制字符
                                                                                      //short USER_API dc_write_4442(HANDLE icdev, short offset, short length, unsigned char *data_buffer);


        [DllImport("D:\\Official\\AFCDevelopment\\Debug v1.2.1.6(1)\\Debug\\WebSocketDriveServer\\Dll\\dcrf32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern short dc_pro_command(int icdev, byte slen, byte[] sendbuff, ref byte rlen, [Out] byte[] recvbuff, byte timeout);
        [DllImport("dcrf32.dll")]


        public static extern short dc_write_4442(int icdev, short offset, short length, [In] byte[] data_buffer);  //普通字符转换成十六进制字符
        //short USER_API dc_read_4442(HANDLE icdev, short offset, short length, unsigned char *data_buffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_read_4442(int icdev, short offset, short length, [Out] byte[] data_buffer);  //普通字符转换成十六进制字符
        [DllImport("dcrf32.dll")]
        public static extern short dc_verifypin_4428(int icdev, [In] byte[] passwd);  //普通字符转换成十六进制字符
        //short USER_API dc_write_4442(HANDLE icdev, short offset, short length, unsigned char *data_buffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_write_4428(int icdev, short offset, short length, [In] byte[] data_buffer);  //普通字符转换成十六进制字符
        //short USER_API dc_read_4442(HANDLE icdev, short offset, short length, unsigned char *data_buffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_read_4428(int icdev, short offset, short length, [Out] byte[] data_buffer);  //普通字符转换成十六进制字符
        //short USER_API dc_setcpu(HANDLE icdev, unsigned char _Byte);
        [DllImport("dcrf32.dll")]
        public static extern short dc_setcpu(int icdev, [In] byte _Byte);  //普通字符转换成十六进制字符
        //short USER_API dc_write_24c(HANDLE icdev, short offset, short length, unsigned char *data_buffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_write_24c(int icdev, short offset, short length, [In] byte[] data_buffer);  //普通字符转换成十六进制字符
        //short USER_API dc_read_4442(HANDLE icdev, short offset, short length, unsigned char *data_buffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_read_24c(int icdev, short offset, short length, [Out] byte[] data_buffer);  //普通字符转换成十六进制字符
        //short USER_API dc_cpureset(HANDLE icdev, unsigned char *rlen, unsigned char *databuffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_cpureset(int icdev, ref byte rlen, [Out] byte[] databuffer);  //普通字符转换成十六进制字符
        //short USER_API dc_cpuapduInt(HANDLE icdev, unsigned int slen, unsigned char *sendbuffer, unsigned int *rlen, unsigned char *databuffer);
        [DllImport("dcrf32.dll")]
        public static extern short dc_cpuapduInt(int icdev, uint slen, [In] byte[] sendbuffer, ref uint rlen, [Out] byte[] databuffer);  //普通字符转换成十六进制字符
        [DllImport("dcrf32.dll")]
        public static extern short dc_SamAReadCardInfo(int handle, int type, ref int text_len, [Out] byte[] text, ref int photo_len, [Out] byte[] photo, ref int fingerprint_len, [Out] byte[] fingerprint, ref int extra_len, [Out] byte[] extra);


        [DllImport("dcrf32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern short dc_cpuapdu(int icdev, byte slen, byte[] sendbuff, ref byte rlen, [Out] byte[] recvbuff);


        [DllImport("dcrf32.dll")]
        public static extern short dc_find_i_d(int handle);
        [DllImport("dcrf32.dll")]
        public static extern int dc_start_i_d(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_name(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_sex(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_nation(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_birth(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_address(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_id_number(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_department(int handle);
        [DllImport("dcrf32.dll")]
        public static extern IntPtr dc_i_d_query_expire_day(int handle);
        [DllImport("dcrf32.dll")]
        public static extern short dc_i_d_query_photo_bmp_buffer(int handle, [Out] byte[] bmp_buffer, ref int bmp_len);
        [DllImport("dcrf32.dll")]
        public static extern short dc_i_d_query_photo_file(int handle, [Out] byte[] FileName);
        [DllImport("dcrf32.dll")]
        public static extern short dc_end_i_d(int handle);
        [DllImport("dcrf32.dll")]
        public static extern short dc_ParseTextInfo(int handle, int charset, int info_len, [Out] byte[] info, [Out] byte[] name, [Out] byte[] sex, [Out] byte[] nation, [Out] byte[] birth_day, [Out] byte[] address, [Out] byte[] id_number, [Out] byte[] department, [Out] byte[] expire_start_day, [Out] byte[] expire_end_day, [Out] byte[] reserved);
        [DllImport("dcrf32.dll")]
        public static extern short dc_ParseTextInfoForForeigner(int handle, int charset, int info_len, [Out] byte[] info, [Out] byte[] english_name, [Out] byte[] sex, [Out] byte[] id_number, [Out] byte[] citizenship, [Out] byte[] chinese_name, [Out] byte[] expire_start_day, [Out] byte[] expire_end_day, [Out] byte[] birth_day, [Out] byte[] version_number, [Out] byte[] department_code, [Out] byte[] type_sign, [Out] byte[] reserved);
        [DllImport("dcrf32.dll")]
        public static extern short dc_ParsePhotoInfo(int handle, int type, int info_len, [Out] byte[] info, ref int photo_len, [Out] byte[] photo);
        [DllImport("dcrf32.dll")]
        public static extern short dc_ParseOtherInfo(int icdev, int flag, [In] byte[] in_info, [Out] byte[] out_info);
        [DllImport("dcrf32.dll")]
        public static extern short dc_Scan2DBarcodeStart(int icdev, byte mode);
        [DllImport("dcrf32.dll")]
        public static extern short dc_Scan2DBarcodeGetData(int icdev, ref int rlen, [Out] byte[] rdata);
        [DllImport("dcrf32.dll")]
        public static extern short dc_Scan2DBarcodeExit(int icdev);
        [DllImport("dcrf32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern short dc_card_double_hex(int icdev, byte model, [Out] char[] snr);




        public static ResponseModel TestReader()
        {
            ResponseModel response = new ResponseModel();



            icdev = dc_init(iport, Convert.ToInt32(selectBaund));

            if ((int)icdev <= 0)
            {
                response.isSuccess = false;
                response.Message = "Error in communication with device";
                response.ResponseType = "ConnectionTest";
            }
            else
            {
                dc_beep(icdev, 10);
                System.Threading.Thread.Sleep(15);
                dc_beep(icdev, 50);
                response.isSuccess = true;
                response.Message = "Reader Connection Success";
                response.ResponseType = "ConnectionTest";
            }

            return response;



        }


        private static void SafeExit()
        {
            if ((int)icdev > 0)
            {
                var st = dc_exit(icdev);
                if (st != 0)
                {

                }
                else
                {

                    icdev = -1;
                }
            }
        }
        public static ResponseModel ReadCardSerial()
        {
            ResponseModel response = new ResponseModel();
            byte[] _Snr = new byte[512];
            byte[] szSnr = new byte[100];
            short iport = 100;

            uint SnrLen = 0;
            byte[] _Snr2 = new byte[200];
            icdev = dc_init(iport, Convert.ToInt32(selectBaund));

            if ((int)icdev <= 0)
            {
                response.isSuccess = false;
                response.Message = "Error in communication with device";
                response.ResponseType = "ReadCardSerial";
            }
            else
            {
                dc_beep(icdev, 10);

                dc_reset(icdev, 1);

                var st = dc_config_card(icdev, 'A');

                st = dc_card_n(icdev, 0x00, ref SnrLen, _Snr);

                if (st != 0)
                {
                    response.isSuccess = false;
                    response.Message = "Error in reading serial number";
                    response.ResponseType = "ReadCardSerial";
                    SafeExit();
                }
                else
                {

                    try
                    {
                        //for (int i = 0; i < 4; i++)
                        //{
                        //    _Snr[i] = _Snr[3 - i];
                        //}
                        hex_a(_Snr, szSnr, 4);
                        var str = System.Text.Encoding.UTF8.GetString(szSnr);
                        response.isSuccess = true;
                        response.ReturData = str.Substring(0, 8);
                        response.Message = "Read card successfull";
                    }
                    catch (Exception exc)
                    {
                        response.isSuccess = false;
                        response.Message = "Error in reading serial number";
                        response.ResponseType = "ReadCardSerial";
                        SafeExit();
                    }

                    SafeExit();
                }





            }

            return response;
        }

        private static short AuthenticateA(byte[] password, byte block)
        {
            return dc_authentication_passaddr(icdev, 0, block, password);
        }
        private static short AuthenticateB(byte[] password, byte block)
        {
            return dc_authentication_passaddr(icdev, 0X04, block, password);
        }

        private static byte[] StrToBytes(string input)
        {
            return System.Text.Encoding.Default.GetBytes(input);
        }



        public static ResponseModel TopupCard(string CardSerial, int Topup)
        {
            ResponseModel response = new ResponseModel();
            byte[] rdata = new byte[33];
            byte[] rdatahex = new byte[33];
            var Keys = EncryptionService(CardSerial);
            byte[] KeyA = new byte[6];
            byte[] KeyB = new byte[6];
            //Key A to be default

            KeyA[0] = 0x30;
            KeyA[1] = 0x31;
            KeyA[2] = 0x32;
            KeyA[3] = 0x33;
            KeyA[4] = 0x34;
            KeyA[5] = 0x35;

            try
            {

                KeyB = StrToBytes(Keys[0]);

                icdev = dc_init(iport, Convert.ToInt32(selectBaund));

                if ((int)icdev <= 0)
                {
                    response.isSuccess = false;
                    response.Message = "Error in communication with RFID device";
                    SafeExit();
                    return response;
                }

                var st = AuthenticateB(KeyB, 11);
                if (st != 0)
                {
                    //Card is corrupt or already initialized because default passeord is not working on first block
                    response.isSuccess = false;
                    response.Message = "Unable to issue card. authentication failed for wallet block 11 ";
                    SafeExit();
                    return response;

                }

                //st = dc_increment(icdev, 9, Convert.ToUInt32(Topup));
                //if (st != 0)
                //{
                //    //Card is corrupt or already initialized because default passeord is not working on first block
                //    response.isSuccess = false;
                //    response.Message = "Unable to topup card. card wallet topup failed ";
                //    SafeExit();
                //    return response;
                //}
                dc_beep(icdev, 10);
                //Read Transaction counter
                st = dc_read(icdev, 10, rdata);
                if (st != 0)
                {
                    //Unable to read card Block
                    response.isSuccess = false;
                    response.Message = "Unable to read block 2";
                    SafeExit();
                    return response;
                }


                hex_a(rdata, rdatahex, 9);
                var str = System.Text.Encoding.Default.GetString(rdata);

                int TCounter = Convert.ToInt32(str) + Topup;


                string TransactionCounter = TCounter.ToString().PadLeft(16, '0');
                var writecarddata = System.Text.Encoding.Default.GetBytes(TransactionCounter);
                //Write this to block 10
                st = dc_write(icdev, 10, writecarddata);


                response.isSuccess = true;

                response.Message = "Card topup successful";
                dc_beep(icdev, 10);
                SafeExit();
                return response;

            }

            catch (Exception exc)
            {
                response.isSuccess = false;
                response.Message = exc.Message + exc.InnerException?.Message;

                return response;


            }
        }


        public static ResponseModel IssueCard(string CardSerial, string CNIC, int Topup, string CardTypeCode, DateTime CardExpiry)
        {
            ResponseModel response = new ResponseModel();

            try
            {

                //Using second complete sector(block 4,5,6) for issuance history and information
                var Keys = EncryptionService(CardSerial);
                byte[] KeyA = new byte[6];
                byte[] KeyB = new byte[6];
                //Key A to be default

                KeyA[0] = 0x30;
                KeyA[1] = 0x31;
                KeyA[2] = 0x32;
                KeyA[3] = 0x33;
                KeyA[4] = 0x34;
                KeyA[5] = 0x35;



                KeyB = StrToBytes(Keys[0]);
                icdev = dc_init(iport, Convert.ToInt32(selectBaund));

                if ((int)icdev <= 0)
                {
                    response.isSuccess = false;
                    response.Message = "Error in communication with RFID device";
                    SafeExit();
                    return response;
                }
                var st = AuthenticateB(KeyB, 7);
                if (st != 0)
                {
                    //unable to authenticate card for issuance on block 7
                    response.isSuccess = false;
                    response.Message = "unable to authenticate card for issuance on block 7 ";
                    SafeExit();
                    return response;

                }
                string CNICAndCrdTypeCode = CNIC.PadLeft(13, '0') + CardTypeCode.PadLeft(3, '0');
                var writecarddata = System.Text.Encoding.Default.GetBytes(CNICAndCrdTypeCode);
                //Write this to Block 4
                st = dc_write(icdev, 4, writecarddata);
                if (st != 0)
                {

                    response.isSuccess = false;
                    response.Message = "Unable to issue card. Writing to card failed on block 4";
                    SafeExit();
                    return response;

                }

                string IssueandExpiryDates = DateTime.Now.ToString("yyyyMMdd") + CardExpiry.ToString("yyyyMMdd");
                writecarddata = System.Text.Encoding.Default.GetBytes(IssueandExpiryDates);
                //Write this to block 5
                st = dc_write(icdev, 5, writecarddata);
                if (st != 0)
                {

                    response.isSuccess = false;
                    response.Message = "Unable to issue card.Writing to card failed on block 5 ";
                    SafeExit();
                    return response;

                }

                dc_beep(icdev, 5);
                //Topup the wallet
                st = AuthenticateB(KeyB, 11);
                if (st != 0)
                {
                    //Card is corrupt or already initialized because default passeord is not working on first block
                    response.isSuccess = false;
                    response.Message = "Unable to issue card. authentication failed for wallet block 11 ";
                    SafeExit();
                    return response;

                }

                st = dc_increment(icdev, 9, Convert.ToUInt32(Topup));
                if (st != 0)
                {
                    //Card is corrupt or already initialized because default passeord is not working on first block
                    response.isSuccess = false;
                    response.Message = "Unable to issue card. card wallet topup failed ";
                    SafeExit();
                    return response;
                }



                //Check Status

                int check = 0;


                string check2 = check.ToString().PadLeft(16, '0');
                writecarddata = System.Text.Encoding.Default.GetBytes(check2);
                //Write this to block 10
                st = dc_write(icdev, 8, writecarddata);







                dc_beep(icdev, 10);
                //Initialize Transaction counter
                string TransactionCounter = Topup.ToString().PadLeft(16, '0');
                writecarddata = System.Text.Encoding.Default.GetBytes(TransactionCounter);
                //Write this to block 10
                st = dc_write(icdev, 10, writecarddata);
                dc_beep(icdev, 10);
                SafeExit();
            }

            catch (Exception)
            {

            }

            response.isSuccess = true;
            response.Message = "Card Issuance successful";
            return response;
        }

        public static ResponseModel ReadCardInfo(string CardSerial)
        {
            ResponseModel response = new ResponseModel();
            byte[] rdata = new byte[33];
            byte[] rdatahex = new byte[33];

            uint ivalue = 0;
            string cvalue;
            byte[] KeyA = new byte[6];
            byte[] KeyB = new byte[6];
            //Key A to be default

            KeyA[0] = 0x30;
            KeyA[1] = 0x31;
            KeyA[2] = 0x32;
            KeyA[3] = 0x33;
            KeyA[4] = 0x34;
            KeyA[5] = 0x35;

            try
            {
                var Keys = EncryptionService(CardSerial);
                KeyB = StrToBytes(Keys[0]);
                dynamic obj = new ExpandoObject();


                icdev = dc_init(iport, Convert.ToInt32(selectBaund));

                if ((int)icdev <= 0)
                {
                    response.isSuccess = false;
                    response.Message = "Error in communication with RFID device";
                    SafeExit();
                    return response;
                }

                //Read Block 1
                var st = AuthenticateA(KeyA, 3);

                if (st != 0)
                {
                    //Unable to write card Block
                    response.isSuccess = false;
                    response.Message = "Unable to program card.  authentication failed for block 3 ";
                    SafeExit();
                    return response;
                }


                st = dc_read(icdev, 1, rdata);
                if (st != 0)
                {
                    //Unable to read card Block
                    response.isSuccess = false;
                    response.Message = "Unable to read block 1";
                    SafeExit();
                    return response;
                }


                hex_a(rdata, rdatahex, 9);
                var str = System.Text.Encoding.Default.GetString(rdata);
                obj.card_number = str;

                st = dc_read(icdev, 2, rdata);
                if (st != 0)
                {
                    //Unable to read card Block
                    response.isSuccess = false;
                    response.Message = "Unable to read block 2";
                    SafeExit();
                    return response;
                }


                hex_a(rdata, rdatahex, 9);
                str = System.Text.Encoding.Default.GetString(rdata);
                obj.organization_code = str.Substring(0, 4);
                obj.initialization_date = str.Substring(4, 8);
                obj.pos_code = str.Substring(12, 4);


                //Read Wallet
                st = AuthenticateA(KeyA, 11);

                //if (st != 0)
                //{
                //    //Unable to write card Block
                //    response.isSuccess = false;
                //    response.Message = "Unable to read card.  authentication failed for block wallet -11 ";
                //    SafeExit();
                //    return response;
                //}


                //st = dc_readval(icdev, 9, ref ivalue);
                //if (st != 0)
                //{
                //    //Unable to write card Block
                //    response.isSuccess = false;
                //    response.Message = "Unable to read card wallet ";
                //    SafeExit();
                //    return response;
                //}

                //cvalue = ivalue.ToString();
                st = dc_read(icdev, 10, rdata);
                if (st != 0)
                {
                    //Unable to read card Block
                    response.isSuccess = false;
                    response.Message = "Unable to read block 2";
                    SafeExit();
                    return response;
                }


                hex_a(rdata, rdatahex, 9);
                 str = System.Text.Encoding.Default.GetString(rdata);

                obj.balance = Convert.ToInt32(str) ;
               // obj.balance = Convert.ToInt32(cvalue);
               // obj.balance = Convert.ToInt32(str);

                response.isSuccess = true;
                response.ReturData = obj;
                response.Message = "Card info read successful";

                dc_beep(icdev, 15);
                SafeExit();
                return response;
            }

            catch (Exception exc)
            {
                response.isSuccess = false;
                response.Message = exc.Message + exc.InnerException?.Message;
                SafeExit();
                return response;
            }

        }


        public static ResponseModel InitializeCard(string CardSerial, String OrganizationCode, string CardNumber, string PosCode)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var Keys = EncryptionService(CardSerial);
                byte[] acc1 = new byte[4];
                byte[] acc2 = new byte[4];



                byte[] default_password = new byte[7];
                default_password[0] = 0xff;
                default_password[1] = 0xff;
                default_password[2] = 0xff;
                default_password[3] = 0xff;
                default_password[4] = 0xff;
                default_password[5] = 0xff;

                byte[] KeyA = new byte[6];
                byte[] KeyB = new byte[6];
                byte[] acc = new byte[4];


                //Key A to be default

                KeyA[0] = 0x30;
                KeyA[1] = 0x31;
                KeyA[2] = 0x32;
                KeyA[3] = 0x33;
                KeyA[4] = 0x34;
                KeyA[5] = 0x35;



                KeyB = StrToBytes(Keys[0]);
                acc1[0] = 0x7F;
                acc1[1] = 0x07;
                acc1[2] = 0x88;
                acc1[3] = 0x30;


                acc2[0] = 0x08;
                acc2[1] = 0x77;
                acc2[2] = 0x8F;
                acc2[3] = 0x31;


                var Card = ReadCardSerial().ReturData.ToString();
                if (Card != CardSerial)
                {
                    // Card changed after initial read on scanner
                    response.isSuccess = false;
                    response.Message = "Card serial changed";
                    SafeExit();
                    return response;
                }


                //Initializing Blocks
                icdev = dc_init(iport, Convert.ToInt32(selectBaund));

                if ((int)icdev <= 0)
                {
                    response.isSuccess = false;
                    response.Message = "Error in communication with RFID device";
                    SafeExit();
                    return response;
                }

                for (int sector = 0; sector < 16; sector++)
                {
                    var block = (4 * sector) + 3;

                    //loops through 16 blocks

                    var st = AuthenticateA(default_password, (byte)block);
                    if (st != 0)
                    {
                        //Card is corrupt or already initialized because default passeord is not working on first block
                        response.isSuccess = false;
                        response.Message = "Unable to program card. default authentication failed for block " + block;
                        SafeExit();
                        return response;

                    }
                    else
                    {
                        //replace block keys
                        byte[] writedata = new byte[16];
                        System.Buffer.BlockCopy(KeyA, 0, writedata, 0, 6);
                        System.Buffer.BlockCopy(acc1, 0, writedata, 6, 4);
                        System.Buffer.BlockCopy(KeyB, 0, writedata, 10, 6);

                        //Write Key A and B for Authentication
                        st = dc_write(icdev, (byte)block, writedata);
                        if (st != 0)
                        {
                            //Unable to write card Block
                            response.isSuccess = false;
                            response.Message = "Unable to program card. default authentication failed for block " + block;
                            SafeExit();
                            return response;
                        }
                        else
                        {
                            dc_beep(icdev, 10);

                        }
                    }
                }


                //Securing Wallet Sector


                var walletblock = 11;


                var st1 = AuthenticateB(KeyB, (byte)walletblock);
                if (st1 != 0)
                {
                    //Card is corrupt or already initialized because default passeord is not working on first block
                    response.isSuccess = false;
                    response.Message = "Unable to program card. authentication failed for wallet block 11 ";
                    SafeExit();
                    return response;

                }
                else
                {
                    //replace block keys
                    byte[] writedata = new byte[16];
                    System.Buffer.BlockCopy(KeyA, 0, writedata, 0, 6);
                    System.Buffer.BlockCopy(acc2, 0, writedata, 6, 4);
                    System.Buffer.BlockCopy(KeyB, 0, writedata, 10, 6);

                    //Write Key A and B for Authentication
                    st1 = dc_write(icdev, (byte)walletblock, writedata);
                    if (st1 != 0)
                    {
                        //Unable to write card Block
                        response.isSuccess = false;
                        response.Message = "Unable to program card. write data failed to wallet block ";
                        SafeExit();
                        return response;
                    }
                    else
                    {
                        dc_beep(icdev, 10);

                    }

                }


                // Initialize Wallet Sector
                st1 = AuthenticateB(KeyB, (byte)walletblock);
                if (st1 != 0)
                {
                    //Card is corrupt or already initialized because default passeord is not working on first block
                    response.isSuccess = false;
                    response.Message = "Unable to program card. authentication failed for wallet block 11 ";
                    SafeExit();
                    return response;

                }
                else
                {
                    st1 = dc_initval(icdev, 9, 0);
                    if (st1 != 0)
                    {
                        //Unable to write card Block
                        response.isSuccess = false;
                        response.Message = "Unable to program card. Initialize wallet sector failed ";
                        SafeExit();
                        return response;
                    }
                    else
                    {
                        dc_beep(icdev, 10);

                    }

                }
                //Write Card number of 16 bytes to block 1
                st1 = AuthenticateA(KeyA, 3);
                if (st1 != 0)
                {

                    response.isSuccess = false;
                    response.Message = "Unable to program card. Writing card number failed to block 1 ";
                    SafeExit();
                    return response;
                }
                else
                {
                    string card = CardNumber.PadLeft(16, '0');
                    var writecarddata = System.Text.Encoding.Default.GetBytes(card);
                    st1 = dc_write(icdev, 1, writecarddata);
                    if (st1 != 0)
                    {

                        response.isSuccess = false;
                        response.Message = "Unable to program card. Writing card number failed to block 1 ";
                        SafeExit();
                        return response;

                    }

                    string OrganizationData = OrganizationCode.PadLeft(4, '0') + DateTime.Now.ToString("yyyyMMdd") + PosCode.PadLeft(4, '0');
                    writecarddata = System.Text.Encoding.Default.GetBytes(OrganizationData);
                    st1 = dc_write(icdev, 2, writecarddata);
                    if (st1 != 0)
                    {

                        response.isSuccess = false;
                        response.Message = "Unable to program card. Writing organizaition data failed to block 1 ";
                        SafeExit();
                        return response;

                    }
                }




                response.isSuccess = true;
                response.Message = "Initialization successful";
                SafeExit();




                return response;
            }

            catch (Exception)
            {
                return response;
            }

        }


        public static List<string> EncryptionService(string CardSN)
        {
            string RootKey = "JUMBLEHUMBLEAESTRUTHTRVERSEINAYATGRUZ10203012321478765600987654321JUBILEE";
            List<string> keys = new List<string>();


            byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
            int BlockSize = 128;
            try
            {
                SymmetricAlgorithm crypt = Aes.Create();
                HashAlgorithm hash = MD5.Create();
                crypt.BlockSize = BlockSize;
                crypt.Key = hash.ComputeHash(Encoding.Unicode.GetBytes(CardSN));
                crypt.IV = IV;
                byte[] bytes = Encoding.Unicode.GetBytes(RootKey);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, crypt.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(bytes, 0, bytes.Length);
                    }

                    string Key = Convert.ToBase64String(memoryStream.ToArray());

                    keys.Add(Key.Substring(0, 6));
                    keys.Add(Key.Substring(6, 6));
                    keys.Add(Key.Substring(12, 6));
                }

            }
            catch (Exception)
            {

            }


            keys[0] = "PhGxoY";
            return keys;


        }
    }
}
