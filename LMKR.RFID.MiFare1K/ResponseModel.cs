﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMKR.RFID.MiFare1K
{
    public class ResponseModel
    {
        public bool isSuccess { get; set; }

        public string ResponseType { get; set; }
        public string Message { get; set; }

        public object ReturData { get; set; }
    }
}
