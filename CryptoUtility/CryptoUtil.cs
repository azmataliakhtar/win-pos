﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CryptoUtility
{
    public class CryptoUtil
    {
        private RSACryptoServiceProvider _rsaDecryptor = null;

        public CryptoUtil()
        {
            var fileText = File.ReadAllLines(@"rsa-private-key.pem");// file containing RSA PKCS8 private key
            StringBuilder sb = new StringBuilder();
            if (fileText.Length > 1 && fileText[0].StartsWith("-----") && fileText[fileText.Length - 1].StartsWith("-----"))
            {
                for (int i = 1; i < fileText.Length-1; i++)
                    sb.Append(fileText[i]);
                
            }
            
            var keyString = sb.ToString();
            _rsaDecryptor = new RSACryptoServiceProvider();


            _rsaDecryptor.ImportPkcs8PrivateKey(Convert.FromBase64String(keyString), out int readBytes);
            var test = _rsaDecryptor.ToXmlString(true);
        }

        public string DecryptRSA(string data)
        {
            var value = _rsaDecryptor.Decrypt(Convert.FromBase64String(data),true);
            return Encoding.UTF8.GetString(value);
        }
        //public static string RSAEncrypt(string data, string PublicKeyXML)
        //{
        //    try
        //    {
        //        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        //        byte[] DataToEncrypt = ByteConverter.GetBytes(data);
        //        byte[] encryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {

        //            //Import the RSA Key information. This only needs
        //            //toinclude the public key information.
        //            RSA.FromXmlString(PublicKeyXML);

        //            //Encrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            encryptedData = RSA.Encrypt(DataToEncrypt, RSAEncryptionPadding.OaepSHA1);
        //        }
        //        return Convert.ToBase64String(encryptedData);


        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.Message);

        //        return null;
        //    }
        //}


        //public static string RSADecrypt(string data, string PrivateKeyXML)
        //{
        //    try
        //    {

        //        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        //        var d = Convert.FromBase64String(data);
        //        byte[] DataToDecrypt = d;
        //        byte[] decryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {
        //            //Import the RSA Key information. This needs
        //            //to include the private key information.
        //            RSA.FromXmlString(PrivateKeyXML);

        //            //Decrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            decryptedData = RSA.Decrypt(DataToDecrypt, RSAEncryptionPadding.OaepSHA1);
        //        }
        //        return ByteConverter.GetString(decryptedData);
        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.ToString());

        //        return null;
        //    }
        //}

    }
}
