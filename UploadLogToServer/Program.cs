﻿using System;
using System.Collections.Generic;
using System.Data;
using UploadLogToServer.ExtenionMethod;
using UploadLogToServer.Model;

namespace UploadLogToServer
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Application Started");
                LogModel logModel = new LogModel();
                DataTable dataTable = LogManager.LogManager.LoadLog();
                if (dataTable.Rows.Count > 0)
                {
                    Console.WriteLine("Data Received From Database");
                }
                else
                {
                    Console.WriteLine("0 records found");
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    logModel.LogID = Convert.ToInt32(row["ID"]);
                    logModel.UserID = row["UserID"].ToString();
                    logModel.ActionPerform = row["ActionPerform"].ToString();
                    logModel.UserName = row["UserName"].ToString();
                    logModel.Date = Convert.ToDateTime(row["LoginDate"]);
                    logModel.IPAddress = row["IP_Address"].ToString();
                    if (LogManager.LogManager.RunLog(logModel))
                    {
                        int Result = LogManager.LogManager.DeleteLogRecord(logModel.LogID);
                        if (Result == 1)
                        {
                            Console.WriteLine("Record Deleted with ID = " + logModel.LogID);
                        }
                        else
                        {
                            Console.WriteLine("Record not Deleted with ID = " + logModel.LogID + "Check your Database Connection");
                        }
                        Console.WriteLine("row inserted with logid=" + logModel.LogID + " , Action Performed " + logModel.ActionPerform);
                    }
                    else
                    {

                        Console.WriteLine("row not inserted with logid= " + logModel.LogID);
                    }

                }
                Console.WriteLine("total rows inserted= " + dataTable.Rows.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
