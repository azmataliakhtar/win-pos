﻿using BLL.Constants;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using UploadLogToServer.Model;

namespace UploadLogToServer.LogManager
{
    public class LogManager
    {
        public static bool RunLog(LogModel logModel)
        {
            bool Result = false;
            try
            {
                string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ServiceURL"].ToString();
                object input = new
                {
                    LogID = logModel.LogID,
                    UserID = logModel.UserID,
                    ActionPerform = logModel.ActionPerform,
                    IPAddress = logModel.IPAddress,
                    UserName = logModel.UserName,
                    Date = logModel.Date
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                string json = client.UploadString(BaseURL, inputJson);
                Result = Convert.ToBoolean(json);

            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return Result;
        }
        public static DataTable LoadLog()
        {
            SQLiteDataReader result;
            DataTable datatable = new DataTable();
            try
            {
                using (SQLiteConnection cnn = new SQLiteConnection(Constants.ConnectionString()))
                {
                    cnn.Open();
                    SQLiteCommand command = new SQLiteCommand("SELECT * FROM tblLog", cnn);
                    result = command.ExecuteReader();
                    datatable.Load(result);
                    cnn.Close();
                }
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
            return datatable;
        }
        public static int DeleteLogRecord(int ID)
        {
            using (IDbConnection cnn = new SQLiteConnection(Constants.ConnectionString()))
            {
                var Result = cnn.Execute("delete from tbllog Where ID =" + ID);
                return Convert.ToInt32(Result);
            }
        }
    }
}
