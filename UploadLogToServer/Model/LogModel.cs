﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UploadLogToServer.Model
{
    public class LogModel
    {
        public int LogID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string  ActionPerform { get; set; }
        public DateTime Date { get; set; }
        public string IPAddress { get; set; }
        public string RecordType { get; set; }
    }
}
