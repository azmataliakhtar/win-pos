﻿using System;

namespace CryptoUtilityTest
{
    class Program
    {
        static void Main(string[] args)
        {
            do {
                Console.WriteLine("Scan QR");

                var test = Console.ReadLine();
                CryptoUtility.CryptoUtil cryptoUtil = new CryptoUtility.CryptoUtil();
                var decryptedText = cryptoUtil.DecryptRSA(test);
                Console.WriteLine("Decrypted Text:  " + decryptedText);

            }while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
