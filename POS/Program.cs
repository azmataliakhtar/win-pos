﻿using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LogManager logManager = new LogManager();
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            LogModel logModel = new LogModel();
            logModel.UserID = StaticClassUserData.UserID;
            logModel.ActionPerform = "Application Start Event";
            logModel.LoginDate = DateTime.Now.ToString();
            logModel.UserName = StaticClassUserData.Name;
            logManager.SaveLog(logModel);
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            Application.Run(new Login());
         
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            //LogManager logManager = new LogManager();
            //LogModel logModel = new LogModel();
            //logModel.UserID = StaticClassUserData.UserID;
            //logModel.ActionPerform = "Process Exit";
            //logModel.LoginDate = DateTime.Now.ToString();
            //logModel.UserName = StaticClassUserData.Name;
            //logManager.SaveLog(logModel);
        }

        private static void Application_ApplicationExit(object sender, EventArgs e)
        {
            LogManager logManager = new LogManager();
            LogModel logModel = new LogModel();
            logModel.UserID = StaticClassUserData.UserID;
            logModel.ActionPerform = "Application Exit";
            logModel.LoginDate = DateTime.Now.ToString();
            logModel.UserName = StaticClassUserData.Name;
            logManager.SaveLog(logModel);
        }
    }
}
