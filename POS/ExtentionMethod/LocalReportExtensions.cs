﻿namespace POS.Forms
{
    using Microsoft.Reporting.WinForms;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Printing;
    using System.IO;

    /// <summary>
    /// Defines the <see cref="LocalReportExtensions" />.
    /// </summary>
    public static class LocalReportExtensions
    {
        /// <summary>
        /// Defines the m_streams.
        /// </summary>
        //private static List<Stream> m_streams;
        //
        /// <summary>
        /// Defines the m_currentPageIndex.
        /// </summary>
        //private static int m_currentPageIndex = 0;

        /// <summary>
        /// The Print.
        /// </summary>
        /// <param name="report">The report<see cref="LocalReport"/>.</param>
        /// <param name="printType">The printType<see cref="string"/>.</param>
        public static void Print(this LocalReport report, string printType)
        {
            var pageSettings = new PageSettings();
            pageSettings.PaperSize = report.GetDefaultPageSettings().PaperSize;
            pageSettings.Landscape = report.GetDefaultPageSettings().IsLandscape;
            pageSettings.Margins = report.GetDefaultPageSettings().Margins;
            pageSettings.Landscape = false;
            Print(report, pageSettings, printType);
        }

        /// <summary>
        /// The Print.
        /// </summary>
        /// <param name="report">The report<see cref="LocalReport"/>.</param>
        /// <param name="pageSettings">The pageSettings<see cref="PageSettings"/>.</param>
        /// <param name="printType">The printType<see cref="string"/>.</param>
        public static void Print(this LocalReport report, PageSettings pageSettings, string printType)
        {
            string deviceInfo = "";
            if (printType == "CardIssuance")
            {
                deviceInfo =
                @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                </DeviceInfo>";
            }
            else if (printType == "Topup")
            {
                deviceInfo =
                @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>

                </DeviceInfo>";
            }
            else
            {
                deviceInfo =
                           @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                </DeviceInfo>";
            }
            Warning[] warnings;
            var streams = new List<Stream>();
            var currentPageIndex = 0;

            report.Render("Image", deviceInfo,
                (name, fileNameExtension, encoding, mimeType, willSeek) =>
                {
                    var stream = new MemoryStream();
                    streams.Add(stream);
                    return stream;
                }, out warnings);

            foreach (Stream stream in streams)
                stream.Position = 0;

            if (streams == null || streams.Count == 0)
                throw new Exception("Error: no stream to print.");

            var printDocument = new PrintDocument();

            printDocument.DefaultPageSettings = pageSettings;
            if (!printDocument.PrinterSettings.IsValid)
                throw new Exception("Error: cannot find the default printer.");
            else
            {
                printDocument.PrintPage += (sender, e) =>
                {
                    Metafile pageImage = new Metafile(streams[currentPageIndex]);
                    Rectangle adjustedRect = new Rectangle(
                        e.PageBounds.Left,
                        e.PageBounds.Top,
                        e.PageBounds.Width,
                        e.PageBounds.Height);
                    e.Graphics.FillRectangle(Brushes.White, adjustedRect);
                    e.Graphics.DrawImage(pageImage, adjustedRect);
                    currentPageIndex++;
                    e.HasMorePages = (currentPageIndex < streams.Count);
                    //e.Graphics.DrawRectangle(Pens.Red, adjustedRect);
                };
                printDocument.EndPrint += (Sender, e) =>
                {
                    if (streams != null)
                    {
                        foreach (Stream stream in streams)
                            stream.Close();
                        streams = null;
                    }
                };
                printDocument.Print();
            }
        }
    }
}
