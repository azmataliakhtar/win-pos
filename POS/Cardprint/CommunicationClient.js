﻿(function ($, window, undefined) {

    if (typeof ($) !== "function") {
        throw new Error(resources.nojQuery);
    }

    var communicationClient = function () {
        var _communicationHub = null;
        var _isConnected = false;
        var _userName = "";
        var _userGroups = "";
        var _hubName = "";
        var _serverURL = "";

        $.fn.connect = function (userName, userGroups, hubName, serverURL) {
            _userName = userName;
            _userGroups = userGroups;
            _hubName = hubName;
            _serverURL = serverURL;

            _communicationHub = $.connection.communicationHub;

            communicationHub.client.message = function (from, message) {
                ReceivedMessage(from, message);
            };

            $.connection.hub.start()
                .done(function () {
                    IsConnected = true;
                })
                .fail(function () {
                    IsConnected = false;
                });

            $.connection.hub.stop().done(function () {
                IsConnected = false;
            });
        };

        $.fn.disconnect = function () {

        };

        $.fn.sendMessageToUser = function (from, user, message) {

        };

        $.fn.sendMessageToUsers = function (from, users, message) {

        };

        $.fn.sendMessageToGroup = function (from, group, message) {

        };

        $.fn.sendMessageToGroups = function (from, groups, message) {

        };

        $.fn.broadcastMessage = function (from, message) {

        };

        $.fn.sendMessageToServer = function (from, message) {

        };
    };

}(window.jQuery, window));

$.communicationClient.connect()
$.communicationClient.sendMessageToUser(user, message)

if ($.communicationClient.IsConnected) {

}

