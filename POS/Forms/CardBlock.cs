﻿namespace POS.Forms
{
    using BLL.CardBlockManager;
    using BLL.IssueCardManager;
    using BLL.LogManager;
    using BLL.Model;
    using BLL.Models;
    using BLL.Models.ViewModel;
    using LMKR.RFID.MiFare1K;
    using Microsoft.Reporting.WinForms;
    using POS.ExtentionMethod;
    using System;
    using System.Data;
    using System.Windows.Forms;
    using static BLL.Models.CardBlockModel;
    using static BLL.Models.CardHolderInformationModel;
    using static BLL.Models.IssueCardModel;

    /// <summary>
    /// Defines the <see cref="CardBlock" />.
    /// </summary>
    public partial class CardBlock : Form
    {
        /// <summary>
        /// Defines the CardSerialnumber.
        /// </summary>
        public static string CardSerialnumber = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="CardBlock"/> class.
        /// </summary>
        public CardBlock()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The pictureBox2_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Defines the issueCardManager.
        /// </summary>
        internal IssueCardManager issueCardManager = new IssueCardManager();

        /// <summary>
        /// Defines the cardBlock.
        /// </summary>
        internal CardBlockManager cardBlock = new CardBlockManager();

        /// <summary>
        /// Defines the dt.
        /// </summary>
        internal DataTable dt;

        /// <summary>
        /// The CardIssuance_Load.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void CardIssuance_Load(object sender, EventArgs e)
        {
            try
            {
                panelMainScreen.Visible = false;
                pictureBoxCardtopup.Visible = true;
                RootobjectIssueCard rootobjectIssueCard = issueCardManager.GetCardTypeByOrganizationID(StaticClassUserData.OrganizationID);
                dt = ConvertListToDatatable.ToDataTable(rootobjectIssueCard.responseObject);
                comboBoxCardType.DataSource = dt;
                comboBoxCardType.DisplayMember = "Name";
                comboBoxCardType.ValueMember = "CardTypeID";
                comboBoxCardType.SelectedIndex = -1;
            }
            catch (Exception ex) { ex.Message.ToString(); }
        }

        /// <summary>
        /// Defines the CardType.
        /// </summary>
        internal string CardType = "";

        /// <summary>
        /// Defines the maxtopup.
        /// </summary>
        internal int maxtopup = 0;

        /// <summary>
        /// Defines the minTopup.
        /// </summary>
        internal int minTopup = 0;

        /// <summary>
        /// The comboBoxCardType_SelectionChangeCommitted.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void comboBoxCardType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                var CardTypeID = comboBoxCardType.SelectedValue;
                DataRow[] dr = dt.Select("CardTypeID='" + CardTypeID + "'");

                textBoxexpiry.Text = dt.Rows[0]["CardValidity"].ToString();
                minTopup = Convert.ToInt32(dt.Rows[0]["MinimumTopup"].ToString());
                minTopup = Convert.ToInt32(textBoxCurrentBalance.Text);
                maxtopup = Convert.ToInt32(dt.Rows[0]["MaximumTopup"]);

                bool isPersonalize = Convert.ToBoolean(dt.Rows[0]["IsPersonlaized"]);
                CardType = dt.Rows[0]["Name"].ToString();
                if (isPersonalize)
                {
                    groupBoxCardHolderInfo.Visible = true;
                    groupBoxCardInfo.Visible = true;
                    RootobjectCardHolderInformation rootobjectCardHolderInformation = issueCardManager.GetCardHolderInformation(textBoxCardNumber.Text);
                    textBoxName.Text = rootobjectCardHolderInformation.responseObject.CardHolderName;
                    textBoxCNIC.Text = rootobjectCardHolderInformation.responseObject.CNIC;
                    textBoxPhone.Text = rootobjectCardHolderInformation.responseObject.Phone;
                    textBoxGender.Text = rootobjectCardHolderInformation.responseObject.Gender;
                    textBoxAddress.Text = rootobjectCardHolderInformation.responseObject.Address;
                    Convert.ToInt32(textBoxCurrentBalance.Text = rootobjectCardHolderInformation.responseObject.TopupBalance.ToString());
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The btnReadCard_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                var response = T10M1K.ReadCardSerial();
                response.isSuccess = true;
                if (response.isSuccess)
                {

                    RootobjectCardHolderInformation obj = cardBlock.GetCardHolderInformationBySerialNumber(response.ReturData.ToString());

                    if (obj.Message == "card not found")
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 0 && obj.responseObject != null)
                    {
                        MessageBox.Show("Card Not Initialized", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 1)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 2)
                    {
                        DataRow[] dr = dt.Select("CardTypeID='" + obj.responseObject.CardType + "'");

                        textBoxexpiry.Text = dt.Rows[0]["CardValidity"].ToString();
                        minTopup = Convert.ToInt32(dt.Rows[0]["MinimumTopup"].ToString());
                        maxtopup = Convert.ToInt32(dt.Rows[0]["MaximumTopup"]);

                        bool isPersonalize = Convert.ToBoolean(dt.Rows[0]["IsPersonlaized"]);
                        CardType = dt.Rows[0]["Name"].ToString();
                        comboBoxCardType.SelectedValue = obj.responseObject.CardType;
                        pictureBoxCardtopup.Visible = false;
                        panelMainScreen.Visible = true;
                        groupBoxCardHolderInfo.Visible = true;
                        textBoxCardNumber.Text = obj.responseObject.CardNumber;
                        CardSerialnumber = obj.responseObject.SerialNumber;
                        pictureBoxCardBlock.Visible = true;
                        pictureBoxUnblockCard.Visible = false;
                        textBoxReason.Visible = true;
                        labelReason.Visible = true;
                        textBoxCurrentBalance.Text = obj.responseObject.CurrentBalance.ToString();
                        textBoxexpiry.Text = obj.responseObject.expiryDate.ToString();
                        textBoxSearchByCNIC.Text = obj.responseObject.CNIC;
                        textBoxSearchByCardNo.Text = obj.responseObject.CardNumber;
                        if (isPersonalize)
                        {
                            groupBoxCardHolderInfo.Visible = true;
                            textBoxName.Text = obj.responseObject.Name;
                            textBoxCNIC.Text = obj.responseObject.CNIC;
                            textBoxPhone.Text = obj.responseObject.Phone;
                            textBoxGender.Text = obj.responseObject.Gender;
                            textBoxAddress.Text = obj.responseObject.Address;
                        }
                        else
                        {
                            groupBoxCardHolderInfo.Visible = false;
                        }


                    }
                    else if (obj.responseObject.CardStatus == 3)
                    {

                        DataRow[] dr = dt.Select("CardTypeID='" + obj.responseObject.CardType + "'");

                        textBoxexpiry.Text = dt.Rows[0]["CardValidity"].ToString();
                        minTopup = Convert.ToInt32(dt.Rows[0]["MinimumTopup"].ToString());
                        maxtopup = Convert.ToInt32(dt.Rows[0]["MaximumTopup"]);

                        bool isPersonalize = Convert.ToBoolean(dt.Rows[0]["IsPersonlaized"]);
                        CardType = dt.Rows[0]["Name"].ToString();
                        comboBoxCardType.SelectedValue = obj.responseObject.CardType;
                        pictureBoxCardtopup.Visible = false;
                        panelMainScreen.Visible = true;
                        groupBoxCardHolderInfo.Visible = true;
                        textBoxCardNumber.Text = obj.responseObject.CardNumber;
                        CardSerialnumber = obj.responseObject.SerialNumber;
                        pictureBoxUnblockCard.Visible = true;
                        pictureBoxCardBlock.Visible = false;
                        textBoxReason.Visible = false;
                        labelReason.Visible = false;
                        textBoxCurrentBalance.Text = obj.responseObject.CurrentBalance.ToString();
                        textBoxexpiry.Text = obj.responseObject.expiryDate.ToString();
                        textBoxSearchByCNIC.Text = obj.responseObject.CNIC;
                        textBoxSearchByCardNo.Text = obj.responseObject.CardNumber;
                        if (isPersonalize)
                        {
                            groupBoxCardHolderInfo.Visible = true;
                            textBoxName.Text = obj.responseObject.Name;
                            textBoxCNIC.Text = obj.responseObject.CNIC;
                            textBoxPhone.Text = obj.responseObject.Phone;
                            textBoxGender.Text = obj.responseObject.Gender;
                            textBoxAddress.Text = obj.responseObject.Address;
                        }
                        else
                        {
                            groupBoxCardHolderInfo.Visible = false;
                        }
                        //MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //pictureBoxCardtopup.Visible = true;
                        //panelMainScreen.Visible = false;
                        //pictureBoxUnblockCard.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("Put the card in scanner", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The btnRefresh_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            pictureBoxCardtopup.Visible = true;
            panelMainScreen.Visible = false;
        }

        /// <summary>
        /// The pictureBox17_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void pictureBox17_Click(object sender, EventArgs e)
        {
            try
            {
                btnReadCard_Click(null, null);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// The ClearData.
        /// </summary>
        private void ClearData()
        {
            textBoxName.Text = "";
            textBoxCNIC.Text = "";
            textBoxPhone.Text = "";
            textBoxGender.Text = "";
            textBoxAddress.Text = "";
            panelMainScreen.Visible = false;
            pictureBoxCardtopup.Visible = true;
            textBoxexpiry.Text = "";
            textBoxCurrentBalance.Text = "";
            textBoxSearchByCardNo.Text = "";
            textBoxSearchByCNIC.Text = "";
        }

        /// <summary>
        /// The textBoxTopupAmount_KeyPress.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="KeyPressEventArgs"/>.</param>
        private void textBoxTopupAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// The buttonBlockCard_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void buttonBlockCard_Click(object sender, EventArgs e)
        {
            LocalReport report = new LocalReport();
            try
            {
                if (string.IsNullOrEmpty(textBoxReason.Text))
                {
                    MessageBox.Show("Please Enter Reason for Card Blocking", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxReason.Focus();
                    return;
                }

                CardBlockViewModel cardBlockViewModel = new CardBlockViewModel();
                cardBlockViewModel.CardNumber = textBoxCardNumber.Text;
                cardBlockViewModel.SerialNumber = CardSerialnumber;
                cardBlockViewModel.UserID = StaticClassUserData.UserID;
                cardBlockViewModel.Reason = textBoxReason.Text;
                cardBlockViewModel.POS_Code = StaticClassSyncData.POS_Code;
                RootobjectCardBlock rootobjectCardBlock = cardBlock.BlockCard(cardBlockViewModel);
                if (rootobjectCardBlock.Heading == "Success")
                {
                    MessageBox.Show(rootobjectCardBlock.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LogManager logManager = new LogManager();
                    LogModel logModel = new LogModel();
                    logModel.UserID = StaticClassUserData.UserID;
                    logModel.ActionPerform = "Card Blocked),card number= " + cardBlockViewModel.CardNumber + "at " + cardBlockViewModel.POS_Code;
                    logModel.LoginDate = DateTime.Now.ToString();
                    logModel.UserName = StaticClassUserData.Name;
                    logModel.IP_Address = StaticClassSyncData.POS_IP;
                    logManager.SaveLog(logModel);
                    CardSerialnumber = "";
                    ClearData();
                }
                else
                {
                    MessageBox.Show(rootobjectCardBlock.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The pictureBox3_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                // Set cursor as hourglass
                Cursor.Current = Cursors.WaitCursor;
                buttonBlockCard_Click(null, null);
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// The buttonFind_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void buttonFind_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBoxSearchByCardNo.Text) && string.IsNullOrEmpty(textBoxSearchByCNIC.Text))
                {
                    MessageBox.Show("Please Enter CardNumber or CNIC for Search", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxSearchByCardNo.Focus();
                    return;
                }
                RootobjectCardHolderInformation obj = cardBlock.SearchCardByCardNumberCNIC(textBoxSearchByCardNo.Text, textBoxSearchByCNIC.Text);
                //RootobjectCardHolderInformation obj = cardBlock.GetCardHolderInformationBySerialNumber("BD6C6CBD"); 
                if (obj.Message == "card not found")
                {
                    MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pictureBoxCardtopup.Visible = true;
                    panelMainScreen.Visible = false;
                }
                if (obj.responseObject.CardStatus == 0 && obj.responseObject != null)
                {
                    MessageBox.Show("Card Not Initialized", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pictureBoxCardtopup.Visible = true;
                    panelMainScreen.Visible = false;
                }
                if (obj.responseObject.CardStatus == 1)
                {
                    MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pictureBoxCardtopup.Visible = true;
                    panelMainScreen.Visible = false;
                }
                else if (obj.responseObject.CardStatus == 2)
                {
                    DataRow[] dr = dt.Select("CardTypeID='" + obj.responseObject.CardType + "'");

                    textBoxexpiry.Text = dt.Rows[0]["CardValidity"].ToString();
                    minTopup = Convert.ToInt32(dt.Rows[0]["MinimumTopup"].ToString());
                    maxtopup = Convert.ToInt32(dt.Rows[0]["MaximumTopup"]);

                    bool isPersonalize = Convert.ToBoolean(dt.Rows[0]["IsPersonlaized"]);
                    CardType = dt.Rows[0]["Name"].ToString();
                    comboBoxCardType.SelectedValue = obj.responseObject.CardType;
                    pictureBoxCardtopup.Visible = false;
                    panelMainScreen.Visible = true;
                    groupBoxCardHolderInfo.Visible = true;
                    textBoxCardNumber.Text = obj.responseObject.CardNumber;
                    CardSerialnumber = obj.responseObject.SerialNumber;
                    pictureBoxCardBlock.Visible = true;
                    textBoxCurrentBalance.Text = obj.responseObject.CurrentBalance.ToString();
                    textBoxexpiry.Text = obj.responseObject.expiryDate.ToString();
                    textBoxSearchByCNIC.Text = obj.responseObject.CNIC;
                    textBoxSearchByCardNo.Text = obj.responseObject.CardNumber;
                    if (isPersonalize)
                    {
                        groupBoxCardHolderInfo.Visible = true;
                        textBoxName.Text = obj.responseObject.Name;
                        textBoxCNIC.Text = obj.responseObject.CNIC;
                        textBoxPhone.Text = obj.responseObject.Phone;
                        textBoxGender.Text = obj.responseObject.Gender;
                        textBoxAddress.Text = obj.responseObject.Address;
                    }
                    else
                    {
                        groupBoxCardHolderInfo.Visible = false;
                    }


                }
                else if (obj.responseObject.CardStatus == 3)
                {
                    DataRow[] dr = dt.Select("CardTypeID='" + obj.responseObject.CardType + "'");

                    textBoxexpiry.Text = dt.Rows[0]["CardValidity"].ToString();
                    minTopup = Convert.ToInt32(dt.Rows[0]["MinimumTopup"].ToString());
                    maxtopup = Convert.ToInt32(dt.Rows[0]["MaximumTopup"]);

                    bool isPersonalize = Convert.ToBoolean(dt.Rows[0]["IsPersonlaized"]);
                    CardType = dt.Rows[0]["Name"].ToString();
                    comboBoxCardType.SelectedValue = obj.responseObject.CardType;
                    pictureBoxCardtopup.Visible = false;
                    panelMainScreen.Visible = true;
                    groupBoxCardHolderInfo.Visible = true;
                    textBoxCardNumber.Text = obj.responseObject.CardNumber;
                    CardSerialnumber = obj.responseObject.SerialNumber;
                    pictureBoxUnblockCard.Visible = true;
                    textBoxCurrentBalance.Text = obj.responseObject.CurrentBalance.ToString();
                    textBoxexpiry.Text = obj.responseObject.expiryDate.ToString();
                    textBoxSearchByCNIC.Text = obj.responseObject.CNIC;
                    textBoxSearchByCardNo.Text = obj.responseObject.CardNumber;
                    if (isPersonalize)
                    {
                        groupBoxCardHolderInfo.Visible = true;
                        textBoxName.Text = obj.responseObject.Name;
                        textBoxCNIC.Text = obj.responseObject.CNIC;
                        textBoxPhone.Text = obj.responseObject.Phone;
                        textBoxGender.Text = obj.responseObject.Gender;
                        textBoxAddress.Text = obj.responseObject.Address;
                    }
                    else
                    {
                        groupBoxCardHolderInfo.Visible = false;
                    }
                    //MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //pictureBoxCardtopup.Visible = true;
                    //panelMainScreen.Visible = false;
                    //pictureBoxUnblockCard.Visible = false;
                }
                else
                {
                    MessageBox.Show("Put the card in scanner", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The pictureBoxUnblockCard_Click.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/>.</param>
        /// <param name="e">The e<see cref="EventArgs"/>.</param>
        private void pictureBoxUnblockCard_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxReason.Text = "test";
                buttonBlockCard_Click(null, null);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
