﻿
namespace POS
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnShift = new System.Windows.Forms.Button();
            this.pictureBoxUserProfile = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnIssueCard = new System.Windows.Forms.Button();
            this.btnTopUp = new System.Windows.Forms.Button();
            this.btnUpdateCard = new System.Windows.Forms.Button();
            this.btnRefund = new System.Windows.Forms.Button();
            this.btnCardInfo = new System.Windows.Forms.Button();
            this.btnTransferFundWithoutCard = new System.Windows.Forms.Button();
            this.btnTransferFundWithCard = new System.Windows.Forms.Button();
            this.btnSingleJourneyTicket = new System.Windows.Forms.Button();
            this.btnBlockCard = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btnEnter = new System.Windows.Forms.Button();
            this.btnZero = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.receiptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.buttonRegenerateQR = new System.Windows.Forms.Button();
            this.pictureBoxRegenerateQR = new System.Windows.Forms.PictureBox();
            this.pictureBoxUpdateCard = new System.Windows.Forms.PictureBox();
            this.pictureBoxSingleJourneyTicket = new System.Windows.Forms.PictureBox();
            this.pictureBoxCardBlock = new System.Windows.Forms.PictureBox();
            this.pictureBoxtransferWithCard = new System.Windows.Forms.PictureBox();
            this.pictureBoxtransferfundWithoutCard = new System.Windows.Forms.PictureBox();
            this.pictureBoxCardInfo = new System.Windows.Forms.PictureBox();
            this.pictureBoxRefund = new System.Windows.Forms.PictureBox();
            this.pictureBoxTopup = new System.Windows.Forms.PictureBox();
            this.pictureBoxIssueCard = new System.Windows.Forms.PictureBox();
            this.btnScanQR = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserProfile)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegenerateQR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUpdateCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSingleJourneyTicket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxtransferWithCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxtransferfundWithoutCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRefund)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIssueCard)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.btnShift);
            this.panel1.Controls.Add(this.pictureBoxUserProfile);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1294, 48);
            this.panel1.TabIndex = 0;
            // 
            // btnShift
            // 
            this.btnShift.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnShift.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShift.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnShift.Location = new System.Drawing.Point(1103, 0);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(91, 48);
            this.btnShift.TabIndex = 44;
            this.btnShift.Text = "Start Shift";
            this.btnShift.UseVisualStyleBackColor = false;
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // pictureBoxUserProfile
            // 
            this.pictureBoxUserProfile.BackColor = System.Drawing.Color.White;
            this.pictureBoxUserProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxUserProfile.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxUserProfile.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxUserProfile.Image")));
            this.pictureBoxUserProfile.Location = new System.Drawing.Point(1194, 0);
            this.pictureBoxUserProfile.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxUserProfile.Name = "pictureBoxUserProfile";
            this.pictureBoxUserProfile.Size = new System.Drawing.Size(51, 48);
            this.pictureBoxUserProfile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUserProfile.TabIndex = 43;
            this.pictureBoxUserProfile.TabStop = false;
            this.pictureBoxUserProfile.Click += new System.EventHandler(this.pictureBoxUserProfile_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox13);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1245, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(49, 48);
            this.panel2.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(-3, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 9;
            this.label1.Text = "Logout";
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.White;
            this.pictureBox13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(2, 2);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(36, 30);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 8;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Click += new System.EventHandler(this.pictureBox13_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnIssueCard
            // 
            this.btnIssueCard.BackColor = System.Drawing.Color.White;
            this.btnIssueCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIssueCard.Enabled = false;
            this.btnIssueCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIssueCard.Location = new System.Drawing.Point(21, 75);
            this.btnIssueCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnIssueCard.Name = "btnIssueCard";
            this.btnIssueCard.Size = new System.Drawing.Size(143, 119);
            this.btnIssueCard.TabIndex = 1;
            this.btnIssueCard.Text = "Issue Card";
            this.btnIssueCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnIssueCard.UseVisualStyleBackColor = false;
            this.btnIssueCard.Click += new System.EventHandler(this.btnIssueCard_Click);
            // 
            // btnTopUp
            // 
            this.btnTopUp.BackColor = System.Drawing.Color.White;
            this.btnTopUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTopUp.Enabled = false;
            this.btnTopUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTopUp.Location = new System.Drawing.Point(165, 75);
            this.btnTopUp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTopUp.Name = "btnTopUp";
            this.btnTopUp.Size = new System.Drawing.Size(143, 119);
            this.btnTopUp.TabIndex = 3;
            this.btnTopUp.Text = "Top Up";
            this.btnTopUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTopUp.UseVisualStyleBackColor = false;
            this.btnTopUp.Click += new System.EventHandler(this.btnTopUp_Click);
            // 
            // btnUpdateCard
            // 
            this.btnUpdateCard.BackColor = System.Drawing.Color.White;
            this.btnUpdateCard.Enabled = false;
            this.btnUpdateCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCard.Location = new System.Drawing.Point(453, 76);
            this.btnUpdateCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateCard.Name = "btnUpdateCard";
            this.btnUpdateCard.Size = new System.Drawing.Size(143, 119);
            this.btnUpdateCard.TabIndex = 6;
            this.btnUpdateCard.Text = "Update Card";
            this.btnUpdateCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnUpdateCard.UseVisualStyleBackColor = false;
            // 
            // btnRefund
            // 
            this.btnRefund.BackColor = System.Drawing.Color.White;
            this.btnRefund.Enabled = false;
            this.btnRefund.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefund.Location = new System.Drawing.Point(20, 196);
            this.btnRefund.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefund.Name = "btnRefund";
            this.btnRefund.Size = new System.Drawing.Size(143, 119);
            this.btnRefund.TabIndex = 11;
            this.btnRefund.Text = "Refund";
            this.btnRefund.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRefund.UseVisualStyleBackColor = false;
            // 
            // btnCardInfo
            // 
            this.btnCardInfo.BackColor = System.Drawing.Color.White;
            this.btnCardInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCardInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCardInfo.Location = new System.Drawing.Point(165, 197);
            this.btnCardInfo.Margin = new System.Windows.Forms.Padding(2);
            this.btnCardInfo.Name = "btnCardInfo";
            this.btnCardInfo.Size = new System.Drawing.Size(143, 119);
            this.btnCardInfo.TabIndex = 10;
            this.btnCardInfo.Text = "Card Info";
            this.btnCardInfo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCardInfo.UseVisualStyleBackColor = false;
            // 
            // btnTransferFundWithoutCard
            // 
            this.btnTransferFundWithoutCard.BackColor = System.Drawing.Color.White;
            this.btnTransferFundWithoutCard.Enabled = false;
            this.btnTransferFundWithoutCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransferFundWithoutCard.Location = new System.Drawing.Point(20, 317);
            this.btnTransferFundWithoutCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnTransferFundWithoutCard.Name = "btnTransferFundWithoutCard";
            this.btnTransferFundWithoutCard.Size = new System.Drawing.Size(143, 119);
            this.btnTransferFundWithoutCard.TabIndex = 13;
            this.btnTransferFundWithoutCard.Text = "Transfer Fund Without Card";
            this.btnTransferFundWithoutCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTransferFundWithoutCard.UseVisualStyleBackColor = false;
            // 
            // btnTransferFundWithCard
            // 
            this.btnTransferFundWithCard.BackColor = System.Drawing.Color.White;
            this.btnTransferFundWithCard.Enabled = false;
            this.btnTransferFundWithCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransferFundWithCard.Location = new System.Drawing.Point(165, 318);
            this.btnTransferFundWithCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnTransferFundWithCard.Name = "btnTransferFundWithCard";
            this.btnTransferFundWithCard.Size = new System.Drawing.Size(143, 119);
            this.btnTransferFundWithCard.TabIndex = 12;
            this.btnTransferFundWithCard.Text = "Transfer Fund With Card";
            this.btnTransferFundWithCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTransferFundWithCard.UseVisualStyleBackColor = false;
            // 
            // btnSingleJourneyTicket
            // 
            this.btnSingleJourneyTicket.BackColor = System.Drawing.Color.White;
            this.btnSingleJourneyTicket.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSingleJourneyTicket.Enabled = false;
            this.btnSingleJourneyTicket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSingleJourneyTicket.Location = new System.Drawing.Point(20, 439);
            this.btnSingleJourneyTicket.Margin = new System.Windows.Forms.Padding(2);
            this.btnSingleJourneyTicket.Name = "btnSingleJourneyTicket";
            this.btnSingleJourneyTicket.Size = new System.Drawing.Size(143, 119);
            this.btnSingleJourneyTicket.TabIndex = 15;
            this.btnSingleJourneyTicket.Text = "Single Journey Ticket";
            this.btnSingleJourneyTicket.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSingleJourneyTicket.UseVisualStyleBackColor = false;
            this.btnSingleJourneyTicket.Click += new System.EventHandler(this.btnSingleJourneyTicket_Click);
            // 
            // btnBlockCard
            // 
            this.btnBlockCard.BackColor = System.Drawing.SystemColors.Window;
            this.btnBlockCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBlockCard.Enabled = false;
            this.btnBlockCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBlockCard.Location = new System.Drawing.Point(308, 76);
            this.btnBlockCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnBlockCard.Name = "btnBlockCard";
            this.btnBlockCard.Size = new System.Drawing.Size(143, 119);
            this.btnBlockCard.TabIndex = 14;
            this.btnBlockCard.Text = "Block Card";
            this.btnBlockCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBlockCard.UseVisualStyleBackColor = false;
            this.btnBlockCard.Click += new System.EventHandler(this.btnBlockCard_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.ForeColor = System.Drawing.Color.Transparent;
            this.btn1.Location = new System.Drawing.Point(322, 199);
            this.btn1.Margin = new System.Windows.Forms.Padding(2);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(91, 89);
            this.btn1.TabIndex = 22;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.ForeColor = System.Drawing.Color.Transparent;
            this.btn2.Location = new System.Drawing.Point(414, 199);
            this.btn2.Margin = new System.Windows.Forms.Padding(2);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(91, 89);
            this.btn2.TabIndex = 23;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.ForeColor = System.Drawing.Color.Transparent;
            this.btn3.Location = new System.Drawing.Point(506, 200);
            this.btn3.Margin = new System.Windows.Forms.Padding(2);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(91, 89);
            this.btn3.TabIndex = 24;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.ForeColor = System.Drawing.Color.Transparent;
            this.btn6.Location = new System.Drawing.Point(506, 288);
            this.btn6.Margin = new System.Windows.Forms.Padding(2);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(91, 89);
            this.btn6.TabIndex = 27;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.ForeColor = System.Drawing.Color.Transparent;
            this.btn5.Location = new System.Drawing.Point(414, 288);
            this.btn5.Margin = new System.Windows.Forms.Padding(2);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(91, 89);
            this.btn5.TabIndex = 26;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.ForeColor = System.Drawing.Color.Transparent;
            this.btn4.Location = new System.Drawing.Point(322, 288);
            this.btn4.Margin = new System.Windows.Forms.Padding(2);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(91, 89);
            this.btn4.TabIndex = 25;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.ForeColor = System.Drawing.Color.Transparent;
            this.btn9.Location = new System.Drawing.Point(506, 377);
            this.btn9.Margin = new System.Windows.Forms.Padding(2);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(91, 89);
            this.btn9.TabIndex = 30;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.ForeColor = System.Drawing.Color.Transparent;
            this.btn8.Location = new System.Drawing.Point(415, 376);
            this.btn8.Margin = new System.Windows.Forms.Padding(2);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(91, 89);
            this.btn8.TabIndex = 29;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.ForeColor = System.Drawing.Color.Transparent;
            this.btn7.Location = new System.Drawing.Point(323, 376);
            this.btn7.Margin = new System.Windows.Forms.Padding(2);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(91, 89);
            this.btn7.TabIndex = 28;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            // 
            // btnEnter
            // 
            this.btnEnter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btnEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnter.ForeColor = System.Drawing.Color.Transparent;
            this.btnEnter.Location = new System.Drawing.Point(506, 466);
            this.btnEnter.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(91, 89);
            this.btnEnter.TabIndex = 33;
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = false;
            // 
            // btnZero
            // 
            this.btnZero.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btnZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZero.ForeColor = System.Drawing.Color.Transparent;
            this.btnZero.Location = new System.Drawing.Point(415, 465);
            this.btnZero.Margin = new System.Windows.Forms.Padding(2);
            this.btnZero.Name = "btnZero";
            this.btnZero.Size = new System.Drawing.Size(91, 89);
            this.btnZero.TabIndex = 32;
            this.btnZero.Text = "0";
            this.btnZero.UseVisualStyleBackColor = false;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(76)))), ((int)(((byte)(172)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Transparent;
            this.btnClear.Location = new System.Drawing.Point(323, 465);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(91, 89);
            this.btnClear.TabIndex = 31;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.pictureBox16);
            this.panel3.Controls.Add(this.pictureBox15);
            this.panel3.Controls.Add(this.lblVersion);
            this.panel3.Controls.Add(this.pictureBox14);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 674);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1294, 30);
            this.panel3.TabIndex = 36;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(1222, 0);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(32, 30);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 41;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Click += new System.EventHandler(this.pictureBox16_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox15.Location = new System.Drawing.Point(1254, 0);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(8, 30);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 40;
            this.pictureBox15.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(11, 4);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(49, 17);
            this.lblVersion.TabIndex = 39;
            this.lblVersion.Text = "Version";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(1262, 0);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(32, 30);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 37;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Click += new System.EventHandler(this.pictureBox14_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.receiptViewer);
            this.panel4.Location = new System.Drawing.Point(608, 76);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(664, 551);
            this.panel4.TabIndex = 38;
            // 
            // receiptViewer
            // 
            this.receiptViewer.DocumentMapWidth = 45;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = null;
            this.receiptViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.receiptViewer.LocalReport.ReportEmbeddedResource = "POS.Reports.QRPrint1.rdlc";
            this.receiptViewer.Location = new System.Drawing.Point(607, 478);
            this.receiptViewer.Name = "receiptViewer";
            this.receiptViewer.ServerReport.BearerToken = null;
            this.receiptViewer.Size = new System.Drawing.Size(47, 60);
            this.receiptViewer.TabIndex = 48;
            this.receiptViewer.Visible = false;
            // 
            // buttonRegenerateQR
            // 
            this.buttonRegenerateQR.BackColor = System.Drawing.Color.White;
            this.buttonRegenerateQR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRegenerateQR.Enabled = false;
            this.buttonRegenerateQR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegenerateQR.Location = new System.Drawing.Point(165, 439);
            this.buttonRegenerateQR.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRegenerateQR.Name = "buttonRegenerateQR";
            this.buttonRegenerateQR.Size = new System.Drawing.Size(143, 119);
            this.buttonRegenerateQR.TabIndex = 40;
            this.buttonRegenerateQR.Text = "Regenrate QR";
            this.buttonRegenerateQR.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonRegenerateQR.UseVisualStyleBackColor = false;
            this.buttonRegenerateQR.Click += new System.EventHandler(this.buttonRegenerateQR_Click);
            // 
            // pictureBoxRegenerateQR
            // 
            this.pictureBoxRegenerateQR.BackColor = System.Drawing.Color.White;
            this.pictureBoxRegenerateQR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxRegenerateQR.Enabled = false;
            this.pictureBoxRegenerateQR.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRegenerateQR.Image")));
            this.pictureBoxRegenerateQR.Location = new System.Drawing.Point(208, 443);
            this.pictureBoxRegenerateQR.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxRegenerateQR.Name = "pictureBoxRegenerateQR";
            this.pictureBoxRegenerateQR.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxRegenerateQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRegenerateQR.TabIndex = 41;
            this.pictureBoxRegenerateQR.TabStop = false;
            this.pictureBoxRegenerateQR.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBoxUpdateCard
            // 
            this.pictureBoxUpdateCard.BackColor = System.Drawing.Color.White;
            this.pictureBoxUpdateCard.Enabled = false;
            this.pictureBoxUpdateCard.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxUpdateCard.Image")));
            this.pictureBoxUpdateCard.Location = new System.Drawing.Point(494, 99);
            this.pictureBoxUpdateCard.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxUpdateCard.Name = "pictureBoxUpdateCard";
            this.pictureBoxUpdateCard.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxUpdateCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUpdateCard.TabIndex = 7;
            this.pictureBoxUpdateCard.TabStop = false;
            // 
            // pictureBoxSingleJourneyTicket
            // 
            this.pictureBoxSingleJourneyTicket.BackColor = System.Drawing.Color.White;
            this.pictureBoxSingleJourneyTicket.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxSingleJourneyTicket.Enabled = false;
            this.pictureBoxSingleJourneyTicket.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSingleJourneyTicket.Image")));
            this.pictureBoxSingleJourneyTicket.Location = new System.Drawing.Point(52, 443);
            this.pictureBoxSingleJourneyTicket.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxSingleJourneyTicket.Name = "pictureBoxSingleJourneyTicket";
            this.pictureBoxSingleJourneyTicket.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxSingleJourneyTicket.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSingleJourneyTicket.TabIndex = 21;
            this.pictureBoxSingleJourneyTicket.TabStop = false;
            this.pictureBoxSingleJourneyTicket.Click += new System.EventHandler(this.pictureBox12_Click);
            // 
            // pictureBoxCardBlock
            // 
            this.pictureBoxCardBlock.BackColor = System.Drawing.Color.White;
            this.pictureBoxCardBlock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxCardBlock.Enabled = false;
            this.pictureBoxCardBlock.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCardBlock.Image")));
            this.pictureBoxCardBlock.Location = new System.Drawing.Point(348, 99);
            this.pictureBoxCardBlock.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxCardBlock.Name = "pictureBoxCardBlock";
            this.pictureBoxCardBlock.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxCardBlock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCardBlock.TabIndex = 20;
            this.pictureBoxCardBlock.TabStop = false;
            // 
            // pictureBoxtransferWithCard
            // 
            this.pictureBoxtransferWithCard.BackColor = System.Drawing.Color.White;
            this.pictureBoxtransferWithCard.Enabled = false;
            this.pictureBoxtransferWithCard.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxtransferWithCard.Image")));
            this.pictureBoxtransferWithCard.Location = new System.Drawing.Point(208, 325);
            this.pictureBoxtransferWithCard.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxtransferWithCard.Name = "pictureBoxtransferWithCard";
            this.pictureBoxtransferWithCard.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxtransferWithCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxtransferWithCard.TabIndex = 19;
            this.pictureBoxtransferWithCard.TabStop = false;
            // 
            // pictureBoxtransferfundWithoutCard
            // 
            this.pictureBoxtransferfundWithoutCard.BackColor = System.Drawing.Color.White;
            this.pictureBoxtransferfundWithoutCard.Enabled = false;
            this.pictureBoxtransferfundWithoutCard.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxtransferfundWithoutCard.Image")));
            this.pictureBoxtransferfundWithoutCard.Location = new System.Drawing.Point(52, 325);
            this.pictureBoxtransferfundWithoutCard.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxtransferfundWithoutCard.Name = "pictureBoxtransferfundWithoutCard";
            this.pictureBoxtransferfundWithoutCard.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxtransferfundWithoutCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxtransferfundWithoutCard.TabIndex = 18;
            this.pictureBoxtransferfundWithoutCard.TabStop = false;
            // 
            // pictureBoxCardInfo
            // 
            this.pictureBoxCardInfo.BackColor = System.Drawing.Color.White;
            this.pictureBoxCardInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxCardInfo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCardInfo.Image")));
            this.pictureBoxCardInfo.Location = new System.Drawing.Point(208, 217);
            this.pictureBoxCardInfo.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxCardInfo.Name = "pictureBoxCardInfo";
            this.pictureBoxCardInfo.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxCardInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCardInfo.TabIndex = 17;
            this.pictureBoxCardInfo.TabStop = false;
            // 
            // pictureBoxRefund
            // 
            this.pictureBoxRefund.BackColor = System.Drawing.Color.White;
            this.pictureBoxRefund.Enabled = false;
            this.pictureBoxRefund.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRefund.Image")));
            this.pictureBoxRefund.Location = new System.Drawing.Point(52, 217);
            this.pictureBoxRefund.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxRefund.Name = "pictureBoxRefund";
            this.pictureBoxRefund.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxRefund.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRefund.TabIndex = 16;
            this.pictureBoxRefund.TabStop = false;
            // 
            // pictureBoxTopup
            // 
            this.pictureBoxTopup.BackColor = System.Drawing.Color.White;
            this.pictureBoxTopup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxTopup.Enabled = false;
            this.pictureBoxTopup.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTopup.Image")));
            this.pictureBoxTopup.Location = new System.Drawing.Point(208, 99);
            this.pictureBoxTopup.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxTopup.Name = "pictureBoxTopup";
            this.pictureBoxTopup.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxTopup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxTopup.TabIndex = 5;
            this.pictureBoxTopup.TabStop = false;
            this.pictureBoxTopup.Click += new System.EventHandler(this.pictureBoxTopup_Click);
            // 
            // pictureBoxIssueCard
            // 
            this.pictureBoxIssueCard.BackColor = System.Drawing.Color.White;
            this.pictureBoxIssueCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxIssueCard.Enabled = false;
            this.pictureBoxIssueCard.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxIssueCard.Image")));
            this.pictureBoxIssueCard.Location = new System.Drawing.Point(52, 99);
            this.pictureBoxIssueCard.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxIssueCard.Name = "pictureBoxIssueCard";
            this.pictureBoxIssueCard.Size = new System.Drawing.Size(65, 63);
            this.pictureBoxIssueCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxIssueCard.TabIndex = 2;
            this.pictureBoxIssueCard.TabStop = false;
            this.pictureBoxIssueCard.Click += new System.EventHandler(this.pictureBoxIssueCard_Click);
            // 
            // btnScanQR
            // 
            this.btnScanQR.BackColor = System.Drawing.Color.White;
            this.btnScanQR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnScanQR.Enabled = false;
            this.btnScanQR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScanQR.Location = new System.Drawing.Point(21, 562);
            this.btnScanQR.Margin = new System.Windows.Forms.Padding(2);
            this.btnScanQR.Name = "btnScanQR";
            this.btnScanQR.Size = new System.Drawing.Size(575, 53);
            this.btnScanQR.TabIndex = 43;
            this.btnScanQR.Text = "Scan QR";
            this.btnScanQR.UseVisualStyleBackColor = false;
            this.btnScanQR.Click += new System.EventHandler(this.btnScanQR_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(1294, 704);
            this.Controls.Add(this.btnScanQR);
            this.Controls.Add(this.pictureBoxRegenerateQR);
            this.Controls.Add(this.buttonRegenerateQR);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pictureBoxUpdateCard);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.btnZero);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.pictureBoxSingleJourneyTicket);
            this.Controls.Add(this.pictureBoxCardBlock);
            this.Controls.Add(this.pictureBoxtransferWithCard);
            this.Controls.Add(this.pictureBoxtransferfundWithoutCard);
            this.Controls.Add(this.pictureBoxCardInfo);
            this.Controls.Add(this.pictureBoxRefund);
            this.Controls.Add(this.btnSingleJourneyTicket);
            this.Controls.Add(this.btnBlockCard);
            this.Controls.Add(this.btnTransferFundWithoutCard);
            this.Controls.Add(this.btnTransferFundWithCard);
            this.Controls.Add(this.btnRefund);
            this.Controls.Add(this.btnCardInfo);
            this.Controls.Add(this.btnUpdateCard);
            this.Controls.Add(this.pictureBoxTopup);
            this.Controls.Add(this.btnTopUp);
            this.Controls.Add(this.pictureBoxIssueCard);
            this.Controls.Add(this.btnIssueCard);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserProfile)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegenerateQR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUpdateCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSingleJourneyTicket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxtransferWithCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxtransferfundWithoutCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRefund)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIssueCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBoxIssueCard;
        private System.Windows.Forms.Button btnTopUp;
        private System.Windows.Forms.PictureBox pictureBoxTopup;
        private System.Windows.Forms.Button btnUpdateCard;
        private System.Windows.Forms.PictureBox pictureBoxUpdateCard;
        private System.Windows.Forms.Button btnRefund;
        private System.Windows.Forms.Button btnCardInfo;
        private System.Windows.Forms.Button btnTransferFundWithoutCard;
        private System.Windows.Forms.Button btnTransferFundWithCard;
        private System.Windows.Forms.PictureBox pictureBoxRefund;
        private System.Windows.Forms.PictureBox pictureBoxCardInfo;
        private System.Windows.Forms.PictureBox pictureBoxtransferfundWithoutCard;
        private System.Windows.Forms.PictureBox pictureBoxtransferWithCard;
        private System.Windows.Forms.PictureBox pictureBoxCardBlock;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Button btnZero;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.PictureBox pictureBoxRegenerateQR;
        private System.Windows.Forms.Button buttonRegenerateQR;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        public System.Windows.Forms.Button btnBlockCard;
        public System.Windows.Forms.Button btnSingleJourneyTicket;
        public System.Windows.Forms.PictureBox pictureBoxSingleJourneyTicket;
        public System.Windows.Forms.Button btnIssueCard;
        private System.Windows.Forms.PictureBox pictureBoxUserProfile;
        private System.Windows.Forms.Button btnScanQR;
        private System.Windows.Forms.Button btnShift;
        private Microsoft.Reporting.WinForms.ReportViewer receiptViewer;
    }
}

