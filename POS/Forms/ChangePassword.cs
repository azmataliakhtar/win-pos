﻿using BLL.ChangePasswordManager;
using BLL.Constants;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using System;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            LogManager logManager = new LogManager();
            if (string.IsNullOrEmpty(textBoxEmail.Text))
            {
                errorProvider1.SetError(textBoxEmail, "Please Enter Email");
                textBoxEmail.Focus();
                return;
            }
            else
            {
                errorProvider1.Clear();
            }
            if (string.IsNullOrEmpty(textBoxOldPassword.Text))
            {
                errorProvider1.SetError(textBoxOldPassword, "Please Enter Old Password");
                textBoxOldPassword.Focus();
                return;
            }
            else
            {
                errorProvider1.Clear();
            }
            if (string.IsNullOrEmpty(textBoxNewPassword.Text))
            {
                errorProvider1.SetError(textBoxNewPassword, "Please Enter New Password");
                textBoxNewPassword.Focus();
                return;
            }
            else
            {
                errorProvider1.Clear();
            }
            if (textBoxNewPassword.Text == textBoxOldPassword.Text)
            {
                errorProvider1.SetError(textBoxNewPassword, "Old Password is same as new password");
                textBoxNewPassword.Focus();
                return;
            }
            else
            {
                errorProvider1.Clear();
            }

            ChangePasswordManager changePasswordManager = new ChangePasswordManager();
           var Result= changePasswordManager.ChangePassword(textBoxEmail.Text,textBoxOldPassword.Text,textBoxNewPassword.Text);
            if (Result.IsSuccess)
            {
                MessageBox.Show(Result.Message);
                LogModel logModel = new LogModel();
                logModel.UserID = StaticClassUserData.UserID;
                logModel.ActionPerform = "Password Changed For user " + textBoxEmail.Text+"Old Password was ="+textBoxOldPassword.Text+"new password="+textBoxNewPassword.Text;
                logModel.LoginDate = DateTime.Now.ToString();
                logModel.UserName = StaticClassUserData.Name;
                logModel.IP_Address = StaticClassSyncData.POS_IP;
                logManager.SaveLog(logModel);

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name != "Login")
                        Application.OpenForms[i].Close();
                    else
                        Application.OpenForms[i].Show();
                }

                Constants.clearStaticObject();
            }
            else
            {

                MessageBox.Show(Result.Message);
            }
            
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            textBoxEmail.Text = StaticClassUserData.Email;
        }
    }
}
