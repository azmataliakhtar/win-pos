﻿using CryptoUtil;
using System;
using System.Windows.Forms;
using BLL.QRScanManager;
using BLL.Models;
using Newtonsoft.Json;
using BLL.Models.ViewModel;
using System.Media;

namespace POS.Forms
{
    public partial class QRScanForm : Form
    {
        public string ScanQueueText { get; set; }
        public bool IsScanModeCheckin { get; set; }

        public QRScanForm()
        {
            InitializeComponent();
            IsScanModeCheckin = false;
            btnScanMode.Text = "Select Mode";
            lblScanQ.Text = "Select Mode";
        }
        private void readTicket(string qrcode)
        {
            try
            {
                QRScanResult result = null;
                CryptoUtility cryptoUtil = new CryptoUtility();
                var decryptedText = cryptoUtil.DecryptRSA(qrcode);
                var qRScanManager = new QRScanManager();

                var scanModel = JsonConvert.DeserializeObject<QRScanModel>(decryptedText);

                if (IsScanModeCheckin)
                   result = qRScanManager.ScanQRCheckin(scanModel);
                else
                   result = qRScanManager.ScanQRCheckout(scanModel);

                if (result.IsSuccess)
                {
                    SoundPlayer splayer = new SoundPlayer(Properties.Resources.Success);
                    splayer.Play();
                }
                else
                { 
                    SoundPlayer splayer = new SoundPlayer(Properties.Resources.Error);
                    splayer.Play();
                    MessageBox.Show(result.Message, "QR Code Scan Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                SoundPlayer splayer = new SoundPlayer(Properties.Resources.Error);
                splayer.Play();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                txtQR.Text = "";
            }
        }

        private void btnScanMode_Click(object sender, EventArgs e)
        {
            IsScanModeCheckin = !IsScanModeCheckin;
            btnScanMode.Text = IsScanModeCheckin ? "Check In" : "Check Out";
            txtQR.Text = "";
            txtQR.Focus();
            lblScanQ.Text = "Ready to scan";
        }

        private void txtQR_KeyUp(object sender, KeyEventArgs e)
        {
            lblScanQ.Text = "Scanning";
            if(e.KeyCode == Keys.Enter)
            {
                var QRText = txtQR.Text;
                readTicket(QRText);
                lblScanQ.Text = "Ready to scan";
            }
        }



        //private void txtQR_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        var txt = txtQR.Text;
        //    }
        //}

        //private void QRScanForm_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if(IsScanEnabled)
        //    {
        //        if(e.KeyCode == Keys.Enter)
        //        {
        //            ScanQue.Text = "Scan Complete";
        //        }
        //        else 
        //        {
        //            ScanQue.Text = "Scanning......";
        //            QRText += Convert.ToChar(e.KeyCode).ToString();
        //        }
        //    }
        //}

        //private void QRScanForm_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    var check = e.KeyChar;
        //}

        //private void QRScanForm_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (IsScanEnabled)
        //    {
        //        if (e.KeyCode == Keys.Enter)
        //        {
        //            ScanQue.Text = "Scan Complete";
        //        }
        //        else
        //        {
        //            ScanQue.Text = "Scanning......";
        //            QRText += Convert.ToChar(e.KeyCode).ToString();
        //        }
        //    }
        //}

        //private void QRScanForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        //{
        //    var check = e.KeyCode;
        //}


        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    if (IsScanEnabled)
        //    {
        //        ScanQue.Text = "Scanning....";
        //        if (keyData == Keys.Enter)
        //        {
        //            // Handle key at form level.
        //            // Do not send event to focused control by returning true.
        //            ScanQue.Text = "Scan Complete";
        //        }
        //        else
        //            QRText += keyData.ToString();

        //        return true;
        //    }
        //    return base.ProcessCmdKey(ref msg, keyData);
        //}
    }
}
