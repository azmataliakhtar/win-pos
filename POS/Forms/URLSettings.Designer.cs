﻿
namespace POS.Forms
{
    partial class URLSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(URLSettings));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxIPAddress1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPort1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTest1 = new System.Windows.Forms.Button();
            this.buttonTest2 = new System.Windows.Forms.Button();
            this.textBoxPort2 = new System.Windows.Forms.TextBox();
            this.textBoxIPAddress2 = new System.Windows.Forms.TextBox();
            this.buttonTestPrinter = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textBoxServiceName2 = new System.Windows.Forms.TextBox();
            this.textBoxServiceName1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSaveURLSettings = new System.Windows.Forms.Button();
            this.textBoxID2 = new System.Windows.Forms.TextBox();
            this.textBoxID1 = new System.Windows.Forms.TextBox();
            this.textBoxID3 = new System.Windows.Forms.TextBox();
            this.textBoxServiceName3 = new System.Windows.Forms.TextBox();
            this.buttonTest3 = new System.Windows.Forms.Button();
            this.textBoxPort3 = new System.Windows.Forms.TextBox();
            this.textBoxIPAddress3 = new System.Windows.Forms.TextBox();
            this.textBoxID4 = new System.Windows.Forms.TextBox();
            this.textBoxServiceName4 = new System.Windows.Forms.TextBox();
            this.buttonTest4 = new System.Windows.Forms.Button();
            this.textBoxPort4 = new System.Windows.Forms.TextBox();
            this.textBoxIPAddress4 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.panel1.Controls.Add(this.label8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(635, 65);
            this.panel1.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(237, 21);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(155, 23);
            this.label8.TabIndex = 0;
            this.label8.Text = "Service url settings";
            // 
            // textBoxIPAddress1
            // 
            this.textBoxIPAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIPAddress1.Location = new System.Drawing.Point(241, 130);
            this.textBoxIPAddress1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIPAddress1.Name = "textBoxIPAddress1";
            this.textBoxIPAddress1.Size = new System.Drawing.Size(158, 26);
            this.textBoxIPAddress1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(237, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "IP Address";
            // 
            // textBoxPort1
            // 
            this.textBoxPort1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPort1.Location = new System.Drawing.Point(403, 130);
            this.textBoxPort1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPort1.Name = "textBoxPort1";
            this.textBoxPort1.Size = new System.Drawing.Size(80, 26);
            this.textBoxPort1.TabIndex = 20;
            this.textBoxPort1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPort_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(404, 103);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "Port Number";
            // 
            // btnTest1
            // 
            this.btnTest1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.btnTest1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTest1.ForeColor = System.Drawing.Color.Transparent;
            this.btnTest1.Location = new System.Drawing.Point(501, 125);
            this.btnTest1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnTest1.Name = "btnTest1";
            this.btnTest1.Size = new System.Drawing.Size(74, 28);
            this.btnTest1.TabIndex = 37;
            this.btnTest1.Text = "Test";
            this.btnTest1.UseVisualStyleBackColor = false;
            this.btnTest1.Click += new System.EventHandler(this.btnSyncData_Click);
            // 
            // buttonTest2
            // 
            this.buttonTest2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.buttonTest2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTest2.ForeColor = System.Drawing.Color.Transparent;
            this.buttonTest2.Location = new System.Drawing.Point(501, 161);
            this.buttonTest2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonTest2.Name = "buttonTest2";
            this.buttonTest2.Size = new System.Drawing.Size(74, 27);
            this.buttonTest2.TabIndex = 42;
            this.buttonTest2.Text = "Test";
            this.buttonTest2.UseVisualStyleBackColor = false;
            this.buttonTest2.Click += new System.EventHandler(this.buttonTest2_Click);
            // 
            // textBoxPort2
            // 
            this.textBoxPort2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPort2.Location = new System.Drawing.Point(403, 161);
            this.textBoxPort2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPort2.Name = "textBoxPort2";
            this.textBoxPort2.Size = new System.Drawing.Size(80, 26);
            this.textBoxPort2.TabIndex = 41;
            // 
            // textBoxIPAddress2
            // 
            this.textBoxIPAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIPAddress2.Location = new System.Drawing.Point(241, 161);
            this.textBoxIPAddress2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIPAddress2.Name = "textBoxIPAddress2";
            this.textBoxIPAddress2.Size = new System.Drawing.Size(158, 26);
            this.textBoxIPAddress2.TabIndex = 39;
            // 
            // buttonTestPrinter
            // 
            this.buttonTestPrinter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.buttonTestPrinter.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTestPrinter.ForeColor = System.Drawing.Color.Transparent;
            this.buttonTestPrinter.Location = new System.Drawing.Point(359, 348);
            this.buttonTestPrinter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonTestPrinter.Name = "buttonTestPrinter";
            this.buttonTestPrinter.Size = new System.Drawing.Size(123, 29);
            this.buttonTestPrinter.TabIndex = 49;
            this.buttonTestPrinter.Text = "Test Printer";
            this.buttonTestPrinter.UseVisualStyleBackColor = false;
            this.buttonTestPrinter.Click += new System.EventHandler(this.buttonTestPrinter_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.button5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Transparent;
            this.button5.Location = new System.Drawing.Point(493, 348);
            this.button5.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 29);
            this.button5.TabIndex = 50;
            this.button5.Text = "Test RFID Reader";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(273, 227);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(15, 204);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(254, 173);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 52;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // textBoxServiceName2
            // 
            this.textBoxServiceName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServiceName2.Location = new System.Drawing.Point(50, 161);
            this.textBoxServiceName2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxServiceName2.Name = "textBoxServiceName2";
            this.textBoxServiceName2.ReadOnly = true;
            this.textBoxServiceName2.Size = new System.Drawing.Size(187, 26);
            this.textBoxServiceName2.TabIndex = 55;
            // 
            // textBoxServiceName1
            // 
            this.textBoxServiceName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServiceName1.Location = new System.Drawing.Point(50, 130);
            this.textBoxServiceName1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxServiceName1.Name = "textBoxServiceName1";
            this.textBoxServiceName1.ReadOnly = true;
            this.textBoxServiceName1.Size = new System.Drawing.Size(187, 26);
            this.textBoxServiceName1.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 53;
            this.label3.Text = "Service Name";
            // 
            // buttonSaveURLSettings
            // 
            this.buttonSaveURLSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(176)))), ((int)(((byte)(223)))));
            this.buttonSaveURLSettings.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveURLSettings.ForeColor = System.Drawing.Color.Transparent;
            this.buttonSaveURLSettings.Location = new System.Drawing.Point(135, 264);
            this.buttonSaveURLSettings.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonSaveURLSettings.Name = "buttonSaveURLSettings";
            this.buttonSaveURLSettings.Size = new System.Drawing.Size(264, 34);
            this.buttonSaveURLSettings.TabIndex = 56;
            this.buttonSaveURLSettings.Text = "Save Settings";
            this.buttonSaveURLSettings.UseVisualStyleBackColor = false;
            this.buttonSaveURLSettings.Click += new System.EventHandler(this.buttonSaveURLSettings_Click);
            // 
            // textBoxID2
            // 
            this.textBoxID2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID2.Location = new System.Drawing.Point(15, 161);
            this.textBoxID2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxID2.Name = "textBoxID2";
            this.textBoxID2.ReadOnly = true;
            this.textBoxID2.Size = new System.Drawing.Size(26, 26);
            this.textBoxID2.TabIndex = 58;
            this.textBoxID2.Visible = false;
            // 
            // textBoxID1
            // 
            this.textBoxID1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID1.Location = new System.Drawing.Point(15, 130);
            this.textBoxID1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxID1.Name = "textBoxID1";
            this.textBoxID1.ReadOnly = true;
            this.textBoxID1.Size = new System.Drawing.Size(26, 26);
            this.textBoxID1.TabIndex = 57;
            this.textBoxID1.Visible = false;
            // 
            // textBoxID3
            // 
            this.textBoxID3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID3.Location = new System.Drawing.Point(15, 191);
            this.textBoxID3.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxID3.Name = "textBoxID3";
            this.textBoxID3.ReadOnly = true;
            this.textBoxID3.Size = new System.Drawing.Size(26, 26);
            this.textBoxID3.TabIndex = 63;
            this.textBoxID3.Visible = false;
            // 
            // textBoxServiceName3
            // 
            this.textBoxServiceName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServiceName3.Location = new System.Drawing.Point(50, 191);
            this.textBoxServiceName3.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxServiceName3.Name = "textBoxServiceName3";
            this.textBoxServiceName3.ReadOnly = true;
            this.textBoxServiceName3.Size = new System.Drawing.Size(187, 26);
            this.textBoxServiceName3.TabIndex = 62;
            // 
            // buttonTest3
            // 
            this.buttonTest3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.buttonTest3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTest3.ForeColor = System.Drawing.Color.Transparent;
            this.buttonTest3.Location = new System.Drawing.Point(501, 191);
            this.buttonTest3.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonTest3.Name = "buttonTest3";
            this.buttonTest3.Size = new System.Drawing.Size(74, 27);
            this.buttonTest3.TabIndex = 61;
            this.buttonTest3.Text = "Test";
            this.buttonTest3.UseVisualStyleBackColor = false;
            this.buttonTest3.Click += new System.EventHandler(this.buttonTest3_Click);
            // 
            // textBoxPort3
            // 
            this.textBoxPort3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPort3.Location = new System.Drawing.Point(403, 191);
            this.textBoxPort3.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPort3.Name = "textBoxPort3";
            this.textBoxPort3.Size = new System.Drawing.Size(80, 26);
            this.textBoxPort3.TabIndex = 60;
            // 
            // textBoxIPAddress3
            // 
            this.textBoxIPAddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIPAddress3.Location = new System.Drawing.Point(241, 191);
            this.textBoxIPAddress3.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIPAddress3.Name = "textBoxIPAddress3";
            this.textBoxIPAddress3.Size = new System.Drawing.Size(158, 26);
            this.textBoxIPAddress3.TabIndex = 59;
            // 
            // textBoxID4
            // 
            this.textBoxID4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID4.Location = new System.Drawing.Point(15, 221);
            this.textBoxID4.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxID4.Name = "textBoxID4";
            this.textBoxID4.ReadOnly = true;
            this.textBoxID4.Size = new System.Drawing.Size(26, 26);
            this.textBoxID4.TabIndex = 68;
            this.textBoxID4.Visible = false;
            // 
            // textBoxServiceName4
            // 
            this.textBoxServiceName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServiceName4.Location = new System.Drawing.Point(50, 221);
            this.textBoxServiceName4.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxServiceName4.Name = "textBoxServiceName4";
            this.textBoxServiceName4.ReadOnly = true;
            this.textBoxServiceName4.Size = new System.Drawing.Size(187, 26);
            this.textBoxServiceName4.TabIndex = 67;
            // 
            // buttonTest4
            // 
            this.buttonTest4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.buttonTest4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTest4.ForeColor = System.Drawing.Color.Transparent;
            this.buttonTest4.Location = new System.Drawing.Point(501, 221);
            this.buttonTest4.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonTest4.Name = "buttonTest4";
            this.buttonTest4.Size = new System.Drawing.Size(74, 27);
            this.buttonTest4.TabIndex = 66;
            this.buttonTest4.Text = "Test";
            this.buttonTest4.UseVisualStyleBackColor = false;
            this.buttonTest4.Click += new System.EventHandler(this.buttonTest4_Click);
            // 
            // textBoxPort4
            // 
            this.textBoxPort4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPort4.Location = new System.Drawing.Point(403, 221);
            this.textBoxPort4.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPort4.Name = "textBoxPort4";
            this.textBoxPort4.Size = new System.Drawing.Size(80, 26);
            this.textBoxPort4.TabIndex = 65;
            // 
            // textBoxIPAddress4
            // 
            this.textBoxIPAddress4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIPAddress4.Location = new System.Drawing.Point(241, 221);
            this.textBoxIPAddress4.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIPAddress4.Name = "textBoxIPAddress4";
            this.textBoxIPAddress4.Size = new System.Drawing.Size(158, 26);
            this.textBoxIPAddress4.TabIndex = 64;
            // 
            // URLSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(635, 384);
            this.Controls.Add(this.textBoxID4);
            this.Controls.Add(this.textBoxServiceName4);
            this.Controls.Add(this.buttonTest4);
            this.Controls.Add(this.textBoxPort4);
            this.Controls.Add(this.textBoxIPAddress4);
            this.Controls.Add(this.textBoxID3);
            this.Controls.Add(this.textBoxServiceName3);
            this.Controls.Add(this.buttonTest3);
            this.Controls.Add(this.textBoxPort3);
            this.Controls.Add(this.textBoxIPAddress3);
            this.Controls.Add(this.textBoxID2);
            this.Controls.Add(this.textBoxID1);
            this.Controls.Add(this.buttonSaveURLSettings);
            this.Controls.Add(this.textBoxServiceName2);
            this.Controls.Add(this.textBoxServiceName1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.buttonTestPrinter);
            this.Controls.Add(this.buttonTest2);
            this.Controls.Add(this.textBoxPort2);
            this.Controls.Add(this.textBoxIPAddress2);
            this.Controls.Add(this.btnTest1);
            this.Controls.Add(this.textBoxPort1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxIPAddress1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "URLSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "URLSettings";
            this.Load += new System.EventHandler(this.URLSettings_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox textBoxIPAddress1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxPort1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTest1;
        private System.Windows.Forms.Button buttonTest2;
        public System.Windows.Forms.TextBox textBoxPort2;
        public System.Windows.Forms.TextBox textBoxIPAddress2;
        private System.Windows.Forms.Button buttonTestPrinter;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox textBoxServiceName2;
        public System.Windows.Forms.TextBox textBoxServiceName1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSaveURLSettings;
        public System.Windows.Forms.TextBox textBoxID2;
        public System.Windows.Forms.TextBox textBoxID1;
        public System.Windows.Forms.TextBox textBoxID3;
        public System.Windows.Forms.TextBox textBoxServiceName3;
        private System.Windows.Forms.Button buttonTest3;
        public System.Windows.Forms.TextBox textBoxPort3;
        public System.Windows.Forms.TextBox textBoxIPAddress3;
        public System.Windows.Forms.TextBox textBoxID4;
        public System.Windows.Forms.TextBox textBoxServiceName4;
        private System.Windows.Forms.Button buttonTest4;
        public System.Windows.Forms.TextBox textBoxPort4;
        public System.Windows.Forms.TextBox textBoxIPAddress4;
    }
}