﻿using BLL.Constants;
using BLL.HeartBeatManager;
using BLL.InitializeCardManager;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.ShiftManager;
using Microsoft.Reporting.WinForms;
using POS.Forms;
using System;
using System.ComponentModel;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace POS
{
    public partial class MainForm : Form
    {
        private bool btnInitializedCard = false;
        private bool btnIssueCards = false;
        private LogManager logManager;
        private BackgroundWorker backgroundHBeatWorker;
        private HeartBeatManager heartBeatManager;
        private int heartBeatTimer = 10000;
        private string softwareVersion;



        public MainForm()
        {
            InitializeComponent();
            this.KeyDown += MainForm_KeyDown;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(100, 100);
            logManager = new LogManager();
            heartBeatManager = new HeartBeatManager();

            softwareVersion = System.Windows.Forms.Application.ProductVersion;
            this.lblVersion.Text = String.Format("Version {0}", softwareVersion);
            this.backgroundHBeatWorker = new BackgroundWorker();
            this.backgroundHBeatWorker.WorkerSupportsCancellation = true;

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.backgroundHBeatWorker.DoWork += new DoWorkEventHandler(this.backgroundHBeatWorker_DoWork);
            backgroundHBeatWorker.RunWorkerAsync();
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                lblVersion.Text = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                softwareVersion = lblVersion.Text;
            }
            ShiftManager shiftManager = new ShiftManager(StaticClassUserData.UserID);

            if (shiftManager.ActiveShiftExists)
            {
                EnableButtons();
                btnShift.Text = "End Shift";
            }
            else
            {
                MessageBox.Show("Welcome! You have not started your shift yet. Please click on Start Shift button to start your shift.", "No Active Shift");
            }

            Application.ApplicationExit += Application_ApplicationExit;
            foreach (Control c in this.Controls)
            {
                if (c is MdiClient)
                {
                    MdiClient mdiClient;
                    mdiClient = (MdiClient)c;
                    mdiClient.BackgroundImage = Properties.Resources.AFC_login_bg;
                }
            }
        }

        private void backgroundHBeatWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                heartBeatManager.SendHeartBeat(softwareVersion);
                Thread.Sleep(heartBeatTimer);
            }
        }
 
        private void EnableButtons()
        {
            if (Login.IsCardIssued)
            {
                btnIssueCard.Enabled = true;
                pictureBoxIssueCard.Enabled = true;
            }
            if (Login.IsCardBlockEnable)
            {
                btnBlockCard.Enabled = true;
                pictureBoxCardBlock.Enabled = true;
            }
            if (Login.IsSingleJourneyTicketEnable)
            {
                btnSingleJourneyTicket.Enabled = true;
                pictureBoxSingleJourneyTicket.Enabled = true;
            }
            if (Login.IsCardTopup)
            {
                btnTopUp.Enabled = true;
                pictureBoxTopup.Enabled = true;
            }
            if (Login.IsRegenerateQR)
            {
                buttonRegenerateQR.Enabled = true;
                pictureBoxRegenerateQR.Enabled = true;
            }
            if (Login.IsQRScan)
            {
                btnScanQR.Enabled = true;
            }
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            //LogManager logManager = new LogManager();
            //LogModel logModel = new LogModel();
            //logModel.UserID = StaticClassUserData.UserID;
            //logModel.ActionPerform = "Application Exit";
            //logModel.LoginDate = DateTime.Now.ToString();
            //logModel.UserName = StaticClassUserData.Name;
            //logManager.SaveLog(logModel);
            backgroundHBeatWorker.CancelAsync();
            backgroundHBeatWorker.Dispose();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 1");
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            LogManager logManager = new LogManager();
            if (MessageBox.Show("Are you sure you want to Logout ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
               
                LogModel logModel = new LogModel();
                logModel.UserID = StaticClassUserData.UserID;
                logModel.ActionPerform = "Log Out";
                logModel.LoginDate = DateTime.Now.ToString();
                logModel.UserName = StaticClassUserData.Name;
                logModel.IP_Address = StaticClassSyncData.POS_IP;
                logManager.SaveLog(logModel);
                Login objmain = new Login();
                objmain.Show();
                Constants.clearStaticObject();
                this.Close();

                //Application.Exit();

            }

        }
        Forms.Configuration fmConguration = null;
        private void pictureBox14_Click(object sender, EventArgs e)
        {

            if (fmConguration == null || fmConguration.Text == "")
            {
                fmConguration = new Forms.Configuration();
                fmConguration.WindowState = FormWindowState.Normal;
                // fm.MdiParent = this;
                fmConguration.TopLevel = false;
                panel4.Controls.Add(fmConguration);
                fmConguration.Dock = DockStyle.Fill;
                fmConguration.Show();
                fmConguration.BringToFront();
            }
            else if (CheckOpened(fmConguration.Name))
            {
                fmConguration.WindowState = FormWindowState.Normal;
                //fm.MdiParent = this;
                fmConguration.TopLevel = false;
                panel4.Controls.Add(fmConguration);
                fmConguration.Dock = DockStyle.Fill;
                fmConguration.Show();
                fmConguration.BringToFront();
                //  fmsingleJourney.SendToBack();
            }
        }
        private void Menu_Click_1(object sender, EventArgs e)
        {
            Forms.Profile objForm = new Forms.Profile();
            objForm.Show();
        }
        Forms.SingleJourneyTicket fmsingleJourney = null;
        private void btnSingleJourneyTicket_Click(object sender, EventArgs e)
        {

            if (fmsingleJourney == null || fmsingleJourney.Text == "")
            {
                fmsingleJourney = new Forms.SingleJourneyTicket();
                fmsingleJourney.WindowState = FormWindowState.Normal;
                //fmsingleJourney.MdiParent = this;
                fmsingleJourney.TopLevel = false;
                panel4.Controls.Add(fmsingleJourney);
                fmsingleJourney.Dock = DockStyle.Fill;
                fmsingleJourney.Show();
                fmsingleJourney.BringToFront();
            }
            else if (CheckOpened(fmsingleJourney.Name))
            {
                fmsingleJourney.WindowState = FormWindowState.Normal;
                fmsingleJourney.TopLevel = false;
                panel4.Controls.Add(fmsingleJourney);
                fmsingleJourney.Dock = DockStyle.Fill;
                fmsingleJourney.Show();
                fmsingleJourney.BringToFront();
            }
        }
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Text == name)
                {
                    return true;
                }
            }
            return false;
        }
       
        private void pictureBox12_Click(object sender, EventArgs e)
        {
            btnSingleJourneyTicket_Click(null, null);
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            Forms.URLSettings objForm = new Forms.URLSettings();
            objForm.Show();
        }
        Forms.CardIssuance objForm = null;
        private void btnIssueCard_Click(object sender, EventArgs e)
        {
            try
            {
                btnIssueCards = true;
                btnInitializedCard = false;
                //btnIssueCard.BackColor = Color.LightGreen;
                if (objForm == null || objForm.Text == "")
                {
                    objForm = new Forms.CardIssuance();
                    objForm.WindowState = FormWindowState.Normal;
                    //fmsingleJourney.MdiParent = this;
                    objForm.TopLevel = false;
                    panel4.Controls.Add(objForm);
                    objForm.Dock = DockStyle.Fill;
                    objForm.Show();
                    objForm.BringToFront();
                }
                else if (CheckOpened(objForm.Name))
                {
                    objForm.WindowState = FormWindowState.Normal;
                    objForm.TopLevel = false;
                    panel4.Controls.Add(objForm);
                    objForm.Dock = DockStyle.Fill;
                    objForm.Show();
                    objForm.BringToFront();

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        Forms.RegenerateQR objFormRegenerateQR = null;
        private void buttonRegenerateQR_Click(object sender, EventArgs e)
        {

            if (objFormRegenerateQR == null || objFormRegenerateQR.Text == "")
            {
                objFormRegenerateQR = new Forms.RegenerateQR();
                objFormRegenerateQR.WindowState = FormWindowState.Normal;
                //fmsingleJourney.MdiParent = this;
                objFormRegenerateQR.TopLevel = false;
                panel4.Controls.Add(objFormRegenerateQR);
                objFormRegenerateQR.Dock = DockStyle.Fill;
                objFormRegenerateQR.Show();
                objFormRegenerateQR.BringToFront();
            }
            else if (CheckOpened(objFormRegenerateQR.Name))
            {
                objFormRegenerateQR.WindowState = FormWindowState.Normal;
                objFormRegenerateQR.TopLevel = false;
                panel4.Controls.Add(objFormRegenerateQR);
                objFormRegenerateQR.Dock = DockStyle.Fill;
                objFormRegenerateQR.Show();
                objFormRegenerateQR.BringToFront();

            }
        }

        private void pictureBoxIssueCard_Click(object sender, EventArgs e)
        {
            try
            {
                btnIssueCard_Click(null, null);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            buttonRegenerateQR_Click(null, null);
        }
        Forms.CardBlock objFormCardBlock = null;
        private void btnBlockCard_Click(object sender, EventArgs e)
        {
            if (objFormCardBlock == null || objFormCardBlock.Text == "")
            {
                objFormCardBlock = new Forms.CardBlock();
                objFormCardBlock.WindowState = FormWindowState.Normal;
                //fmsingleJourney.MdiParent = this;
                objFormCardBlock.TopLevel = false;
                panel4.Controls.Add(objFormCardBlock);
                objFormCardBlock.Dock = DockStyle.Fill;
                objFormCardBlock.Show();
                objFormCardBlock.BringToFront();
            }
            else if (CheckOpened(objFormCardBlock.Name))
            {
                objFormCardBlock.WindowState = FormWindowState.Normal;
                objFormCardBlock.TopLevel = false;
                panel4.Controls.Add(objFormCardBlock);
                objFormCardBlock.Dock = DockStyle.Fill;
                objFormCardBlock.Show();
                objFormCardBlock.BringToFront();

            }
        }
        Forms.CardInitialization objFormInitializeCard = null;
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.O && Login.IsCardInitialized)
            {
                //btnInitializedCard = true;
                //btnIssueCards = false;
                if (objFormInitializeCard == null || objFormInitializeCard.Text == "")
                {
                    objFormInitializeCard = new Forms.CardInitialization();
                    objFormInitializeCard.WindowState = FormWindowState.Normal;
                    //fmsingleJourney.MdiParent = this;
                    objFormInitializeCard.TopLevel = false;
                    panel4.Controls.Add(objFormRegenerateQR);
                    objFormInitializeCard.Dock = DockStyle.Fill;
                    objFormInitializeCard.Show();
                    //objFormInitializeCard.Activate();
                    //objFormInitializeCard.BringToFront();
                }
                else if (CheckOpened(objFormInitializeCard.Name))
                {
                    objFormInitializeCard.WindowState = FormWindowState.Normal;
                    objFormInitializeCard.TopLevel = false;
                    panel4.Controls.Add(objFormInitializeCard);
                    objFormInitializeCard.Dock = DockStyle.Fill;
                    objFormInitializeCard.Show();
                    objFormInitializeCard.BringToFront();

                }
            }
        }

        InitializeCardManager InitializeCard = new InitializeCardManager();
        private void btnReadCard_Click(object sender, EventArgs e)
        {
            bool result = btnIssueCards;
            bool res = btnInitializedCard;
            if (btnInitializedCard)
            {

                
            }

        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {


        }

        Forms.CardTopup objtopup = null;
        private void btnTopUp_Click(object sender, EventArgs e)
        {
            if (objtopup == null || objtopup.Text == "")
            {
                objtopup = new Forms.CardTopup();
                objtopup.WindowState = FormWindowState.Normal;
                //fmsingleJourney.MdiParent = this;
                objtopup.TopLevel = false;
                panel4.Controls.Add(objtopup);
                objtopup.Dock = DockStyle.Fill;
                objtopup.Show();
                objtopup.BringToFront();
            }
            else if (CheckOpened(objtopup.Name))
            {
                objtopup.WindowState = FormWindowState.Normal;
                objtopup.TopLevel = false;
                panel4.Controls.Add(objtopup);
                objtopup.Dock = DockStyle.Fill;
                objtopup.Show();
                objtopup.BringToFront();

            }
        }

        private void pictureBoxTopup_Click(object sender, EventArgs e)
        {
            btnTopUp_Click(null,null);
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBoxUserProfile_Click(object sender, EventArgs e)
        {
            Forms.Profile objForm = new Forms.Profile();
            objForm.Show();
        }

        Forms.QRScanForm scanForm = null;
        private void btnScanQR_Click(object sender, EventArgs e)
        {
            if (scanForm == null || scanForm.Text == "")
            {
                scanForm = new Forms.QRScanForm();
                scanForm.WindowState = FormWindowState.Normal;
                //fmsingleJourney.MdiParent = this;
                scanForm.TopLevel = false;
                panel4.Controls.Add(scanForm);
                scanForm.Dock = DockStyle.Fill;
                scanForm.Show();
                scanForm.BringToFront();
            }
            else if (CheckOpened(scanForm.Name))
            {
                scanForm.WindowState = FormWindowState.Normal;
                scanForm.TopLevel = false;
                panel4.Controls.Add(scanForm);
                scanForm.Dock = DockStyle.Fill;
                scanForm.Show();
                scanForm.BringToFront();
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            try
            {
                ShiftManager shiftManager = new ShiftManager(StaticClassUserData.UserID);


                if (shiftManager.ActiveShiftExists) //If active shift exists end the shift, get cash in hand value from user,generate reciept,close active forms and disable action buttons
                {
                    if (MessageBox.Show("This will end your current Shift. Are you sure you want to proceed?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (var cInHandDialog = new CashInHandDialog())
                        {
                            var result = cInHandDialog.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                shiftManager.ActiveShift.CashInHand = cInHandDialog.CashInHand;
                                if (shiftManager.EndShift())
                                {
                                    btnShift.Text = "Start Shift";
                                    LogModel logModel = new LogModel();
                                    logModel.UserID = StaticClassUserData.UserID;
                                    logModel.ActionPerform = "Shift Ended";
                                    logModel.LoginDate = DateTime.Now.ToString();
                                    logModel.UserName = StaticClassUserData.Name;
                                    logModel.IP_Address = StaticClassSyncData.POS_IP;
                                    logManager.SaveLog(logModel);

                                    DisableButtons();
                                    foreach (Form form in panel4.Controls.OfType<Form>().ToArray())
                                        form.Close();

                                    GenerateReceipt(shiftManager);

                                }
                                else
                                    MessageBox.Show("Shift could not be ended properly. Please contact Admin.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    if (shiftManager.StartShift(StaticClassUserData.UserID))
                    {
                        btnShift.Text = "End Shift";
                        EnableButtons();
                        MessageBox.Show("Shift has been started at: " + shiftManager.ActiveShift.ShiftStartTime, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = "Shift Started";
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);

                    }
                    else
                        MessageBox.Show("Shift could not be started properly. Please contact Admin.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void GenerateReceipt(ShiftManager shiftManager)
        {
            var receipt = shiftManager.GenerateLastReceipt();
            LocalReport report = new LocalReport();
            if (receipt.isSuccess)
            {
                string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\ShiftReceipt.rdlc";
                report.ReportPath = fullPath;

                this.receiptViewer.LocalReport.ReportPath = fullPath;
                this.receiptViewer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("POSUserName", receipt.responseObject.userName));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("POSName", StaticClassSyncData.POS_Name));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("StationName", StaticClassSyncData.Station_Name));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("ShiftStartTime", receipt.responseObject.ShiftStartTime));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("ShiftEndTime", receipt.responseObject.ShiftEndTime));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("QRSold", string.IsNullOrEmpty(receipt.responseObject.totalQrSales) ? "0" : receipt.responseObject.totalQrSales));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("TotalAmount", string.IsNullOrEmpty(receipt.responseObject.totalRevenue) ? "0" : receipt.responseObject.totalRevenue));
                this.receiptViewer.LocalReport.SetParameters(new ReportParameter("TotalCashInHand", string.IsNullOrEmpty(receipt.responseObject.ShiftAmount) ? "0" : receipt.responseObject.ShiftAmount));


                this.receiptViewer.LocalReport.Print("Receipt");
                // Log Saving in tblLog in Sqlite Database
                LogModel logModel = new LogModel();
                logModel.UserID = StaticClassUserData.UserID;
                logModel.ActionPerform = "Receipt Printed";
                logModel.LoginDate = DateTime.Now.ToString();
                logModel.UserName = StaticClassUserData.Name;
                logModel.IP_Address = StaticClassSyncData.POS_IP;
                logManager.SaveLog(logModel);
                MessageBox.Show("Receipt printed successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                LogModel logModel = new LogModel();
                logModel.UserID = StaticClassUserData.UserID;
                logModel.ActionPerform = receipt.message + receipt.responseObject.userName;
                logModel.LoginDate = DateTime.Now.ToString();
                logModel.UserName = StaticClassUserData.Name;
                logModel.IP_Address = StaticClassSyncData.POS_IP;

                logManager.SaveLog(logModel);
                MessageBox.Show(receipt.message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void DisableButtons()
        {
            btnIssueCard.Enabled = false;
            pictureBoxIssueCard.Enabled = false;
            btnBlockCard.Enabled = false;
            pictureBoxCardBlock.Enabled = false;
            btnSingleJourneyTicket.Enabled = false;
            pictureBoxSingleJourneyTicket.Enabled = false;
            btnTopUp.Enabled = false;
            pictureBoxTopup.Enabled = false;
            buttonRegenerateQR.Enabled = false;
            pictureBoxRegenerateQR.Enabled = false;
        }
    }
}
