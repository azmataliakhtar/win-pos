﻿
namespace POS.Forms
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuration));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxOrganizationName = new System.Windows.Forms.TextBox();
            this.textBoxStationName = new System.Windows.Forms.TextBox();
            this.textBoxStationCode = new System.Windows.Forms.TextBox();
            this.textBoxPublicKeyVersion = new System.Windows.Forms.TextBox();
            this.textBoxMACAddress = new System.Windows.Forms.TextBox();
            this.textBoxPOSIP = new System.Windows.Forms.TextBox();
            this.textBoxPSAMNumber = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSyncData = new System.Windows.Forms.Button();
            this.textBoxMobile = new System.Windows.Forms.TextBox();
            this.textBoxFax = new System.Windows.Forms.TextBox();
            this.textBoxSkype = new System.Windows.Forms.TextBox();
            this.textBoxPOSCode = new System.Windows.Forms.TextBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.textBoxOrganizationAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxLastSyncDateTime = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxtextBoxPOSMinTopup = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPOSMaxTopup = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Organization Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 403);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Public Key Version";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 357);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Station Code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 311);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Station Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "MAC Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "POS IP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(144, 29);
            this.label7.TabIndex = 6;
            this.label7.Text = "PSAM Number";
            // 
            // textBoxOrganizationName
            // 
            this.textBoxOrganizationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOrganizationName.Location = new System.Drawing.Point(209, 127);
            this.textBoxOrganizationName.Name = "textBoxOrganizationName";
            this.textBoxOrganizationName.ReadOnly = true;
            this.textBoxOrganizationName.Size = new System.Drawing.Size(264, 35);
            this.textBoxOrganizationName.TabIndex = 7;
            // 
            // textBoxStationName
            // 
            this.textBoxStationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStationName.Location = new System.Drawing.Point(209, 311);
            this.textBoxStationName.Name = "textBoxStationName";
            this.textBoxStationName.ReadOnly = true;
            this.textBoxStationName.Size = new System.Drawing.Size(264, 35);
            this.textBoxStationName.TabIndex = 9;
            // 
            // textBoxStationCode
            // 
            this.textBoxStationCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStationCode.Location = new System.Drawing.Point(209, 357);
            this.textBoxStationCode.Name = "textBoxStationCode";
            this.textBoxStationCode.ReadOnly = true;
            this.textBoxStationCode.Size = new System.Drawing.Size(264, 35);
            this.textBoxStationCode.TabIndex = 10;
            // 
            // textBoxPublicKeyVersion
            // 
            this.textBoxPublicKeyVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPublicKeyVersion.Location = new System.Drawing.Point(209, 403);
            this.textBoxPublicKeyVersion.Name = "textBoxPublicKeyVersion";
            this.textBoxPublicKeyVersion.ReadOnly = true;
            this.textBoxPublicKeyVersion.Size = new System.Drawing.Size(264, 35);
            this.textBoxPublicKeyVersion.TabIndex = 11;
            // 
            // textBoxMACAddress
            // 
            this.textBoxMACAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMACAddress.Location = new System.Drawing.Point(209, 265);
            this.textBoxMACAddress.Name = "textBoxMACAddress";
            this.textBoxMACAddress.ReadOnly = true;
            this.textBoxMACAddress.Size = new System.Drawing.Size(264, 35);
            this.textBoxMACAddress.TabIndex = 12;
            // 
            // textBoxPOSIP
            // 
            this.textBoxPOSIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOSIP.Location = new System.Drawing.Point(209, 219);
            this.textBoxPOSIP.Name = "textBoxPOSIP";
            this.textBoxPOSIP.ReadOnly = true;
            this.textBoxPOSIP.Size = new System.Drawing.Size(264, 35);
            this.textBoxPOSIP.TabIndex = 13;
            // 
            // textBoxPSAMNumber
            // 
            this.textBoxPSAMNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSAMNumber.Location = new System.Drawing.Point(209, 173);
            this.textBoxPSAMNumber.Name = "textBoxPSAMNumber";
            this.textBoxPSAMNumber.ReadOnly = true;
            this.textBoxPSAMNumber.Size = new System.Drawing.Size(264, 35);
            this.textBoxPSAMNumber.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 100);
            this.panel1.TabIndex = 15;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(955, 11);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 26);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(397, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 33);
            this.label8.TabIndex = 0;
            this.label8.Text = "Configuration";
            // 
            // btnSyncData
            // 
            this.btnSyncData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.btnSyncData.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.btnSyncData.ForeColor = System.Drawing.Color.Transparent;
            this.btnSyncData.Location = new System.Drawing.Point(417, 582);
            this.btnSyncData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSyncData.Name = "btnSyncData";
            this.btnSyncData.Size = new System.Drawing.Size(259, 54);
            this.btnSyncData.TabIndex = 36;
            this.btnSyncData.Text = "Sync Information";
            this.btnSyncData.UseVisualStyleBackColor = false;
            this.btnSyncData.Click += new System.EventHandler(this.btnSyncData_Click);
            // 
            // textBoxMobile
            // 
            this.textBoxMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMobile.Location = new System.Drawing.Point(690, 173);
            this.textBoxMobile.Name = "textBoxMobile";
            this.textBoxMobile.ReadOnly = true;
            this.textBoxMobile.Size = new System.Drawing.Size(261, 35);
            this.textBoxMobile.TabIndex = 50;
            // 
            // textBoxFax
            // 
            this.textBoxFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFax.Location = new System.Drawing.Point(690, 219);
            this.textBoxFax.Name = "textBoxFax";
            this.textBoxFax.ReadOnly = true;
            this.textBoxFax.Size = new System.Drawing.Size(261, 35);
            this.textBoxFax.TabIndex = 49;
            // 
            // textBoxSkype
            // 
            this.textBoxSkype.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSkype.Location = new System.Drawing.Point(690, 265);
            this.textBoxSkype.Name = "textBoxSkype";
            this.textBoxSkype.ReadOnly = true;
            this.textBoxSkype.Size = new System.Drawing.Size(261, 35);
            this.textBoxSkype.TabIndex = 48;
            // 
            // textBoxPOSCode
            // 
            this.textBoxPOSCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOSCode.Location = new System.Drawing.Point(690, 357);
            this.textBoxPOSCode.Name = "textBoxPOSCode";
            this.textBoxPOSCode.ReadOnly = true;
            this.textBoxPOSCode.Size = new System.Drawing.Size(261, 35);
            this.textBoxPOSCode.TabIndex = 46;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDescription.Location = new System.Drawing.Point(690, 311);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.Size = new System.Drawing.Size(261, 35);
            this.textBoxDescription.TabIndex = 45;
            // 
            // textBoxOrganizationAddress
            // 
            this.textBoxOrganizationAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOrganizationAddress.Location = new System.Drawing.Point(690, 127);
            this.textBoxOrganizationAddress.Name = "textBoxOrganizationAddress";
            this.textBoxOrganizationAddress.ReadOnly = true;
            this.textBoxOrganizationAddress.Size = new System.Drawing.Size(261, 35);
            this.textBoxOrganizationAddress.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(492, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 29);
            this.label9.TabIndex = 43;
            this.label9.Text = "Mobile";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(492, 219);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 29);
            this.label10.TabIndex = 42;
            this.label10.Text = "Fax";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(492, 265);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 29);
            this.label11.TabIndex = 41;
            this.label11.Text = "Skype";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(492, 311);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 29);
            this.label12.TabIndex = 40;
            this.label12.Text = "Description";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(492, 357);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 29);
            this.label13.TabIndex = 39;
            this.label13.Text = "POS Code";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(492, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(171, 29);
            this.label15.TabIndex = 37;
            this.label15.Text = "Organization Addr";
            // 
            // textBoxLastSyncDateTime
            // 
            this.textBoxLastSyncDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLastSyncDateTime.Location = new System.Drawing.Point(690, 403);
            this.textBoxLastSyncDateTime.Name = "textBoxLastSyncDateTime";
            this.textBoxLastSyncDateTime.ReadOnly = true;
            this.textBoxLastSyncDateTime.Size = new System.Drawing.Size(261, 35);
            this.textBoxLastSyncDateTime.TabIndex = 52;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(500, 403);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 29);
            this.label16.TabIndex = 51;
            this.label16.Text = "Last Sync Time";
            // 
            // textBoxID
            // 
            this.textBoxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID.Location = new System.Drawing.Point(35, 572);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(10, 35);
            this.textBoxID.TabIndex = 53;
            this.textBoxID.Visible = false;
            // 
            // textBoxtextBoxPOSMinTopup
            // 
            this.textBoxtextBoxPOSMinTopup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxtextBoxPOSMinTopup.Location = new System.Drawing.Point(690, 447);
            this.textBoxtextBoxPOSMinTopup.Name = "textBoxtextBoxPOSMinTopup";
            this.textBoxtextBoxPOSMinTopup.ReadOnly = true;
            this.textBoxtextBoxPOSMinTopup.Size = new System.Drawing.Size(261, 35);
            this.textBoxtextBoxPOSMinTopup.TabIndex = 57;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(500, 447);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 29);
            this.label14.TabIndex = 56;
            this.label14.Text = "POS Min Topup";
            // 
            // textBoxPOSMaxTopup
            // 
            this.textBoxPOSMaxTopup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPOSMaxTopup.Location = new System.Drawing.Point(209, 447);
            this.textBoxPOSMaxTopup.Name = "textBoxPOSMaxTopup";
            this.textBoxPOSMaxTopup.ReadOnly = true;
            this.textBoxPOSMaxTopup.Size = new System.Drawing.Size(264, 35);
            this.textBoxPOSMaxTopup.TabIndex = 55;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 447);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(158, 29);
            this.label17.TabIndex = 54;
            this.label17.Text = "POS Max Topup";
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(997, 703);
            this.Controls.Add(this.textBoxtextBoxPOSMinTopup);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxPOSMaxTopup);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.textBoxLastSyncDateTime);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBoxMobile);
            this.Controls.Add(this.textBoxFax);
            this.Controls.Add(this.textBoxSkype);
            this.Controls.Add(this.textBoxPOSCode);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.textBoxOrganizationAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnSyncData);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxPSAMNumber);
            this.Controls.Add(this.textBoxPOSIP);
            this.Controls.Add(this.textBoxMACAddress);
            this.Controls.Add(this.textBoxPublicKeyVersion);
            this.Controls.Add(this.textBoxStationCode);
            this.Controls.Add(this.textBoxStationName);
            this.Controls.Add(this.textBoxOrganizationName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Configuration";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSyncData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox textBoxOrganizationName;
        public System.Windows.Forms.TextBox textBoxStationName;
        public System.Windows.Forms.TextBox textBoxStationCode;
        public System.Windows.Forms.TextBox textBoxPublicKeyVersion;
        public System.Windows.Forms.TextBox textBoxMACAddress;
        public System.Windows.Forms.TextBox textBoxPOSIP;
        public System.Windows.Forms.TextBox textBoxPSAMNumber;
        public System.Windows.Forms.TextBox textBoxMobile;
        public System.Windows.Forms.TextBox textBoxFax;
        public System.Windows.Forms.TextBox textBoxSkype;
        public System.Windows.Forms.TextBox textBoxPOSCode;
        public System.Windows.Forms.TextBox textBoxDescription;
        public System.Windows.Forms.TextBox textBoxOrganizationAddress;
        public System.Windows.Forms.TextBox textBoxLastSyncDateTime;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox textBoxID;
        public System.Windows.Forms.TextBox textBoxtextBoxPOSMinTopup;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox textBoxPOSMaxTopup;
        private System.Windows.Forms.Label label17;
    }
}