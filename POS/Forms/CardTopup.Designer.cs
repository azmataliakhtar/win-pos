﻿
namespace POS.Forms
{
    partial class CardTopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardTopup));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelMainScreen = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxTopupAmount = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxPaymentMethod = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.groupBoxCardHolderInfo = new System.Windows.Forms.GroupBox();
            this.pictureBoxCardtopup = new System.Windows.Forms.PictureBox();
            this.textBoxGender = new System.Windows.Forms.ComboBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxCNIC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBoxCardInfo = new System.Windows.Forms.GroupBox();
            this.textBoxCurrentBalance = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxexpiry = new System.Windows.Forms.TextBox();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxCardType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonTopup = new System.Windows.Forms.Button();
            this.btnReadCard = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMainScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxTopupAmount)).BeginInit();
            this.groupBoxCardHolderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardtopup)).BeginInit();
            this.groupBoxCardInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(0, 86);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(886, 71);
            this.panel1.TabIndex = 39;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(849, 9);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 21);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(353, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 29);
            this.label8.TabIndex = 0;
            this.label8.Text = "Card Topup";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(141, 169);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(9, 8);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // panelMainScreen
            // 
            this.panelMainScreen.Controls.Add(this.pictureBox3);
            this.panelMainScreen.Controls.Add(this.groupBox1);
            this.panelMainScreen.Controls.Add(this.reportViewer2);
            this.panelMainScreen.Controls.Add(this.groupBoxCardHolderInfo);
            this.panelMainScreen.Controls.Add(this.groupBoxCardInfo);
            this.panelMainScreen.Controls.Add(this.buttonTopup);
            this.panelMainScreen.Location = new System.Drawing.Point(11, 174);
            this.panelMainScreen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelMainScreen.Name = "panelMainScreen";
            this.panelMainScreen.Size = new System.Drawing.Size(875, 484);
            this.panelMainScreen.TabIndex = 42;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(204, 402);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(388, 80);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 56;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxTopupAmount);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBoxPaymentMethod);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(17, 316);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(833, 86);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Topup Information";
            // 
            // textBoxTopupAmount
            // 
            this.textBoxTopupAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTopupAmount.Location = new System.Drawing.Point(208, 33);
            this.textBoxTopupAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxTopupAmount.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.textBoxTopupAmount.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.textBoxTopupAmount.Name = "textBoxTopupAmount";
            this.textBoxTopupAmount.Size = new System.Drawing.Size(155, 30);
            this.textBoxTopupAmount.TabIndex = 59;
            this.textBoxTopupAmount.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 29);
            this.label4.TabIndex = 57;
            this.label4.Text = "Topup Amount";
            // 
            // comboBoxPaymentMethod
            // 
            this.comboBoxPaymentMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPaymentMethod.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPaymentMethod.FormattingEnabled = true;
            this.comboBoxPaymentMethod.Items.AddRange(new object[] {
            "Cash",
            "Easy Paisa"});
            this.comboBoxPaymentMethod.Location = new System.Drawing.Point(586, 22);
            this.comboBoxPaymentMethod.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxPaymentMethod.Name = "comboBoxPaymentMethod";
            this.comboBoxPaymentMethod.Size = new System.Drawing.Size(243, 35);
            this.comboBoxPaymentMethod.TabIndex = 56;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(389, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(177, 29);
            this.label14.TabIndex = 55;
            this.label14.Text = "Payment Method";
            // 
            // reportViewer2
            // 
            this.reportViewer2.DocumentMapWidth = 59;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = null;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "POS.Reports.QRPrint1.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(792, 449);
            this.reportViewer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ServerReport.BearerToken = null;
            this.reportViewer2.Size = new System.Drawing.Size(61, 32);
            this.reportViewer2.TabIndex = 61;
            this.reportViewer2.Visible = false;
            // 
            // groupBoxCardHolderInfo
            // 
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxGender);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxAddress);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxCNIC);
            this.groupBoxCardHolderInfo.Controls.Add(this.label7);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxPhone);
            this.groupBoxCardHolderInfo.Controls.Add(this.pictureBox1);
            this.groupBoxCardHolderInfo.Controls.Add(this.label9);
            this.groupBoxCardHolderInfo.Controls.Add(this.label12);
            this.groupBoxCardHolderInfo.Controls.Add(this.label10);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxName);
            this.groupBoxCardHolderInfo.Controls.Add(this.label11);
            this.groupBoxCardHolderInfo.Location = new System.Drawing.Point(17, 130);
            this.groupBoxCardHolderInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardHolderInfo.Name = "groupBoxCardHolderInfo";
            this.groupBoxCardHolderInfo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardHolderInfo.Size = new System.Drawing.Size(836, 169);
            this.groupBoxCardHolderInfo.TabIndex = 60;
            this.groupBoxCardHolderInfo.TabStop = false;
            this.groupBoxCardHolderInfo.Text = "Card Holder Information";
            this.groupBoxCardHolderInfo.Visible = false;
            // 
            // pictureBoxCardtopup
            // 
            this.pictureBoxCardtopup.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCardtopup.Image")));
            this.pictureBoxCardtopup.Location = new System.Drawing.Point(210, 239);
            this.pictureBoxCardtopup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxCardtopup.Name = "pictureBoxCardtopup";
            this.pictureBoxCardtopup.Size = new System.Drawing.Size(442, 333);
            this.pictureBoxCardtopup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCardtopup.TabIndex = 45;
            this.pictureBoxCardtopup.TabStop = false;
            // 
            // textBoxGender
            // 
            this.textBoxGender.Font = new System.Drawing.Font("Work Sans", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGender.FormattingEnabled = true;
            this.textBoxGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.textBoxGender.Location = new System.Drawing.Point(604, 54);
            this.textBoxGender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxGender.Name = "textBoxGender";
            this.textBoxGender.Size = new System.Drawing.Size(195, 34);
            this.textBoxGender.TabIndex = 54;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Enabled = false;
            this.textBoxAddress.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddress.Location = new System.Drawing.Point(235, 94);
            this.textBoxAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(463, 65);
            this.textBoxAddress.TabIndex = 4;
            // 
            // textBoxCNIC
            // 
            this.textBoxCNIC.Enabled = false;
            this.textBoxCNIC.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCNIC.Location = new System.Drawing.Point(604, 20);
            this.textBoxCNIC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCNIC.Name = "textBoxCNIC";
            this.textBoxCNIC.Size = new System.Drawing.Size(195, 31);
            this.textBoxCNIC.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(444, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 29);
            this.label7.TabIndex = 53;
            this.label7.Text = "Gender";
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Enabled = false;
            this.textBoxPhone.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhone.Location = new System.Drawing.Point(180, 55);
            this.textBoxPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(180, 31);
            this.textBoxPhone.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(444, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 29);
            this.label9.TabIndex = 53;
            this.label9.Text = "CNIC";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 29);
            this.label12.TabIndex = 51;
            this.label12.Text = "Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 29);
            this.label10.TabIndex = 51;
            this.label10.Text = "Phone";
            // 
            // textBoxName
            // 
            this.textBoxName.Enabled = false;
            this.textBoxName.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(180, 20);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(180, 31);
            this.textBoxName.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 29);
            this.label11.TabIndex = 51;
            this.label11.Text = "Name";
            // 
            // groupBoxCardInfo
            // 
            this.groupBoxCardInfo.Controls.Add(this.textBoxCurrentBalance);
            this.groupBoxCardInfo.Controls.Add(this.label6);
            this.groupBoxCardInfo.Controls.Add(this.textBoxexpiry);
            this.groupBoxCardInfo.Controls.Add(this.textBoxCardNumber);
            this.groupBoxCardInfo.Controls.Add(this.label2);
            this.groupBoxCardInfo.Controls.Add(this.label5);
            this.groupBoxCardInfo.Controls.Add(this.comboBoxCardType);
            this.groupBoxCardInfo.Controls.Add(this.label1);
            this.groupBoxCardInfo.Location = new System.Drawing.Point(14, 5);
            this.groupBoxCardInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardInfo.Name = "groupBoxCardInfo";
            this.groupBoxCardInfo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardInfo.Size = new System.Drawing.Size(836, 115);
            this.groupBoxCardInfo.TabIndex = 59;
            this.groupBoxCardInfo.TabStop = false;
            this.groupBoxCardInfo.Text = "Card Info";
            // 
            // textBoxCurrentBalance
            // 
            this.textBoxCurrentBalance.Enabled = false;
            this.textBoxCurrentBalance.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCurrentBalance.Location = new System.Drawing.Point(604, 69);
            this.textBoxCurrentBalance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCurrentBalance.Name = "textBoxCurrentBalance";
            this.textBoxCurrentBalance.Size = new System.Drawing.Size(195, 31);
            this.textBoxCurrentBalance.TabIndex = 54;
            this.textBoxCurrentBalance.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxMinTopup_Validating_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(412, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 29);
            this.label6.TabIndex = 53;
            this.label6.Text = "Current Balance";
            // 
            // textBoxexpiry
            // 
            this.textBoxexpiry.Enabled = false;
            this.textBoxexpiry.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxexpiry.Location = new System.Drawing.Point(173, 69);
            this.textBoxexpiry.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxexpiry.Name = "textBoxexpiry";
            this.textBoxexpiry.Size = new System.Drawing.Size(180, 31);
            this.textBoxexpiry.TabIndex = 52;
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Enabled = false;
            this.textBoxCardNumber.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCardNumber.Location = new System.Drawing.Point(175, 20);
            this.textBoxCardNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(237, 31);
            this.textBoxCardNumber.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 29);
            this.label2.TabIndex = 57;
            this.label2.Text = "Card Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 29);
            this.label5.TabIndex = 51;
            this.label5.Text = "Card Expiry";
            // 
            // comboBoxCardType
            // 
            this.comboBoxCardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCardType.Enabled = false;
            this.comboBoxCardType.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCardType.FormattingEnabled = true;
            this.comboBoxCardType.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxCardType.Location = new System.Drawing.Point(570, 16);
            this.comboBoxCardType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxCardType.Name = "comboBoxCardType";
            this.comboBoxCardType.Size = new System.Drawing.Size(243, 35);
            this.comboBoxCardType.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(447, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 29);
            this.label1.TabIndex = 55;
            this.label1.Text = "Card Type";
            // 
            // buttonTopup
            // 
            this.buttonTopup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonTopup.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTopup.ForeColor = System.Drawing.Color.Transparent;
            this.buttonTopup.Location = new System.Drawing.Point(204, 426);
            this.buttonTopup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonTopup.Name = "buttonTopup";
            this.buttonTopup.Size = new System.Drawing.Size(384, 53);
            this.buttonTopup.TabIndex = 54;
            this.buttonTopup.Text = "Top Up";
            this.buttonTopup.UseVisualStyleBackColor = false;
            this.buttonTopup.Click += new System.EventHandler(this.buttonTopup_Click_1);
            // 
            // btnReadCard
            // 
            this.btnReadCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.btnReadCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReadCard.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadCard.ForeColor = System.Drawing.Color.Transparent;
            this.btnReadCard.Location = new System.Drawing.Point(449, 12);
            this.btnReadCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReadCard.Name = "btnReadCard";
            this.btnReadCard.Padding = new System.Windows.Forms.Padding(89, 2, 2, 2);
            this.btnReadCard.Size = new System.Drawing.Size(427, 63);
            this.btnReadCard.TabIndex = 53;
            this.btnReadCard.Text = "Read Card";
            this.btnReadCard.UseVisualStyleBackColor = false;
            this.btnReadCard.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Transparent;
            this.btnRefresh.Location = new System.Drawing.Point(3, 12);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Padding = new System.Windows.Forms.Padding(89, 2, 2, 2);
            this.btnRefresh.Size = new System.Drawing.Size(438, 63);
            this.btnRefresh.TabIndex = 52;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.pictureBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(162, 25);
            this.pictureBox18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(57, 39);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 54;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(595, 25);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(57, 39);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 55;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // CardTopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(886, 678);
            this.Controls.Add(this.pictureBoxCardtopup);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.btnReadCard);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelMainScreen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CardTopup";
            this.Text = "CardIssuance";
            this.Load += new System.EventHandler(this.CardIssuance_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMainScreen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxTopupAmount)).EndInit();
            this.groupBoxCardHolderInfo.ResumeLayout(false);
            this.groupBoxCardHolderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardtopup)).EndInit();
            this.groupBoxCardInfo.ResumeLayout(false);
            this.groupBoxCardInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelMainScreen;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.GroupBox groupBoxCardHolderInfo;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxCNIC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBoxCardInfo;
        private System.Windows.Forms.TextBox textBoxCurrentBalance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxexpiry;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCardNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCardType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonTopup;
        private System.Windows.Forms.PictureBox pictureBoxCardtopup;
        private System.Windows.Forms.Button btnReadCard;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.ComboBox textBoxGender;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxPaymentMethod;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown textBoxTopupAmount;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}