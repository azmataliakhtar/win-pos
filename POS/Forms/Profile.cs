﻿using BLL.Model;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class Profile : Form
    {
        public Profile()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            textBoxName.Text = StaticClassUserData.Name;
            textBoxUserName.Text = StaticClassUserData.Name;
            textBoxDepartment.Text = StaticClassUserData.DepartmentName;
            textBoxEmail.Text = StaticClassUserData.Email;
            textBoxPhone.Text = "";
            textBoxOrganization.Text = StaticClassUserData.OrganizationName;
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            ChangePassword changePassword = new ChangePassword();
            changePassword.ShowDialog();
        }
    }
}
