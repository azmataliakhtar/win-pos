﻿using BLL.ConfiguratonManager;
using BLL.Constants;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.Models.ViewModel;
using BLL.UserManager;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BLL.Models.SynchronizeModel;

namespace POS
{
    public partial class Login : Form
    {

        public Login()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        public static bool IsCardBlockEnable = false;
        public static bool IsSingleJourneyTicketEnable = false;
        public static bool IsCardIssued = false;
        public static bool IsCardTopup = false;
        public static bool IsCardInitialized = false;
        public static bool IsQRScan = false;

        public static bool IsRegenerateQR = false;

        public static int UserID { get; set; }
        private void Login_Load(object sender, EventArgs e)
        {
            if(!SynchronizewithServer())
            {
                Application.Exit();
            }
            picBoxLoading.Visible = false;
            panel1.Location = new Point(
                this.ClientSize.Width / 2 - panel1.Size.Width / 2,
                this.ClientSize.Height / 2 - panel1.Size.Height / 2);
            panel1.Anchor = AnchorStyles.None;
            this.textBoxUserName.Focus();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to Close ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();

            }
        }
        private async void btnLogin_Click(object sender, EventArgs e)
        {
            
            try
            {
                picBoxLoading.Visible = true;
                //string a = StaticClassUserData.IsSuccess;
                UserManager obj = new UserManager();
                LogManager logManager = new LogManager();
                ConfigurationManager configurationManager = new ConfigurationManager();
                if (string.IsNullOrEmpty(textBoxUserName.Text))
                {
                    MessageBox.Show("Please Enter User Name First", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxUserName.Focus();
                    picBoxLoading.Visible = false;
                    return;
                }
                if (string.IsNullOrEmpty(textBoxPassword.Text))
                {
                    MessageBox.Show("Please Enter Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxPassword.Focus();
                    picBoxLoading.Visible = false;
                    return;
                }
                btnLogin.Enabled = false;
                Rootobject result = await Task.Run(() =>  obj.Login(textBoxUserName.Text, textBoxPassword.Text));
                if (result == null || result.responseObject == null)
                {
                    MessageBox.Show("Network Error. Please check your network connection or contact system administrator.");
                    picBoxLoading.Visible = false;
                }
                if (StaticClassUserData.IsSuccess == "True")
                {
                    picBoxLoading.Visible = false;
                    StaticClassUserData.Email = textBoxUserName.Text;
                    StaticClassUserData.OrganizationID = result.responseObject.userInfo.OrganizationID;
                    // var loginDate = logManager.LoadLog(StaticClassUserData.UserID);
                    // StaticClassUserData.LastLogin = loginDate.ToString();

                    textBoxPassword.Text = "";
                    this.Hide();
                    foreach (var item in result.responseObject.userActions)
                    {
                        string ActionName = item.ActionName;
                        string ActionCode = item.ActionCode;

                        if (ActionName == "Card Issuance" && ActionCode == "1001")
                        {
                            IsCardIssued = true;
                        }
                        if (ActionName == "Card Block" && ActionCode == "1002")
                        {
                            IsCardBlockEnable = true;
                        }
                        if (ActionName == "Card Top-up" && ActionCode == "1003")
                        {
                            IsCardTopup = true;
                        }
                        if (ActionName == "Single Journey Ticket" && ActionCode == "1004")
                        {
                            IsSingleJourneyTicketEnable = true;
                        }
                        if (ActionName == "Regenerate QR" && ActionCode == "1005")
                        {
                            IsRegenerateQR = true;
                        }
                        if (ActionName == "Card Initialization" && ActionCode == "1006")
                        {
                            IsCardInitialized = true;
                        }
                        if (ActionName == "Scan QR" && ActionCode == "1007")
                        {
                            IsQRScan = true;
                        }
                    }
                    MainForm objmain = new MainForm();
                    objmain.Show();
                    backgroundWorker1.RunWorkerAsync();
                }
                else if(StaticClassUserData.IsSuccess == "False")
                {
                    MessageBox.Show("Invalid Credentials. Please contact system administrator.");
                    picBoxLoading.Visible = false;
                }
                
                btnLogin.Enabled = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
           if(!SynchronizewithServer())
            {
                Application.Exit();
            }
        }

        public static bool SynchronizewithServer()
        {
            ConfigurationManager configurationManager = new ConfigurationManager();
            UserManager obj = new UserManager();
            LogManager logManager = new LogManager();

            LogModel logModel = new LogModel();
            logModel.UserID = StaticClassUserData.UserID;
            logModel.ActionPerform = "logged in";
            logModel.LoginDate = DateTime.Now.ToString();
            logModel.UserName = StaticClassUserData.Name;
            string MACAddress = Constants.MacAddress();
            //MACAddress = NetworkInterface.GetAllNetworkInterfaces().Where(x => x.OperationalStatus == OperationalStatus.Up).Select(x => x.GetPhysicalAddress()).FirstOrDefault().ToString();
            RootobjectSync result = configurationManager.Synchronize(MACAddress);
            if (result == null || result.responseObject == null)
            {
                MessageBox.Show("Network Error. Please check your network connection or contact system administrator.");
                return false;
            }
            if (StaticClassSyncData.IsSuccess == "True")
            {
                try
                {
                    ConfigurationViewModel model = new ConfigurationViewModel();
                    logModel.IP_Address = StaticClassSyncData.POS_IP;

                    model.POS_ID = result.responseObject.posDetails.POS_ID;
                    StaticClassSyncData.POS_ID = model.POS_ID;
                    logManager.SaveLog(logModel);
                    var configurationViewModels = configurationManager.LoadConfiguration(StaticClassSyncData.POS_Code);
                    if (configurationViewModels.Count == 0)
                    {

                        model.POS_ID = StaticClassSyncData.POS_ID;
                        model.POS_StationID = StaticClassSyncData.POS_StationID;
                        model.POS_Name = StaticClassSyncData.POS_Name;
                        model.POS_Code = StaticClassSyncData.POS_Code;
                        model.POS_MaxSaleLimit = StaticClassSyncData.POS_MaxSaleLimit;
                        model.POS_IP = StaticClassSyncData.POS_IP;
                        model.POS_MinTopUp = StaticClassSyncData.POS_MinTopUp;
                        model.POS_MaxTopUp = StaticClassSyncData.POS_MaxTopUp;
                        model.POS_Description = StaticClassSyncData.POS_Description;
                        model.POS_FareAmount = StaticClassSyncData.POS_FareAmount;
                        model.POS_TicketValidityMinutes = StaticClassSyncData.pos_TicketValidityMinutes;
                        model.Station_Latitude = StaticClassSyncData.Station_Latitude;
                        model.Station_Longitude = StaticClassSyncData.Station_Longitude;
                        model.Station_Name = StaticClassSyncData.Station_Name;
                        model.Station_Details = StaticClassSyncData.Station_Details;
                        model.Station_StationCode = StaticClassSyncData.Station_StationCode;
                        model.Station_MaxSaleLimit = StaticClassSyncData.Station_MaxSaleLimit;
                        model.Organization_Name = StaticClassSyncData.Organization_Name;
                        model.Organization_ZipCode = StaticClassSyncData.ZipCode;
                        model.Organization_Mobile = StaticClassSyncData.Organization_Mobile;
                        model.Organization_Telephone = StaticClassSyncData.Organization_Telephone;
                        model.Organization_Skype = StaticClassSyncData.Organization_Skype;
                        model.Organization_Fax = StaticClassSyncData.Organization_Fax;
                        model.Organization_Address = StaticClassSyncData.Organization_Address;
                        model.Organization_Remarks = StaticClassSyncData.Organization_Remarks;
                        model.Organization_OrganizationTypeID = StaticClassSyncData.Organization_OrganizationTypeID;
                        model.PersonInchargeID = StaticClassSyncData.PersonInchargeID;
                        model.POS_PSAM_MAC = StaticClassSyncData.POS_PSAM_MAC;
                        model.PreviousStationID = StaticClassSyncData.PreviousStationID;
                        model.Station_OrganizationID = StaticClassSyncData.Station_OrganizationID;
                        model.Station_PersonInchargeID = StaticClassSyncData.Station_PersonInchargeID;
                        model.Station_RouteID = StaticClassSyncData.Station_RouteID;
                        model.Station_SerialNumber = StaticClassSyncData.Station_SerialNumber;
                        model.SyncDateTime = DateTime.Now.ToString();
                        StaticClassSyncData.SyncDateTime = model.SyncDateTime;
                        model.Mac_Address = StaticClassSyncData.POS_PSAM_MAC;
                        configurationManager.SaveConfiguration(model);

                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("MAC Address binding missing for MAC = [" + MACAddress+"]");
                return false;
            }
            
        }
    }
}
