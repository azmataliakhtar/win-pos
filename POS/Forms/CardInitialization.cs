﻿using BLL.InitializeCardManager;
using BLL.IssueCardManager;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.Models.ViewModel;
using LMKR.RFID.MiFare1K;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.InitializeCardModel;
using static BLL.Models.IssueCardModel;

namespace POS.Forms
{
    public partial class CardInitialization : Form
    {
        public CardInitialization()
        {
            InitializeComponent();
        }
        InitializeCardManager InitializeCard = new InitializeCardManager();
        private void buttonInitializeCard_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBoxCardNumber.Text))
                {
                    MessageBox.Show("Please Enter Card Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxCardNumber.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSerialNumber.Text))
                {
                    MessageBox.Show("Please Enter card Serial Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxSerialNumber.Focus();
                    return;
                }
                InitializeCard initializeCard = new InitializeCard();
                initializeCard.CardNumber = textBoxCardNumber.Text;
                initializeCard.SerialNumber = textBoxSerialNumber.Text;
                var result = T10M1K.InitializeCard(initializeCard.SerialNumber, "1234", initializeCard.CardNumber, StaticClassSyncData.POS_Code);
                if (result.isSuccess)
                {
                    RootobjectInitializeCard rootobjectInitializeCard = InitializeCard.InitializeCard(initializeCard);
                    if (rootobjectInitializeCard.Heading == "Warning")
                    {
                        MessageBox.Show(rootobjectInitializeCard.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (rootobjectInitializeCard.Heading == "Success")
                    {
                        MessageBox.Show(rootobjectInitializeCard.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LogManager logManager = new LogManager();
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = "Card initialized,card number= " + initializeCard.CardNumber;
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                        pictureBoxCardInitilizaton.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show(rootobjectInitializeCard.ExceptionMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CardInitialization_Load(object sender, EventArgs e)
        {

            pictureBoxInitializedScreen.Visible = true;
            panel2.Visible = false;
            pictureBoxCardInitilizaton.Visible = false;
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                var response = T10M1K.ReadCardSerial();
                // response.isSuccess = true;

                if (response.isSuccess)
                {
                    RootobjectCardHolderInformation obj = InitializeCard.GetCardHolderInformationBySerialNumber(response.ReturData.ToString());
                    if (obj.Heading == "Error!")
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                    }
                    else if (obj.Message == "Serial Number not found")
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                        pictureBoxCardInitilizaton.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 0)
                    {

                        pictureBoxInitializedScreen.Visible = false;
                        panel2.Visible = true;
                        textBoxCardNumber.Text = obj.responseObject.CardNumber;
                        textBoxSerialNumber.Text = obj.responseObject.SerialNumber;
                        pictureBoxCardInitilizaton.Visible = true;
                    }
                    if (obj.responseObject.CardStatus == 1)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                        pictureBoxCardInitilizaton.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 2)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                        pictureBoxCardInitilizaton.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 3)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxInitializedScreen.Visible = true;
                        panel2.Visible = false;
                        pictureBoxCardInitilizaton.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("Put the card in scanner", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
          
            pictureBoxInitializedScreen.Visible = true;
            panel2.Visible = false;
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            btnReadCard_Click(null,null);
        }

        private void pictureBoxInitialize_Click(object sender, EventArgs e)
        {
            buttonInitializeCard_Click(null,null);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                // Set cursor as hourglass
                Cursor.Current = Cursors.WaitCursor;
                buttonInitializeCard_Click(null, null);
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
