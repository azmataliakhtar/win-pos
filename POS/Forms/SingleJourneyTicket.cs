﻿using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.Models.ViewModel;
using BLL.QRManager;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class SingleJourneyTicket : Form
    {
        private static List<Stream> m_streams;
        private static int m_currentPageIndex = 0;
        private StationsViewModel stationsVM;
        public SingleJourneyTicket()
        {
            InitializeComponent();
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                LogManager logManager = new LogManager();
                QrManager qrManager = new QrManager();
                if (string.IsNullOrEmpty(textBoxTotalReceit.Text))
                {
                    MessageBox.Show("Please select a valid number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxTotalReceit.Focus();
                    return;
                }
                for (int i = 0; i < Convert.ToInt32(textBoxTotalReceit.Text.Count()); i++)
                {

                    LocalReport report = new LocalReport();
                    //RootobjectQR rootobjectQR = qrManager.GetQR("1001", "test@mail.com", "123", "123");
                    RootobjectQR rootobjectQR = qrManager.GetQR("1001", "test@mail.com", "123", StaticClassUserData.UserID);
                    if (rootobjectQR.status)
                    {
                        rootobjectQR.responseObject.fileAsBase64 = rootobjectQR.responseObject.fileAsBase64.Replace("data:image/png;base64,", "");
                        // Base64ToImage(rootobjectQR.responseObject.fileAsBase64);
                        //string path = Path.GetDirectoryName(Application.ExecutablePath);
                        string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\QRPrint.rdlc";
                        report.ReportPath = fullPath;
                        this.reportViewer2.LocalReport.ReportPath = fullPath;

                        this.reportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Image", rootobjectQR.responseObject.fileAsBase64));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("QrNumber", rootobjectQR.responseObject.qrNumber));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("POSName", StaticClassSyncData.POS_Name));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("expiryTime", rootobjectQR.responseObject.expiryTime));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("FareAmount", rootobjectQR.responseObject.fareAmount));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Operator", StaticClassUserData.Name));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("IssueTime", rootobjectQR.responseObject.creationDate));
                        // Set cursor as hourglass
                        Cursor.Current = Cursors.WaitCursor;
                        //this.reportViewer2.RefreshReport();
                        this.reportViewer2.LocalReport.Print("SingleJourney");
                        // Set cursor as default arrow
                        Cursor.Current = Cursors.Default;
                        // Log Saving in tblLog in Sqlite Database
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = "QR Print" + rootobjectQR.responseObject.qrNumber;
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);
                        //MessageBox.Show("QrTicket print successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = rootobjectQR.status + rootobjectQR.responseObject.qrNumber;
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);
                       // MessageBox.Show("QrTicket print successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex.Message, ex.InnerException?.Message);
            }
            Cursor.Current = Cursors.Default;
        }
        public static void DisposePrint()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }

        private void SingleJourneyTicket_Load(object sender, EventArgs e)
        {
            textBoxTotalReceit.SelectedIndex = 0;
            QrManager qrManager = new QrManager();
            var stationsData = qrManager.GetStationsList(StaticClassUserData.OrganizationID);
            stationsVM = new StationsViewModel(StaticClassUserData.OrganizationID, stationsData, StaticClassSyncData.Station_StationCode);
            StationSelectionDropDown.DisplayMember = "Name";
            StationSelectionDropDown.ValueMember = "Code";
            StationSelectionDropDown.DataSource = stationsVM.Stations;
            StationSelectionDropDown.SelectedValue = stationsVM.SelectedStation.Code;
            //this.reportViewer2.RefreshReport();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

            try
            {
                btnReadCard_Click(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void StationSelectionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            StaticClassSyncData.Station_StationCode = StationSelectionDropDown.SelectedValue.ToString();
        }
    }
}
