﻿using BLL.ConfiguratonManager;
using BLL.Models;
using BLL.Models.ViewModel;
using System;
using System.Management;
using System.Windows.Forms;
using static BLL.Models.SynchronizeModel;

namespace POS.Forms
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
        }
        public string GetSystemMACID()
        {
            string systemName = System.Windows.Forms.SystemInformation.ComputerName;
            try
            {
                ManagementScope theScope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");
                ObjectQuery theQuery = new ObjectQuery("SELECT * FROM Win32_NetworkAdapter");
                ManagementObjectSearcher theSearcher = new ManagementObjectSearcher(theScope, theQuery);
                ManagementObjectCollection theCollectionOfResults = theSearcher.Get();

                foreach (ManagementObject theCurrentObject in theCollectionOfResults)
                {
                    if (theCurrentObject["MACAddress"] != null)
                    {
                        string macAdd = theCurrentObject["MACAddress"].ToString();
                        return macAdd.Replace(':', '-');
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return string.Empty;
        }
        private void Configuration_Load(object sender, EventArgs e)
        {
            try
            {
                ConfigurationManager configurationManager = new ConfigurationManager();
                //    string MACAddress = "1.2.3.4";
                //    ConfigurationManager configurationManager = new ConfigurationManager();
                //    RootobjectSync result =configurationManager.Synchronize(MACAddress);
                var configurationViewModels = configurationManager.LoadConfiguration(StaticClassSyncData.POS_Code);
                if (configurationViewModels.Count >= 0)
                {
                    textBoxID.Text = configurationViewModels[0].ID.ToString();
                    textBoxOrganizationName.Text = configurationViewModels[0].Organization_Name;
                    textBoxPSAMNumber.Text = configurationViewModels[0].POS_PSAM_MAC;
                    textBoxPOSIP.Text = configurationViewModels[0].POS_IP;
                    textBoxMACAddress.Text = configurationViewModels[0].POS_PSAM_MAC;
                    textBoxStationName.Text = configurationViewModels[0].Station_Name;
                    textBoxStationCode.Text = configurationViewModels[0].Station_StationCode;
                    textBoxOrganizationAddress.Text = configurationViewModels[0].Organization_Address;
                    textBoxMobile.Text = configurationViewModels[0].Organization_Mobile;
                    textBoxFax.Text = configurationViewModels[0].Organization_Fax;
                    textBoxSkype.Text = configurationViewModels[0].Organization_Skype;
                    textBoxDescription.Text = configurationViewModels[0].POS_Description;
                    textBoxPOSCode.Text = configurationViewModels[0].POS_Code;
                    textBoxLastSyncDateTime.Text = configurationViewModels[0].SyncDateTime;
                    textBoxPOSMaxTopup.Text = configurationViewModels[0].POS_MaxTopUp;
                    textBoxtextBoxPOSMinTopup.Text = configurationViewModels[0].POS_MinTopUp;

                }

                string macAddress = GetSystemMACID();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //textBoxMACAddress.Text = macAddress;
            //textBoxOrganizationCode.Text = "Test Organization";

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSyncData_Click(object sender, EventArgs e)
        {
            try
            {

               if(!Login.SynchronizewithServer())
                {
                    Application.Exit();
                }
                ConfigurationManager configurationManager = new ConfigurationManager();
                ConfigurationViewModel model = new ConfigurationViewModel();
                model.POS_ID = StaticClassSyncData.POS_ID;
                model.POS_StationID = StaticClassSyncData.POS_StationID;
                model.POS_Name = StaticClassSyncData.POS_Name;
                model.POS_Code = StaticClassSyncData.POS_Code;
                model.POS_MaxSaleLimit = StaticClassSyncData.POS_MaxSaleLimit;
                model.POS_IP = StaticClassSyncData.POS_IP;
                model.POS_MinTopUp = StaticClassSyncData.POS_MinTopUp;
                model.POS_MaxTopUp = StaticClassSyncData.POS_MaxTopUp;
                model.POS_Description = StaticClassSyncData.POS_Description;
                model.POS_FareAmount = StaticClassSyncData.POS_FareAmount;
                model.POS_TicketValidityMinutes = StaticClassSyncData.pos_TicketValidityMinutes;
                model.Station_Latitude = StaticClassSyncData.Station_Latitude;
                model.Station_Longitude = StaticClassSyncData.Station_Longitude;
                model.Station_Name = StaticClassSyncData.Station_Name;
                model.Station_Details = StaticClassSyncData.Station_Details;
                model.Station_StationCode = StaticClassSyncData.Station_StationCode;
                model.Station_MaxSaleLimit = StaticClassSyncData.Station_MaxSaleLimit;
                model.Organization_Name = StaticClassSyncData.Organization_Name;
                model.Organization_ZipCode = StaticClassSyncData.ZipCode;
                model.Organization_Mobile = StaticClassSyncData.Organization_Mobile;
                model.Organization_Telephone = StaticClassSyncData.Organization_Telephone;
                model.Organization_Skype = StaticClassSyncData.Organization_Skype;
                model.Organization_Fax = StaticClassSyncData.Organization_Fax;
                model.Organization_Address = StaticClassSyncData.Organization_Address;
                model.Organization_Remarks = StaticClassSyncData.Organization_Remarks;


                model.Organization_OrganizationTypeID = StaticClassSyncData.Organization_OrganizationTypeID;
                model.PersonInchargeID = StaticClassSyncData.PersonInchargeID;
                model.POS_PSAM_MAC = StaticClassSyncData.POS_PSAM_MAC;
                model.PreviousStationID = StaticClassSyncData.PreviousStationID;
                model.Station_OrganizationID = StaticClassSyncData.Station_OrganizationID;
                model.Station_PersonInchargeID = StaticClassSyncData.Station_PersonInchargeID;
                model.Station_RouteID = StaticClassSyncData.Station_RouteID;
                model.Station_SerialNumber = StaticClassSyncData.Station_SerialNumber;

                model.SyncDateTime = DateTime.Now.ToString();
                StaticClassSyncData.SyncDateTime = model.SyncDateTime;
                model.Mac_Address = StaticClassSyncData.POS_PSAM_MAC;
                model.ID = Convert.ToInt32(textBoxID.Text);
                if (configurationManager.UpdateConfiguration(model) == 1)
                {
                    Configuration_Load(null, null);
                    MessageBox.Show("Configuration Setting Updated Successfully.");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
