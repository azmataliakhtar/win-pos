﻿
namespace POS.Forms
{
    partial class CardInitialization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardInitialization));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonInitializeCard = new System.Windows.Forms.Button();
            this.pictureBoxInitializedScreen = new System.Windows.Forms.PictureBox();
            this.textBoxSerialNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBoxInitialize = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.btnReadCard = new System.Windows.Forms.Button();
            this.pictureBoxCardInitilizaton = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInitializedScreen)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInitialize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardInitilizaton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(0, 104);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 89);
            this.panel1.TabIndex = 42;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(955, 11);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 26);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(404, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(179, 35);
            this.label8.TabIndex = 0;
            this.label8.Text = "Initialize Card";
            // 
            // buttonInitializeCard
            // 
            this.buttonInitializeCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(122)))), ((int)(((byte)(96)))));
            this.buttonInitializeCard.Font = new System.Drawing.Font("Work Sans SemiBold", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInitializeCard.ForeColor = System.Drawing.Color.Transparent;
            this.buttonInitializeCard.Location = new System.Drawing.Point(130, 500);
            this.buttonInitializeCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonInitializeCard.Name = "buttonInitializeCard";
            this.buttonInitializeCard.Size = new System.Drawing.Size(437, 81);
            this.buttonInitializeCard.TabIndex = 2;
            this.buttonInitializeCard.Text = "Initialize Card";
            this.buttonInitializeCard.UseVisualStyleBackColor = false;
            this.buttonInitializeCard.Click += new System.EventHandler(this.buttonInitializeCard_Click);
            // 
            // pictureBoxInitializedScreen
            // 
            this.pictureBoxInitializedScreen.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxInitializedScreen.Image")));
            this.pictureBoxInitializedScreen.Location = new System.Drawing.Point(246, 316);
            this.pictureBoxInitializedScreen.Name = "pictureBoxInitializedScreen";
            this.pictureBoxInitializedScreen.Size = new System.Drawing.Size(509, 354);
            this.pictureBoxInitializedScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxInitializedScreen.TabIndex = 44;
            this.pictureBoxInitializedScreen.TabStop = false;
            // 
            // textBoxSerialNumber
            // 
            this.textBoxSerialNumber.Enabled = false;
            this.textBoxSerialNumber.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSerialNumber.Location = new System.Drawing.Point(260, 14);
            this.textBoxSerialNumber.Name = "textBoxSerialNumber";
            this.textBoxSerialNumber.Size = new System.Drawing.Size(304, 36);
            this.textBoxSerialNumber.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 35);
            this.label1.TabIndex = 47;
            this.label1.Text = "Card Serial";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 35);
            this.label2.TabIndex = 47;
            this.label2.Text = "Card Number";
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Enabled = false;
            this.textBoxCardNumber.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCardNumber.Location = new System.Drawing.Point(260, 66);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(304, 36);
            this.textBoxCardNumber.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBoxInitialize);
            this.panel2.Controls.Add(this.textBoxSerialNumber);
            this.panel2.Controls.Add(this.textBoxCardNumber);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.buttonInitializeCard);
            this.panel2.Location = new System.Drawing.Point(150, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(718, 585);
            this.panel2.TabIndex = 48;
            // 
            // pictureBoxInitialize
            // 
            this.pictureBoxInitialize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(122)))), ((int)(((byte)(96)))));
            this.pictureBoxInitialize.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxInitialize.Image")));
            this.pictureBoxInitialize.Location = new System.Drawing.Point(129, 511);
            this.pictureBoxInitialize.Name = "pictureBoxInitialize";
            this.pictureBoxInitialize.Size = new System.Drawing.Size(75, 61);
            this.pictureBoxInitialize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxInitialize.TabIndex = 48;
            this.pictureBoxInitialize.TabStop = false;
            this.pictureBoxInitialize.Click += new System.EventHandler(this.pictureBoxInitialize_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(190, 25);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(64, 49);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 50;
            this.pictureBox18.TabStop = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRefresh.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Transparent;
            this.btnRefresh.Location = new System.Drawing.Point(9, 11);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Padding = new System.Windows.Forms.Padding(100, 2, 2, 2);
            this.btnRefresh.Size = new System.Drawing.Size(493, 79);
            this.btnRefresh.TabIndex = 49;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(666, 28);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(64, 49);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 52;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // btnReadCard
            // 
            this.btnReadCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.btnReadCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReadCard.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadCard.ForeColor = System.Drawing.Color.Transparent;
            this.btnReadCard.Location = new System.Drawing.Point(511, 11);
            this.btnReadCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReadCard.Name = "btnReadCard";
            this.btnReadCard.Padding = new System.Windows.Forms.Padding(100, 2, 2, 2);
            this.btnReadCard.Size = new System.Drawing.Size(480, 79);
            this.btnReadCard.TabIndex = 51;
            this.btnReadCard.Text = "Read Card";
            this.btnReadCard.UseVisualStyleBackColor = false;
            this.btnReadCard.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // pictureBoxCardInitilizaton
            // 
            this.pictureBoxCardInitilizaton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxCardInitilizaton.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCardInitilizaton.Image")));
            this.pictureBoxCardInitilizaton.Location = new System.Drawing.Point(276, 689);
            this.pictureBoxCardInitilizaton.Name = "pictureBoxCardInitilizaton";
            this.pictureBoxCardInitilizaton.Size = new System.Drawing.Size(441, 100);
            this.pictureBoxCardInitilizaton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCardInitilizaton.TabIndex = 57;
            this.pictureBoxCardInitilizaton.TabStop = false;
            this.pictureBoxCardInitilizaton.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // CardInitialization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(997, 848);
            this.Controls.Add(this.pictureBoxCardInitilizaton);
            this.Controls.Add(this.pictureBoxInitializedScreen);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.btnReadCard);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRefresh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CardInitialization";
            this.Text = "CardInitialization";
            this.Load += new System.EventHandler(this.CardInitialization_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInitializedScreen)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInitialize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardInitilizaton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonInitializeCard;
        private System.Windows.Forms.TextBox textBoxSerialNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCardNumber;
        public System.Windows.Forms.PictureBox pictureBoxInitializedScreen;
        public System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Button btnReadCard;
        private System.Windows.Forms.PictureBox pictureBoxInitialize;
        private System.Windows.Forms.PictureBox pictureBoxCardInitilizaton;
    }
}