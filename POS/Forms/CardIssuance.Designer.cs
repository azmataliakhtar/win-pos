﻿
namespace POS.Forms
{
    partial class CardIssuance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardIssuance));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelMainScreen = new System.Windows.Forms.Panel();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.groupBoxCardHolderInfo = new System.Windows.Forms.GroupBox();
            this.textBoxGender = new System.Windows.Forms.ComboBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxCNIC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBoxCardTypeInfo = new System.Windows.Forms.GroupBox();
            this.textBoxMinTopup = new System.Windows.Forms.TextBox();
            this.textBoxCardDiscount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxValidity = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCardFee = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCardType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxCardIssuance = new System.Windows.Forms.PictureBox();
            this.btnReadCard = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.buttonIssueCard = new System.Windows.Forms.PictureBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMainScreen.SuspendLayout();
            this.groupBoxCardHolderInfo.SuspendLayout();
            this.groupBoxCardTypeInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardIssuance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonIssueCard)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(0, 86);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(886, 71);
            this.panel1.TabIndex = 39;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(849, 9);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 21);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(353, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 29);
            this.label8.TabIndex = 0;
            this.label8.Text = "Issue Card";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(141, 169);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(9, 8);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // panelMainScreen
            // 
            this.panelMainScreen.Controls.Add(this.buttonPrint);
            this.panelMainScreen.Controls.Add(this.reportViewer2);
            this.panelMainScreen.Controls.Add(this.groupBoxCardHolderInfo);
            this.panelMainScreen.Controls.Add(this.groupBoxCardTypeInfo);
            this.panelMainScreen.Controls.Add(this.textBoxCardNumber);
            this.panelMainScreen.Controls.Add(this.label2);
            this.panelMainScreen.Controls.Add(this.comboBoxCardType);
            this.panelMainScreen.Controls.Add(this.label1);
            this.panelMainScreen.Location = new System.Drawing.Point(11, 174);
            this.panelMainScreen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelMainScreen.Name = "panelMainScreen";
            this.panelMainScreen.Size = new System.Drawing.Size(865, 507);
            this.panelMainScreen.TabIndex = 42;
            this.panelMainScreen.Visible = false;
            // 
            // reportViewer2
            // 
            this.reportViewer2.DocumentMapWidth = 30;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = null;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "POS.Reports.QRPrint1.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(787, 432);
            this.reportViewer2.Margin = new System.Windows.Forms.Padding(4);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ServerReport.BearerToken = null;
            this.reportViewer2.Size = new System.Drawing.Size(58, 36);
            this.reportViewer2.TabIndex = 61;
            this.reportViewer2.Visible = false;
            // 
            // groupBoxCardHolderInfo
            // 
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxGender);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxAddress);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxCNIC);
            this.groupBoxCardHolderInfo.Controls.Add(this.label7);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxPhone);
            this.groupBoxCardHolderInfo.Controls.Add(this.pictureBox1);
            this.groupBoxCardHolderInfo.Controls.Add(this.label9);
            this.groupBoxCardHolderInfo.Controls.Add(this.label12);
            this.groupBoxCardHolderInfo.Controls.Add(this.label10);
            this.groupBoxCardHolderInfo.Controls.Add(this.textBoxName);
            this.groupBoxCardHolderInfo.Controls.Add(this.label11);
            this.groupBoxCardHolderInfo.Location = new System.Drawing.Point(17, 229);
            this.groupBoxCardHolderInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardHolderInfo.Name = "groupBoxCardHolderInfo";
            this.groupBoxCardHolderInfo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardHolderInfo.Size = new System.Drawing.Size(836, 194);
            this.groupBoxCardHolderInfo.TabIndex = 60;
            this.groupBoxCardHolderInfo.TabStop = false;
            this.groupBoxCardHolderInfo.Text = "Card Holder Info";
            this.groupBoxCardHolderInfo.Visible = false;
            // 
            // textBoxGender
            // 
            this.textBoxGender.Font = new System.Drawing.Font("Work Sans", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGender.FormattingEnabled = true;
            this.textBoxGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.textBoxGender.Location = new System.Drawing.Point(604, 62);
            this.textBoxGender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxGender.Name = "textBoxGender";
            this.textBoxGender.Size = new System.Drawing.Size(195, 34);
            this.textBoxGender.TabIndex = 54;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddress.Location = new System.Drawing.Point(173, 112);
            this.textBoxAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(451, 65);
            this.textBoxAddress.TabIndex = 4;
            // 
            // textBoxCNIC
            // 
            this.textBoxCNIC.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCNIC.Location = new System.Drawing.Point(604, 20);
            this.textBoxCNIC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCNIC.Name = "textBoxCNIC";
            this.textBoxCNIC.Size = new System.Drawing.Size(195, 31);
            this.textBoxCNIC.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(444, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 29);
            this.label7.TabIndex = 53;
            this.label7.Text = "Gender";
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhone.Location = new System.Drawing.Point(173, 69);
            this.textBoxPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(180, 31);
            this.textBoxPhone.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(444, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 29);
            this.label9.TabIndex = 53;
            this.label9.Text = "CNIC";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 29);
            this.label12.TabIndex = 51;
            this.label12.Text = "Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 29);
            this.label10.TabIndex = 51;
            this.label10.Text = "Phone";
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(173, 20);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(180, 31);
            this.textBoxName.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 29);
            this.label11.TabIndex = 51;
            this.label11.Text = "Name";
            // 
            // groupBoxCardTypeInfo
            // 
            this.groupBoxCardTypeInfo.Controls.Add(this.textBoxMinTopup);
            this.groupBoxCardTypeInfo.Controls.Add(this.textBoxCardDiscount);
            this.groupBoxCardTypeInfo.Controls.Add(this.label6);
            this.groupBoxCardTypeInfo.Controls.Add(this.textBoxValidity);
            this.groupBoxCardTypeInfo.Controls.Add(this.label4);
            this.groupBoxCardTypeInfo.Controls.Add(this.label5);
            this.groupBoxCardTypeInfo.Controls.Add(this.textBoxCardFee);
            this.groupBoxCardTypeInfo.Controls.Add(this.label3);
            this.groupBoxCardTypeInfo.Location = new System.Drawing.Point(9, 68);
            this.groupBoxCardTypeInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardTypeInfo.Name = "groupBoxCardTypeInfo";
            this.groupBoxCardTypeInfo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxCardTypeInfo.Size = new System.Drawing.Size(836, 138);
            this.groupBoxCardTypeInfo.TabIndex = 59;
            this.groupBoxCardTypeInfo.TabStop = false;
            this.groupBoxCardTypeInfo.Text = "Card Type Info";
            // 
            // textBoxMinTopup
            // 
            this.textBoxMinTopup.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinTopup.Location = new System.Drawing.Point(604, 69);
            this.textBoxMinTopup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxMinTopup.Name = "textBoxMinTopup";
            this.textBoxMinTopup.Size = new System.Drawing.Size(195, 31);
            this.textBoxMinTopup.TabIndex = 54;
            this.textBoxMinTopup.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxMinTopup_Validating_1);
            // 
            // textBoxCardDiscount
            // 
            this.textBoxCardDiscount.Enabled = false;
            this.textBoxCardDiscount.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCardDiscount.Location = new System.Drawing.Point(604, 20);
            this.textBoxCardDiscount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCardDiscount.Name = "textBoxCardDiscount";
            this.textBoxCardDiscount.Size = new System.Drawing.Size(195, 31);
            this.textBoxCardDiscount.TabIndex = 54;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(444, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 29);
            this.label6.TabIndex = 53;
            this.label6.Text = "TopUp";
            // 
            // textBoxValidity
            // 
            this.textBoxValidity.Enabled = false;
            this.textBoxValidity.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxValidity.Location = new System.Drawing.Point(173, 69);
            this.textBoxValidity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxValidity.Name = "textBoxValidity";
            this.textBoxValidity.Size = new System.Drawing.Size(180, 31);
            this.textBoxValidity.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(444, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 29);
            this.label4.TabIndex = 53;
            this.label4.Text = "Card Discount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 29);
            this.label5.TabIndex = 51;
            this.label5.Text = "Card Validity";
            // 
            // textBoxCardFee
            // 
            this.textBoxCardFee.Enabled = false;
            this.textBoxCardFee.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCardFee.Location = new System.Drawing.Point(173, 20);
            this.textBoxCardFee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCardFee.Name = "textBoxCardFee";
            this.textBoxCardFee.Size = new System.Drawing.Size(180, 31);
            this.textBoxCardFee.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 29);
            this.label3.TabIndex = 51;
            this.label3.Text = "Card Fee";
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCardNumber.Location = new System.Drawing.Point(163, 21);
            this.textBoxCardNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(288, 31);
            this.textBoxCardNumber.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 29);
            this.label2.TabIndex = 57;
            this.label2.Text = "Card Number";
            // 
            // comboBoxCardType
            // 
            this.comboBoxCardType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCardType.Font = new System.Drawing.Font("Arial Narrow", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCardType.FormattingEnabled = true;
            this.comboBoxCardType.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxCardType.Location = new System.Drawing.Point(590, 17);
            this.comboBoxCardType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxCardType.Name = "comboBoxCardType";
            this.comboBoxCardType.Size = new System.Drawing.Size(278, 35);
            this.comboBoxCardType.TabIndex = 56;
            this.comboBoxCardType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCardType_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(468, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 29);
            this.label1.TabIndex = 55;
            this.label1.Text = "Card Type";
            // 
            // pictureBoxCardIssuance
            // 
            this.pictureBoxCardIssuance.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCardIssuance.Image")));
            this.pictureBoxCardIssuance.Location = new System.Drawing.Point(201, 223);
            this.pictureBoxCardIssuance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxCardIssuance.Name = "pictureBoxCardIssuance";
            this.pictureBoxCardIssuance.Size = new System.Drawing.Size(492, 357);
            this.pictureBoxCardIssuance.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCardIssuance.TabIndex = 45;
            this.pictureBoxCardIssuance.TabStop = false;
            this.pictureBoxCardIssuance.Visible = false;
            // 
            // btnReadCard
            // 
            this.btnReadCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.btnReadCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReadCard.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadCard.ForeColor = System.Drawing.Color.Transparent;
            this.btnReadCard.Location = new System.Drawing.Point(449, 12);
            this.btnReadCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReadCard.Name = "btnReadCard";
            this.btnReadCard.Padding = new System.Windows.Forms.Padding(89, 2, 2, 2);
            this.btnReadCard.Size = new System.Drawing.Size(427, 63);
            this.btnReadCard.TabIndex = 53;
            this.btnReadCard.Text = "Read Card";
            this.btnReadCard.UseVisualStyleBackColor = false;
            this.btnReadCard.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("Work Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Transparent;
            this.btnRefresh.Location = new System.Drawing.Point(3, 12);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Padding = new System.Windows.Forms.Padding(89, 2, 2, 2);
            this.btnRefresh.Size = new System.Drawing.Size(438, 63);
            this.btnRefresh.TabIndex = 52;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.pictureBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(162, 25);
            this.pictureBox18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(57, 39);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 54;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(210)))));
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(595, 25);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(57, 39);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 55;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // buttonIssueCard
            // 
            this.buttonIssueCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonIssueCard.Image = ((System.Drawing.Image)(resources.GetObject("buttonIssueCard.Image")));
            this.buttonIssueCard.Location = new System.Drawing.Point(247, 604);
            this.buttonIssueCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonIssueCard.Name = "buttonIssueCard";
            this.buttonIssueCard.Size = new System.Drawing.Size(342, 77);
            this.buttonIssueCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonIssueCard.TabIndex = 58;
            this.buttonIssueCard.TabStop = false;
            this.buttonIssueCard.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(161)))), ((int)(((byte)(230)))));
            this.buttonPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonPrint.Font = new System.Drawing.Font("Work Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.ForeColor = System.Drawing.Color.White;
            this.buttonPrint.Image = ((System.Drawing.Image)(resources.GetObject("buttonPrint.Image")));
            this.buttonPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrint.Location = new System.Drawing.Point(590, 443);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(134, 54);
            this.buttonPrint.TabIndex = 55;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPrint.UseVisualStyleBackColor = false;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // CardIssuance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(886, 683);
            this.Controls.Add(this.buttonIssueCard);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBoxCardIssuance);
            this.Controls.Add(this.btnReadCard);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelMainScreen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CardIssuance";
            this.Text = "CardIssuance";
            this.Load += new System.EventHandler(this.CardIssuance_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMainScreen.ResumeLayout(false);
            this.panelMainScreen.PerformLayout();
            this.groupBoxCardHolderInfo.ResumeLayout(false);
            this.groupBoxCardHolderInfo.PerformLayout();
            this.groupBoxCardTypeInfo.ResumeLayout(false);
            this.groupBoxCardTypeInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardIssuance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonIssueCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelMainScreen;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.GroupBox groupBoxCardHolderInfo;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxCNIC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBoxCardTypeInfo;
        private System.Windows.Forms.TextBox textBoxMinTopup;
        private System.Windows.Forms.TextBox textBoxCardDiscount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxValidity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCardFee;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCardNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCardType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxCardIssuance;
        private System.Windows.Forms.Button btnReadCard;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.ComboBox textBoxGender;
        private System.Windows.Forms.PictureBox buttonIssueCard;
        private System.Windows.Forms.Button buttonPrint;
    }
}