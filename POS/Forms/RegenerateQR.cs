﻿using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.RegenerateQRManager;
using Microsoft.Reporting.WinForms;
using System;
using System.IO;
using System.Windows.Forms;
using static BLL.Models.RegenerateQRDataModel;

namespace POS.Forms
{
    public partial class RegenerateQR : Form
    {
        public RegenerateQR()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                RegenerateQRManager regenerateQRManager = new RegenerateQRManager();
                if (string.IsNullOrEmpty(textBoxQRNumber.Text))
                {
                    MessageBox.Show("Please Enter Qr Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxQRNumber.Focus();
                    return;
                }
                LogManager logManager = new LogManager();
                LocalReport report = new LocalReport();
                RootobjectRegenrateQR rootobjectRegenrateQR = regenerateQRManager.RegenerateQR("1001", "test@mail.com", "123", textBoxQRNumber.Text);
                if (rootobjectRegenrateQR.status && rootobjectRegenrateQR.message != "QR Data Not Found.")
                {
                    rootobjectRegenrateQR.responseObject.fileAsBase64 = rootobjectRegenrateQR.responseObject.fileAsBase64.Replace("data:image/png;base64,", "");
                    //string path = Path.GetDirectoryName(Application.ExecutablePath);
                    string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\RegenerateQR.rdlc";
                    report.ReportPath = fullPath;

                    this.reportViewer2.LocalReport.ReportPath = fullPath;
                    this.reportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Image", rootobjectRegenrateQR.responseObject.fileAsBase64));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("QrNumber", rootobjectRegenrateQR.responseObject.qrNumber));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("POSName", StaticClassSyncData.POS_Name));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("expiryTime", rootobjectRegenrateQR.responseObject.expiryTime));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("FareAmount", rootobjectRegenrateQR.responseObject.fareAmount));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Operator", StaticClassUserData.Name));
                    this.reportViewer2.LocalReport.SetParameters(new ReportParameter("IssueTime", rootobjectRegenrateQR.responseObject.creationDate));

                    this.reportViewer2.LocalReport.Print("Regenerate");
                    // Log Saving in tblLog in Sqlite Database
                    LogModel logModel = new LogModel();
                    logModel.UserID = StaticClassUserData.UserID;
                    logModel.ActionPerform = " Regenerated QR  Print QRNumber=" + rootobjectRegenrateQR.responseObject.qrNumber;
                    logModel.LoginDate = DateTime.Now.ToString();
                    logModel.UserName = StaticClassUserData.Name;
                    logModel.IP_Address = StaticClassSyncData.POS_IP;
                    logManager.SaveLog(logModel);
                    MessageBox.Show("RegeneratedQR print successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    LogModel logModel = new LogModel();
                    logModel.UserID = StaticClassUserData.UserID;
                    logModel.ActionPerform = rootobjectRegenrateQR.message + rootobjectRegenrateQR.responseObject.qrNumber;
                    logModel.LoginDate = DateTime.Now.ToString();
                    logModel.UserName = StaticClassUserData.Name;
                    logModel.IP_Address = StaticClassSyncData.POS_IP;

                    logManager.SaveLog(logModel);
                    MessageBox.Show(rootobjectRegenrateQR.message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                // Set cursor as hourglass
                Cursor.Current = Cursors.WaitCursor;
                btnReadCard_Click(null, null);
                // Set cursor as default arrow
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
