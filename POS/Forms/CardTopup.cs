﻿using BLL.InitializeCardManager;
using BLL.IssueCardManager;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.Models.ViewModel;
using BLL.TopupManager;
using LMKR.RFID.MiFare1K;
using Microsoft.Reporting.WinForms;
using POS.ExtentionMethod;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.CardRechargeModel;
using static BLL.Models.IssueCardModel;

namespace POS.Forms
{
    public partial class CardTopup : Form
    {
        public static string CardSerialnumber = "";
        public CardTopup()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        IssueCardManager issueCardManager = new IssueCardManager();
        CardtopupManager cardTopUp = new CardtopupManager();

        DataTable dt;
        private void CardIssuance_Load(object sender, EventArgs e)
        {
            try
            {
                panelMainScreen.Visible = false;
                pictureBoxCardtopup.Visible = true;

                RootobjectIssueCard rootobjectIssueCard = issueCardManager.GetCardTypeByOrganizationID(StaticClassUserData.OrganizationID);
                dt = ConvertListToDatatable.ToDataTable(rootobjectIssueCard.responseObject);
                comboBoxCardType.DataSource = dt;
                comboBoxCardType.DisplayMember = "Name";
                comboBoxCardType.ValueMember = "CardTypeID";
                comboBoxCardType.SelectedIndex = -1;
                // buttonIssueCard.Enabled = false;
                comboBoxPaymentMethod.SelectedIndex = 0;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        string CardType = "";
        int maxtopup = 0;
        int minTopup = 0;

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                var response = T10M1K.ReadCardSerial();
                //response.isSuccess = true;
                if (response.isSuccess)
                {
                    RootobjectCardHolderInformation obj = cardTopUp.GetCardHolderInformationBySerialNumber(response.ReturData.ToString());

                    if (obj.Message == "card not found")
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 0 && obj.responseObject != null)
                    {
                        MessageBox.Show("Card Not Initialized", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 1)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 2)
                    {
                        DataRow[] dr = dt.Select("CardTypeID='" + obj.responseObject.CardType + "'");


                        textBoxexpiry.Text = dr[0].ItemArray[9].ToString();
                        minTopup = Convert.ToInt32(dr[0].ItemArray[5].ToString());
                        maxtopup = Convert.ToInt32(dr[0].ItemArray[7].ToString());

                        bool isPersonalize = Convert.ToBoolean(dr[0].ItemArray[4].ToString());
                        CardType = dr[0].ItemArray[2].ToString();
                        comboBoxCardType.SelectedValue = obj.responseObject.CardType;
                        pictureBoxCardtopup.Visible = false;
                        panelMainScreen.Visible = true;
                        groupBoxCardHolderInfo.Visible = true;
                        textBoxCardNumber.Text = obj.responseObject.CardNumber;
                        CardSerialnumber = response.ReturData.ToString();

                        textBoxCurrentBalance.Text = ((T10M1K.ReadCardInfo(CardSerialnumber).ReturData) as dynamic).balance.ToString();//obj.responseObject.CurrentBalance.ToString();
                        textBoxexpiry.Text = obj.responseObject.expiryDate.ToString();
                        if (isPersonalize)
                        {
                            groupBoxCardHolderInfo.Visible = true;
                            textBoxName.Text = obj.responseObject.Name;
                            textBoxCNIC.Text = obj.responseObject.CNIC;
                            textBoxPhone.Text = obj.responseObject.Phone;
                            textBoxGender.Text = obj.responseObject.Gender;
                            textBoxAddress.Text = obj.responseObject.Address;
                        }
                        else
                        {
                            groupBoxCardHolderInfo.Visible = false;
                        }
                    }
                    else if (obj.responseObject.CardStatus == 3)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardtopup.Visible = true;
                        panelMainScreen.Visible = false;
                    }

                }
                else
                {
                    MessageBox.Show("Put the card in scanner", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            pictureBoxCardtopup.Visible = true;
            panelMainScreen.Visible = false;
        }
        private void pictureBox17_Click(object sender, EventArgs e)
        {
            btnReadCard_Click(null, null);
        }
        private void textBoxMinTopup_Validating_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //int input = 0;
            //bool isNum = Int32.TryParse(textBoxCurrentBalance.Text, out input);

            //if (!isNum || input < minTopup || input > maxtopup)
            //{
            //    // Cancel the event and select the text to be corrected by the user.
            //    e.Cancel = true;
            //    textBoxCurrentBalance.Select(0, textBoxCurrentBalance.Text.Length);
            //    MessageBox.Show("Value must be between " + minTopup + " and " + maxtopup + "", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
        }
        private void ClearData()
        {
            textBoxName.Text = "";
            textBoxCNIC.Text = "";
            textBoxPhone.Text = "";
            textBoxGender.Text = "";
            textBoxAddress.Text = "";
            panelMainScreen.Visible = false;
            pictureBoxCardtopup.Visible = true;
            textBoxexpiry.Text = "";
            textBoxCurrentBalance.Text = "";
        }

        private void textBoxTopupAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void buttonTopup_Click_1(object sender, EventArgs e)
        {
            LocalReport report = new LocalReport();
            if (string.IsNullOrEmpty(textBoxCardNumber.Text))
            {
                MessageBox.Show("Please Enter Card Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxCardNumber.Focus();
                return;
            }
            if (comboBoxCardType.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Card Type", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                comboBoxCardType.Focus();
                return;
            }
            CardRechargeViewModel rechargeViewModel = new CardRechargeViewModel();
            rechargeViewModel.CardNumber = textBoxCardNumber.Text;
            rechargeViewModel.SerialNumber = CardSerialnumber;
            rechargeViewModel.UserID = StaticClassUserData.UserID;
            rechargeViewModel.PaymentMethod = comboBoxPaymentMethod.Text;
            rechargeViewModel.RechargeAmount = Convert.ToInt32(textBoxTopupAmount.Value);
            rechargeViewModel.BalanceAfter = Convert.ToInt32(textBoxCurrentBalance.Text) + Convert.ToInt32(textBoxTopupAmount.Value);
            rechargeViewModel.TransactionType = "topup";
            //string CardSerial, int Topup
            try
            {
                var result = T10M1K.TopupCard(rechargeViewModel.SerialNumber, rechargeViewModel.RechargeAmount);
                if (result.isSuccess)
                {
                    RootobjectCardRecharge rootobjectcardRecharge = cardTopUp.Addtopup(rechargeViewModel);
                    if (rootobjectcardRecharge.Heading == "Success")
                    {
                        //string path = Path.GetDirectoryName(Application.ExecutablePath);
                        string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\CardRecharge.rdlc";
                        report.ReportPath = fullPath;
                        this.reportViewer2.LocalReport.ReportPath = fullPath;
                        this.reportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("CardType", CardType));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("CardNumber", textBoxCardNumber.Text));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Source", rootobjectcardRecharge.responseObject.POS_Code));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("TopupAmount", textBoxTopupAmount.Value.ToString()));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("TotalAmount", textBoxTopupAmount.Value.ToString()));
                        Cursor.Current = Cursors.WaitCursor;
                        this.reportViewer2.LocalReport.Print("Topup");
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Card Topup Receipt Printed Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LogManager logManager = new LogManager();
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = "Card Recharge(topup),card number= " + rechargeViewModel.CardNumber + "and card Topup receipt printed successfully";
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);
                        CardSerialnumber = "";
                        ClearData();
                    }
                    else
                    {
                        MessageBox.Show(rootobjectcardRecharge.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                else
                {
                    MessageBox.Show("un", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                // Set cursor as hourglass

                buttonTopup_Click_1(null, null);
                // Set cursor as default arrow
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
