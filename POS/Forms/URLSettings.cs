﻿using BLL.Models.ViewModel;
using BLL.ServiceURLSettingManager;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Net.Sockets;
using System.Windows.Forms;

namespace POS.Forms
{
    public partial class URLSettings : Form
    {
        public URLSettings()
        {
            InitializeComponent();
        }

        private void textBoxPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void URLSettings_Load(object sender, EventArgs e)
        {
            try
            {
                pictureBox1.Visible = false;
                DataTable dt = ServiceURLManager.LoadURLSettings();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        textBoxID1.Text = dt.Rows[i]["ID"].ToString();
                        textBoxIPAddress1.Text = dt.Rows[i]["IPAddress"].ToString();
                        textBoxPort1.Text = dt.Rows[i]["PortNumber"].ToString();
                        textBoxServiceName1.Text = dt.Rows[i]["ServiceName"].ToString();
                    }
                    else if (i == 1)
                    {
                        textBoxID2.Text = dt.Rows[i]["ID"].ToString();
                        textBoxIPAddress2.Text = dt.Rows[i]["IPAddress"].ToString();
                        textBoxPort2.Text = dt.Rows[i]["PortNumber"].ToString();
                        textBoxServiceName2.Text = dt.Rows[i]["ServiceName"].ToString();
                    }
                    else if (i == 2)
                    {
                        textBoxID3.Text = dt.Rows[i]["ID"].ToString();
                        textBoxIPAddress3.Text = dt.Rows[i]["IPAddress"].ToString();
                        textBoxPort3.Text = dt.Rows[i]["PortNumber"].ToString();
                        textBoxServiceName3.Text = dt.Rows[i]["ServiceName"].ToString();
                    }
                    else if (i == 3)
                    {
                        textBoxID4.Text = dt.Rows[i]["ID"].ToString();
                        textBoxIPAddress4.Text = dt.Rows[i]["IPAddress"].ToString();
                        textBoxPort4.Text = dt.Rows[i]["PortNumber"].ToString();
                        textBoxServiceName4.Text = dt.Rows[i]["ServiceName"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static bool PingHost(string hostUri, int portNumber)
        {
            try
            {
                using (TcpClient client = new TcpClient(hostUri, portNumber))
                {
                    return true;
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Not Responding:'" + hostUri + ":" + portNumber.ToString() + "'");
                return false;
            }
        }
        private void btnSyncData_Click(object sender, EventArgs e)
        {

            bool Status = PingHost(textBoxIPAddress1.Text, Convert.ToInt32(textBoxPort1.Text));
            if (Status)
            {
                MessageBox.Show("Success");
            }
            pictureBox1.Visible = false;
        }
        private void PrintPicture(object sender, PrintPageEventArgs e)
        {

            Bitmap bmp = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            pictureBox2.DrawToBitmap(bmp, new Rectangle(0, 0, pictureBox2.Width, pictureBox2.Height));
            e.Graphics.DrawImage(bmp, 0, 0);
            bmp.Dispose();

        }
        private void buttonTestPrinter_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDialog pd = new PrintDialog();
                PrintDocument pDoc = new PrintDocument();
                pDoc.PrintPage += PrintPicture;
                pd.Document = pDoc;
                pDoc.Print();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonTest2_Click(object sender, EventArgs e)
        {
            try
            {
                bool Status = PingHost(textBoxIPAddress2.Text, Convert.ToInt32(textBoxPort2.Text));
                if (Status)
                {
                    MessageBox.Show("Success");
                }
                pictureBox1.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonTest3_Click(object sender, EventArgs e)
        {
            try
            {
                bool Status = PingHost(textBoxIPAddress3.Text, Convert.ToInt32(textBoxPort3.Text));
                if (Status)
                {
                    MessageBox.Show("Success");
                }
                pictureBox1.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonTest4_Click(object sender, EventArgs e)
        {
            try
            {
                bool Status = PingHost(textBoxIPAddress4.Text, Convert.ToInt32(textBoxPort4.Text));
                if (Status)
                {
                    MessageBox.Show("Success");
                }
                pictureBox1.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSaveURLSettings_Click(object sender, EventArgs e)
        {
            try
            {
                bool allSaved = false;
                DataTable dt = ServiceURLManager.LoadURLSettings();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        textBoxServiceName1.Text = dt.Rows[i]["ServiceName"].ToString();
                        URLSettingViewModel model = new URLSettingViewModel();
                        model.ID = Convert.ToInt32(textBoxID1.Text);
                        model.IPAddress = textBoxIPAddress1.Text;
                        model.PortNumber = Convert.ToInt32(textBoxPort1.Text);
                        allSaved = ServiceURLManager.UpdateURLSettings(model) ==1 ? true:false;
                    }
                    else if (i == 1)
                    {
                        URLSettingViewModel model = new URLSettingViewModel();
                        model.ID = Convert.ToInt32(textBoxID2.Text);
                        model.IPAddress = textBoxIPAddress2.Text;
                        model.PortNumber = Convert.ToInt32(textBoxPort2.Text);
                        allSaved = ServiceURLManager.UpdateURLSettings(model)==1 ? true:false;
                    }
                    else if (i == 2)
                    {
                        URLSettingViewModel model = new URLSettingViewModel();
                        model.ID = Convert.ToInt32(textBoxID3.Text);
                        model.IPAddress = textBoxIPAddress3.Text;
                        model.PortNumber = Convert.ToInt32(textBoxPort3.Text);
                        allSaved = ServiceURLManager.UpdateURLSettings(model) == 1 ? true : false;
                    }
                    else if (i == 3)
                    {
                        URLSettingViewModel model = new URLSettingViewModel();
                        model.ID = Convert.ToInt32(textBoxID4.Text);
                        model.IPAddress = textBoxIPAddress4.Text;
                        model.PortNumber = Convert.ToInt32(textBoxPort4.Text);
                        allSaved = ServiceURLManager.UpdateURLSettings(model) == 1 ? true : false;
                    }
                }
                if (allSaved)
                {
                    MessageBox.Show("URL settings saved", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                var resutl = LMKR.RFID.MiFare1K.T10M1K.TestReader();
                MessageBox.Show(resutl.Message);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
