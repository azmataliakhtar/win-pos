﻿using BLL.InitializeCardManager;
using BLL.IssueCardManager;
using BLL.LogManager;
using BLL.Model;
using BLL.Models;
using BLL.Models.ViewModel;
using LMKR.RFID.MiFare1K;
using Microsoft.Reporting.WinForms;
using POS.ExtentionMethod;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using static BLL.Models.CardHolderInformationModel;
using static BLL.Models.IssueCardModel;

namespace POS.Forms
{
    public partial class CardIssuance : Form
    {
        public static string CardSerialnumber = "";
        string CardType = "";
        int maxtopup = 0;
        int minTopup = 0;
        bool isPersonalize = false;
        public CardIssuance()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        IssueCardManager issueCardManager = new IssueCardManager();
        DataTable dt;
        private void CardIssuance_Load(object sender, EventArgs e)
        {
            try
            {
                panelMainScreen.Visible = false;
                pictureBoxCardIssuance.Visible = true;

                RootobjectIssueCard rootobjectIssueCard = issueCardManager.GetCardTypeByOrganizationID(StaticClassUserData.OrganizationID);
                dt = ConvertListToDatatable.ToDataTable(rootobjectIssueCard.responseObject);
                comboBoxCardType.DataSource = dt;
                comboBoxCardType.DisplayMember = "Name";
                comboBoxCardType.ValueMember = "CardTypeID";
                comboBoxCardType.SelectedIndex = -1;
                buttonIssueCard.Visible = false;
                buttonPrint.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
   
        private void comboBoxCardType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                var CardTypeID = comboBoxCardType.SelectedValue;
                DataRow[] dr = dt.Select("CardTypeID='" + CardTypeID + "'");
                textBoxCardFee.Text = dr[0].ItemArray[5].ToString(); //dt.Rows[0]["CardPrice"].ToString();
                textBoxCardDiscount.Text = "";
                textBoxCardDiscount.Text += dr[0].ItemArray[10].ToString() + "%";

                textBoxValidity.Text = dr[0].ItemArray[9].ToString();
                minTopup = Convert.ToInt32(dr[0].ItemArray[6]);
                // minTopup = Convert.ToInt32(textBoxMinTopup.Text);
                maxtopup = Convert.ToInt32(dr[0].ItemArray[7]);
                textBoxMinTopup.Text = dr[0].ItemArray[6].ToString();
                isPersonalize = Convert.ToBoolean(dr[0].ItemArray[4].ToString());
                CardType = dr[0].ItemArray[2].ToString();
                if (isPersonalize)
                {
                    groupBoxCardHolderInfo.Visible = true;
                    groupBoxCardTypeInfo.Visible = true;
                    RootobjectCardHolderInformation rootobjectCardHolderInformation = issueCardManager.GetCardHolderInformation(textBoxCardNumber.Text);
                    textBoxName.Text = rootobjectCardHolderInformation.responseObject.CardHolderName;
                    textBoxCNIC.Text = rootobjectCardHolderInformation.responseObject.CNIC;
                    textBoxPhone.Text = rootobjectCardHolderInformation.responseObject.Phone;
                    textBoxGender.Text = rootobjectCardHolderInformation.responseObject.Gender;
                    textBoxAddress.Text = rootobjectCardHolderInformation.responseObject.Address;
                    //Convert.ToInt32(textBoxMinTopup.Text = rootobjectCardHolderInformation.responseObject.TopupBalance.ToString());
                    buttonPrint.Visible = true;
                }
                else
                {
                    groupBoxCardHolderInfo.Visible = false;
                    groupBoxCardTypeInfo.Visible = true;
                    textBoxName.Text = "";
                    textBoxCNIC.Text = "";
                    textBoxPhone.Text = "";
                    textBoxGender.Text = "";
                    textBoxAddress.Text = "";
                    buttonPrint.Visible = false;
                }
                buttonIssueCard.Enabled = true;
                buttonIssueCard.Visible = true;
               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonIssueCard_Click(object sender, EventArgs e)
        {
            try
            {
                LocalReport report = new LocalReport();
                if (string.IsNullOrEmpty(textBoxCardNumber.Text))
                {
                    MessageBox.Show("Please Enter Card Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBoxCardNumber.Focus();
                    return;
                }
                if (comboBoxCardType.SelectedIndex == -1)
                {
                    MessageBox.Show("Please Select Card Type", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    comboBoxCardType.Focus();
                    return;
                }
                IssueCard objissueCard = new IssueCard();
                objissueCard.CardNumber = textBoxCardNumber.Text;
                objissueCard.SerialNumber = CardSerialnumber;
                objissueCard.Issuedat = StaticClassSyncData.POS_ID;
                objissueCard.IssuedBy = StaticClassUserData.UserID;
                objissueCard.OrganizationID = StaticClassUserData.OrganizationID;
                objissueCard.CardType = comboBoxCardType.SelectedValue.ToString();
                objissueCard.CardPrice = textBoxCardFee.Text;
                objissueCard.Name = textBoxName.Text;
                objissueCard.CNIC = textBoxCNIC.Text;
                objissueCard.Phone = textBoxPhone.Text;
                objissueCard.Gender = textBoxGender.Text;
                objissueCard.Address = textBoxAddress.Text;
                objissueCard.CardtopupAmount = Convert.ToInt32(textBoxMinTopup.Text);
                objissueCard.IsPersonalized = isPersonalize;
                objissueCard.ExpiryTime = DateTime.Now.AddMonths(Convert.ToInt32(textBoxValidity.Text));
                if (objissueCard.CNIC == "")
                {
                    objissueCard.CNIC = "0000000000000";
                }
                //string CardSerial, string CNIC, int Topup, string CardTypeCode, DateTime CardExpiry
                var result = T10M1K.IssueCard(objissueCard.SerialNumber, objissueCard.CNIC, objissueCard.CardtopupAmount, CardType, DateTime.Now);
                if (result.isSuccess)
                {
                    RootobjectCardHolderInformation rootobjectIssueCard = issueCardManager.IssueCard(objissueCard);
                    if (rootobjectIssueCard.Heading == "Success")
                    {

                        int Topup = Convert.ToInt32(textBoxMinTopup.Text);
                        int CardFee = Convert.ToInt32(textBoxCardFee.Text);
                        int CalculateAmount = Topup + CardFee;
                        //string path = Path.GetDirectoryName(Application.ExecutablePath);
                        string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\CardIssuance.rdlc";
                        report.ReportPath = fullPath;
                        this.reportViewer2.LocalReport.ReportPath = fullPath;
                        this.reportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("CardType", CardType));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("CardNumber", rootobjectIssueCard.responseObject.CardNumber));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("CardFee", textBoxCardFee.Text));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("TopUp", textBoxMinTopup.Text));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("Operator", StaticClassUserData.Name));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("POSName", StaticClassSyncData.POS_Name));
                        this.reportViewer2.LocalReport.SetParameters(new ReportParameter("TotalAmount", CalculateAmount.ToString()));
                        this.reportViewer2.LocalReport.Print("CardIssuance");
                        MessageBox.Show("Card Issuance Receipt Printed Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LogManager logManager = new LogManager();
                        LogModel logModel = new LogModel();
                        logModel.UserID = StaticClassUserData.UserID;
                        logModel.ActionPerform = "Card Issued,card number= " + objissueCard.CardNumber + "and card issuance receipt printed successfully";
                        logModel.LoginDate = DateTime.Now.ToString();
                        logModel.UserName = StaticClassUserData.Name;
                        logModel.IP_Address = StaticClassSyncData.POS_IP;
                        logManager.SaveLog(logModel);
                        CardSerialnumber = "";
                        ClearData();
                        buttonIssueCard.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show(rootobjectIssueCard.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        InitializeCardManager InitializeCard = new InitializeCardManager();
        private void btnReadCard_Click(object sender, EventArgs e)
        {
            try
            {
                var response = T10M1K.ReadCardSerial();
               //response.isSuccess = true;
                if (response.isSuccess)
                {
                    RootobjectCardHolderInformation obj = InitializeCard.GetCardHolderInformationBySerialNumber(response.ReturData.ToString());
                    if (obj.Message == "card not found")
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardIssuance.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 0)
                    {
                        MessageBox.Show("Card Not Initialized", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardIssuance.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    if (obj.responseObject.CardStatus == 1)
                    {
                        // MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardIssuance.Visible = false;
                        panelMainScreen.Visible = true;
                        groupBoxCardTypeInfo.Visible = false;
                        textBoxCardNumber.Text = obj.responseObject.CardNumber;
                        CardSerialnumber = obj.responseObject.SerialNumber;
                    }
                    else if (obj.responseObject.CardStatus == 2)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardIssuance.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                    else if (obj.responseObject.CardStatus == 3)
                    {
                        MessageBox.Show(obj.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        pictureBoxCardIssuance.Visible = true;
                        panelMainScreen.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("Put the card in scanner", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            pictureBoxCardIssuance.Visible = true;
            panelMainScreen.Visible = false;
        }
        private void pictureBox17_Click(object sender, EventArgs e)
        {
            try
            {
                btnReadCard_Click(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void textBoxMinTopup_Validating_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int input = 0;
            bool isNum = Int32.TryParse(textBoxMinTopup.Text, out input);

            if (!isNum || input < minTopup || input > maxtopup)
            {
                // Cancel the event and select the text to be corrected by the user.
                //e.Cancel = true;
                //textBoxMinTopup.Select(0, textBoxMinTopup.Text.Length);
                //MessageBox.Show("Topup Amount must be between/equal " + minTopup + " and " + maxtopup + "", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void ClearData()
        {
            textBoxName.Text = "";
            textBoxCNIC.Text = "";
            textBoxPhone.Text = "";
            textBoxGender.Text = "";
            textBoxAddress.Text = "";
            panelMainScreen.Visible = false;
            pictureBoxCardIssuance.Visible = true;

            textBoxCardFee.Text = "";
            textBoxCardDiscount.Text = "";
            textBoxCardDiscount.Text = "";


            textBoxValidity.Text = "";
            textBoxMinTopup.Text = "";
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

            try
            {
                buttonIssueCard_Click(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string path = Path.GetDirectoryName(Application.ExecutablePath);
                path = path + @"\Release\CardPrinter.exe";
                Process.Start(path);
                Cursor.Current = Cursors.Default;
        
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
